#!python3
#-*-coding:utf8;-*-
"""Disassembler for the ZLOT of the Stantec zebra.
Type an address, and the program prints a stretch of code from that
address until a conditional instruction.
The "else" line are virtual instructions that are executed when the
condition is false.
Note that thanks to the weird way instructions are computed,
this disassembler can't always correctly compute the next address.
"""
from struct import unpack
from collections import defaultdict
import os

#TODO 2: fix paths. This is horrible
PATH = os.path.join(os.path.dirname(__file__), 'tapes/zlot.frd')
if not os.path.exists(PATH):
    PATH = "C:/users/BosJ/appdata/local/Jurjen/zebra/zlot.frd"

def func2letters(f):
    result = "" if f>>14&1 else "X"
    return result+''.join(
        letter if f>>14-i&1 else ""
        for i, letter in enumerate(LETTERS))

LETTERS = "AKQLRIBCDEV421W"

def parse(frdfile):
    "yield adr,f,r,d"
    def readline():
        data = frdfile.read(5)
        if not data: raise StopIteration
        return unpack('<HBH', data)
    while True:
        try:
            func, regs, start = readline()
            assert func==regs==0
            func,regs, stop = readline()
            assert func==regs==0
        except StopIteration: break
        for adr in range(start, stop+1):
            yield (adr,)+readline()

def dump(frdfile):
    for adr, func, regs, mem in parse(frdfile):
        print(adr, "%09x"%(func<<18|regs<<13|mem), display(adr,func,regs,mem))

#dump(open(PATH,"rb"))
#main program: dump code stretches
#read zlot file
drum = {adr:(f,r,d)
    for adr,f,r,d in parse(open(PATH,"rb"))
    }

#construct index of jumps to given addresses
drumIndex = defaultdict(list)
for a,(f,r,d) in drum.items():
    #d is pointed to by a
    if f&0x4001==0:  #no A, no W
        #x instructions with no W
        drumIndex[d].append(a)

from zebra import display

def search(adr):
    """Generate all execution paths leading to address"""
    #endpoints that cannot be extended
    result = []
    #endpoints of chain, with lengths
    todo = dict.fromkeys(drumIndex[adr], 1)
    while todo:
        a, depth = todo.popitem()
        if a==adr: continue
        if depth<12 and a in drumIndex and drumIndex[a]:
            todo.update((da,depth+1) for da in drumIndex[a])
        else:
            #write out chain
            chain = []
            p = a
            while p!=adr:
                chain.append(p)
                p = drum[p][2]
            chain.append(adr)
            #add chain in printing order
            if len(result)>20: print("Too many results")
            result.append(tuple(chain[::-1]))
    result.sort()
    for r in result:
            print('<-'.join(map(str, r)))

def main():
    #loop for entering new address
    adr = 0
    func, regs, mem = drum[adr]
    D = 0, 0, 2
    while True:
        entry = input("adres:")
        if entry.upper()=='E':
            #last condition was false, do implicit A instruction instead
            #do an else
            adr, (func, regs, mem) = "else", oldD
        elif not entry:
            #use current values, condition was true
            pass
        elif entry[0].upper()=='S':
            search(int(entry[1:]))
            continue
        elif entry[0] in '+-':
            #indexed with respect to adr
            mem += eval(entry)
            #handle carry
            regs += mem>>13
            mem &= ~(-1<<13)
            func += regs>>5
            func &= ~(-1<<15)
            adr = "sum"
        else:
            #free eval (security risk...)
            adr = eval(entry)
            func, regs, mem = drum[adr]
            D = 0, 0, mem+2
        #loop for following program flow. we have adr, func, regs, mem
        stop = False
        while not stop:
            print(display(adr, func, regs, mem))
            stop = False
            if func>>14:
                #A instruction: show data if not W
                if not func&1 and mem in drum:
                    print(display("data", *drum[mem]))
                if not D:
                    stop = True
                else:
                    #continue at D, remember stop condition
                    KnotE = func&0x2020==0x2000
                    adr, (func, regs, mem) = "(D)", D
                    #make sure two A instructions fail
                    D = None
                    if KnotE:
                        #K without E, show and stop
                        print(display(adr, func, regs, mem))
                        stop = True
            else:
                #X instruction
                if func&0x2020==0x2000:
                    #K without E, show indexing computation
                    #TODO: this instruction can sometimes be computed through
                    frm = drum[mem]
                    print(display(mem, frm[0], frm[1], frm[2], regs))
                    stop = True
                if func&0xe:
                    #V, show else clause
                    if D: print(display("else", *D))
                    stop = True
                #compute new D for continuation
                oldD, D = D, (func, regs, mem+2)
                if mem not in drum: stop = True
                else: adr, (func, regs, mem) = mem, drum[mem]

if __name__=='__main__':
    main()
