#!python3
"""User interface for the Zebra emulator.
"""
#-----------Background Zebra------------------------------------------
"""
There is a "output" queue that talks to the zebra process, and an "input" queue
that gets results from the Zebra process.
For the communication protocol, see the zebra.py file.
"""

#imports
import math
import multiprocessing
import os
import queue
import struct
import sys
import traceback

import tkinter.tix as tk
from tkinter.font import Font

#local imports
import about
from panels import CLOCKFORMAT, ControlPanel, IOpanel, RegisterPanel
import queuezebra
import zebra

#---------------User interface----------------------------------------

#TODO 5: switch to ttk or even wxWindows, at least do not fix the ugliness without doing that, or switch to wxPython/Kivy/whatever
#TODO 4: animate boot sequence (showing the toggles to do all this)

#find our local files
CURDIR = os.path.dirname(sys.executable if hasattr(sys, 'frozen') else __file__)
FILEDIR = os.path.join(CURDIR, 'tapes')
#set this to true to show communication (at the receiving ends)
queuezebra.COMMDEBUG = COMMDEBUG = False
#delay before showing balloon if balloon help is on
BALLOONDELAY = 1000
FONTSIZE = 11
SMALLFONTSIZE = 9
#number of instructions to ask at once for trace window
REQUESTSIZE = queuezebra.UNDOBUFSIZE//10

#delay for animating the zebra images
ANIMATIONSPEED = 200


#unused stuff
class MySelect(tk.Select):
    """Fix bug in Select where padx and pady are not accepted"""
    def __init__(self, master, cnf={}, **kw):
        tk.TixWidget.__init__(self, master, "tixSelect",
                        ["allowzero", "radio", "orientation", "labelside",
                            "options", "padx", "pady"],
                        cnf, kw)
        self.subwidget_list["label"] = tk._dummyLabel(self, "label")

def rounded_rect(canvas, x, y, w, h, c, outline="black"):
    """Draw a rounded rectangle on the canvas."""
    xl,xr,yt,yb = x+c, x+w-c, y+c, y+h-c
    canvas.create_line(xl, y, xr, y,
        fill=outline)
    canvas.create_arc(xr-c, y, xr+c, yt+c, start=0, extent=90, style="arc",
        outline=outline)
    canvas.create_line(xr+c, yt, xr+c, yb, fill=outline)
    canvas.create_arc(xr-c, yb-c, xr+c, yb+c, start=270, extent=90, style="arc",
        outline=outline)
    canvas.create_line(xr, yb+c, xl, yb+c,
        fill=outline)
    canvas.create_arc(x, yb-c, xl+c, yb+c, start=180, extent=90, style="arc",
        outline=outline)
    canvas.create_line(x, yb, x, yt,
        fill=outline)
    canvas.create_arc(x, y, xl+c, yt+c, start=90, extent=90, style="arc",
        outline=outline)

class BunchingQueue:
    """A Queue that buffers objects and sends them at once when you call .push()
    Overhead is reduced since .put is called less often.
    Assumes you call push relatively frequently, hence a list instead of a deque
    """
    def __init__(self, *args, **kwargs):
        self.queue = multiprocessing.Queue(*args, **kwargs)
        self.buffer = []

    def get(self, *args, **kwargs):
        while not self.buffer:
            self.buffer = self.queue.get(*args, **kwargs)
        return self.buffer.pop(0)

    def put(self, item, **kwargs):
        """Never blocks"""
        self.buffer.append(item)

    def push(self):
        """May block for limited size queue"""
        if self.buffer: self.queue.put(list(self.buffer))

    def empty(self):
        return not self.buffer and self.queue.empty()

    def close(self):
        self.push()
        self.queue.close()

#this is used again
class NumberWindow(tk.Toplevel):
    """Enter function, register, memory fields"""
    def __init__(self, focusLine):
        """Set focusLine to "reg", "addr" or "func" to choose line of cursor
        """
        super().__init__()
        self.title("Enter data for switches")
        #get current value of switches
        mask, value = app.controlPanel.word.getValue3()
        self.setupUI(mask, value)
        self.setupBalloonHelp()
        getattr(self, focusLine).focus()

    def setupUI(self, mask, value):
        tk.Label(self, text=
            "Enter the positions of the switches on the panel.\n"
            "Remember that on a real Zebra\n"
            "you had to do this in your head.",
            justify=tk.LEFT,
            ).grid(columnspan=2)
        tk.Label(self, text="Function:").grid(sticky=tk.W, padx=5, pady=5)
        self.func = tk.Entry(self)
        #fill with value if there is one
        funcMask, funcValue = mask>>18, value>>18
        self.func.insert(tk.END, self.formatRegister(funcMask, funcValue))
        self.func.grid(row=1, column=1, padx=5, sticky=tk.W+tk.E)

        tk.Label(self, text="Register:").grid(sticky=tk.W, padx=5, pady=5)
        self.reg = tk.Entry(self)
        #fill with value if there is one
        regMask, regValue = mask>>13&31, value>>13&31
        if regMask==0x1f: self.reg.insert(tk.END, regValue)
        self.reg.grid(row=2, column=1, padx=5, sticky=tk.W+tk.E)

        tk.Label(self, text="Address:").grid(sticky=tk.W, padx=5, pady=5)
        self.addr = tk.Entry(self)
        #fill with value if there is one
        memMask, memValue = mask&0x1fff,value&0x1fff
        if memMask==0x1fff: self.addr.insert(tk.END, memValue)
        self.addr.grid(row=3, column=1, padx=5, sticky=tk.W+tk.E)

        self.bind('<KeyPress-Return>', self.enterRegister)
        self.bind('<KeyPress-Escape>', lambda e:self.destroy())

    def setupBalloonHelp(self):
        app.balloon.bind_widget(self.func, msg=
            "Enter the letters you want to specify.\n"
            "Upper case put the switch in the upper position;\n"
            "lower case in the low position;\n"
            "the rest of the switches is put in the middle position.\n"
            "Leave empty if you want to keep the current value.\n"
            "For consistency, digits 1,2,4 set the switches in the \n"
            "lower position; use !,@,$ for upper.")
        app.balloon.bind_widget(self.reg, msg="Enter the register number.\n"
            "Leave empty if you want to keep the current value.")
        app.balloon.bind_widget(self.addr, msg="Enter the address.\n"
            "Track number and address are one (decimal) number.\n"
            "Leave empty if you want to keep the current value.")

    def enterRegister(self, event):
        funcs = self.func.get()
        if funcs:
            for low, up, r in zip(
                    "akqlribcdev421w",
                    "AKQLRIBCDEV$@!W",
                    app.controlPanel.word.allKeys[:15]):
                if low in funcs: r['value'] = 0
                elif up in funcs: r['value'] = 1
                else: r['value'] = 'x'
        regs = self.reg.get()
        if regs:
            regs = int(regs)
            for i,r in enumerate(app.controlPanel.word.allKeys[-14:-19:-1]):
                r['value'] = regs>>i&1
        addrs = self.addr.get()
        if addrs:
            addrs = int(addrs)
            for i,r in enumerate(app.controlPanel.word.allKeys[-1:-14:-1]):
                r['value'] = addrs>>i&1
        self.destroy()
        #don't let others get this event
        return "break"

    def formatRegister(self, mask, value):
        """Convert mask and number to string representing the settings.
        """
        result = ""
        for i, low, up  in zip(
                range(14, -1, -1),
                "akqlribcdev421w",
                "AKQLRIBCDEV$@!W"):
            if mask>>i&1:
                result += up if value>>i&1 else low
        return result

class TraceWindow(tk.Toplevel):
    """A window showing the last few instructions the zebra executed,
    allowing to click on them to show the exact machine state.
    If you single step beyond the window, the buffer is moved;
    if you run, the window becomes temporary unavailable until you stop.
    Maximum size of this window is determined by UNDOBUFSIZE
    If a line in the trace window is selected, that is the current instruction
    """
    RUNNING = "Zebra is running; press g to see trace."
    WAITING = "Waiting for reply"
    #TODO 2: ctrl-W should close window, Alt-F4 closes application (not window)
    #TODO 3: fix tape read for undo
    #TODO 2: don't let repeated instructions fill the buffer (which will need a second line counter)
    #TODO 3: figure out CROSFOOT; how do we run this?
    lineFormat = '{:08d}: {:30s} {:45s}'

    def __init__(self):
        super().__init__()
        self.title("Instruction trace")
        self.setupUI()
        #do this here because the window doesn't exist at app creation time
        app.balloon.bind_widget(self, msg=
            "These are the last instructions that are executed.\n"
            "Double-click on an instruction to jump to that point.")
        self.bind('<Destroy>', self.cleanup)
        #pass on some bindings
        #TODO 4: think about bindings; this shouldn't be necessary, is it?
        self.bind('<s>', lambda e:app.controlPanel.stepPushed())
        #step number of first line
        self.firstLineStep = None
        #we don't have to request an update;
        #openTraceWindow sent a enableUndo, and the acknowledge from zebra
        #causes an update request
        #which line should be activated when updated
        self.currentStep = None

    def setupUI(self):
        contentFont = Font(family="Courier", size=SMALLFONTSIZE)
        self.title = tk.Label(self,
            text=self.lineFormat.replace('08d', '8s').format(
                'step #', 'origin hex       instruction', 'meaning'),
            anchor=tk.W,
            font=contentFont)
        self.contents = tk.Frame(self)
        self.title.pack(side=tk.TOP, fill=tk.X)
        self.contents.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.listbox = tk.Listbox(self.contents,
            width=86, height=25,
            font=contentFont,
            selectmode=tk.SINGLE)
        self.scrollbar = tk.Scrollbar(self.contents,
            command=self.listbox.yview)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.listbox['yscrollcommand'] = self.set
        self.listbox.pack(fill=tk.BOTH, expand=True)
        self.listbox.bind("<Double-Button-1>", self.click)
        #the line corresponding to the processor state
        #in contrast with current processor, C is the next instruction,
        #so there is no such a thing as 'before the instruction'
        self.currentPosition = 0
        #request updates for counter
        app.requestZebra("register", -3, None)

    def set(self, *args):
        self.scrollbar.set(*args)

    def update(self, startStep, lines):
        """Update a range of lines, starting from the given stepCounter value.
        the values consists of pairs (C, origin) of consecutive instructions
        that were received from the background process
        The window is filled from bottom to top;
        if we find unfilled lines just above us, initiate filling above lines.
        """
        #TODO 2: figure out if it is possible that lines are "forgotten" if we reduce the number of undo requests
        listbox = self.listbox
        size = self.listbox.size()
        #initialize firstLineStep if needed
        if self.firstLineStep is None:
            #we start afresh, set first line stepCounter
            self.firstLineStep = startStep
        if lines and lines[0] is None:
            #this is a flag indicating that no earlier info is available
            #remove flag from lines data
            del lines[0]
            #erase all earlier stuff from window
            #remove corresponding lines
            if self.firstLineStep<startStep:
                listbox.delete(0, startStep-self.firstLineStep-1)
            self.firstLineStep = startStep
        #compute start and end lines for updating list
        startline = startStep-self.firstLineStep
        endline = startline+len(lines)-1
        if startline<0:
            #we have lines before the top line; shift by -startline to get them in view
            self.firstLineStep = startStep
            endline += startline
            startline = 0
        elif startline>=size:
            #new lines to be read, that will be updated later
            listbox.insert(tk.END, *[self.WAITING]*(startline-size))
        self.replaceLines(startline, endline, lines)
        #request data for unfilled lines
        if startline and listbox.get(startline-1)==self.WAITING:
            #there are unfilled files before current ont; fill new lines with appropriate data
            #TODO 4: be a bit more gentle, asking for not more data than needed, e.g. using binary search (!?). This is done by making line a smaller value, and testing listbox.get(startline-line-2): increase line if this is the waiting text
            line = startline-REQUESTSIZE
            if line<0: line = 0
            self.after_idle(app.requestZebra,
                "undo",
                line+self.firstLineStep,
                startline-line)
        elif self.currentStep is not None:
            self.activateCurrentInstruction()

    def replaceLines(self, startline, endline, lines):
        """Replace listbox[startline:endline] with formatted lines
        """
        #delete the existing lines from the listbox
        self.listbox.delete(startline, endline)
        startStep = startline+self.firstLineStep
        #convert the lines to display format, and insert in display
        lines = [
            self.lineFormat.format(
                counter,
                zebra.display(origin, *zebra.split(C), explainCondition=False),
                zebra.explain(origin, *zebra.split(C)))
            for counter, (C, origin) in enumerate(lines, start=startStep)
            ]
        self.listbox.insert(startline, *lines)

    def activateCurrentInstruction(self):
        """Selects current instruction if possible.
        (This underlines the instruction.)
        """
        listbox = self.listbox
        if self.currentStep is None or self.firstLineStep is None: return
        index = self.currentStep-self.firstLineStep
        if 0<=index<listbox.size():
            currentSelection = listbox.curselection()
            assert len(currentSelection)<2
            if currentSelection:
                currentSelection = int(currentSelection[0])
                if currentSelection==index: return
                #different line selected
                listbox.selection_clear(currentSelection)
            #nothing selected: now set it to new value
            listbox.activate(index)
            listbox.selection_set(index)
            #show active line
            listbox.see(index)

    def click(self, event):
        """User clicked in the window. Now trace forward or backward to that
        point."""
        index = self.listbox.index(tk.ACTIVE)
        step, text = index+self.firstLineStep, self.listbox.get(index)
        #TODO 2: double click in trace window to jump to that instruction state
        print("Click", step, text)

    def step(self):
        """The zebra did a step command: update window for just this step."""
        #TODO 3: adjust step, if we have written the code for an indicator: this will increase the indicated line number
        #append line at end for new entry; do not follow through for other line
        app.requestZebra("undo", 0, 1)

    def run(self):
        """The zebra is running: show user he can't see anything now."""
        self.listbox.insert(tk.END, self.RUNNING)
        self.listbox.see(tk.END)

    def stop(self):
        """The zebra stopped for some reason: update display again.
        """
        listbox = self.listbox
        if listbox.get(tk.END)==self.RUNNING:
            #Delete the "Zebra is running" line (maybe missing, but that's OK)
            listbox.delete(tk.END)
        #request a bunch of lines; the rest will be requested by update
        app.requestZebra("undo", 0, REQUESTSIZE)

    def processCounter(self, value):
        """The zebra did an undo; update current instruction indicator."""
        self.currentStep = value
        self.activateCurrentInstruction()

    def cleanup(self, event):
        """Tell application we are gone."""
        app.traceWindow = None

class ZebraProcess(multiprocessing.Process):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.outputQueue = multiprocessing.Queue()
        self.inputQueue = multiprocessing.Queue()
        #so we don't have to join, allowing faster quit
        self.daemon = True

    def run(self, **kwargs):
        #import cProfile, pstats
        #pr = cProfile.Profile()
        #pr.enable()
        queuezebra.QueueZebra(self.inputQueue, self.outputQueue).run()
        #pr.disable()
        #this is only printed when you press F12
        #ps = pstats.Stats(pr)
        #ps.strip_dirs().sort_stats('tottime').print_stats(25)
        #ps.print_callees('step')

class Application(tk.Frame):
    def __init__(self, master, zebra, updateTime=100):
        self.zebra = zebra
        self.updateTime = updateTime
        super().__init__(master)
        self.setupUI()
        #start the polling calls
        self.listen()
        self.bind('<Destroy>', self.cleanup)
        #link to trace window
        self.traceWindow = None
        #start the zebra process
        self.zebraSpeed = 2
        zebra.start()
        self.zebraStarted()
        #receive updates about register values
        self.registerPanel.redrawRegisters()

    def setupUI(self):
        """Build the user interface.
        """
        leftPanel = tk.Frame(self)
        self.controlPanel = ControlPanel(leftPanel,
            ANIMATIONSPEED,
            app=self)
        self.controlPanel.pack(
            side=tk.BOTTOM,
            fill=tk.X)
        self.registerPanel = RegisterPanel(leftPanel, app=self)
        self.registerPanel.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        leftPanel.pack(side=tk.LEFT, fill=tk.Y)
        self.ioPanel = IOpanel(self, app=self)
        self.ioPanel.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.pack(fill=tk.BOTH, expand=True)
        self.menu = self.makeMenu()
        self.setupBalloonHelp()
        self.bindKeyboardShortcuts()

    def requestZebra(self, *args):
        """Send a request message to the zebra.
        The trick is to request the zebra to send a value if different from
        what we show. Only after updating, we request a new value.
        This way, we can be behind, but we will still eventually show
        the right value.
        """
        self.zebra.inputQueue.put(('R',)+args)

    def tellZebra(self, *args):
        """Send a set message to the zebra.
        """
        self.zebra.inputQueue.put(('S',)+args)

    def listen(self):
        """Poll the zebra queue."""
        self.afterListen = None
        if not self.zebra.is_alive():
            self.controlPanel.setImage(self.controlPanel.GONE)
        try:
            while True:
                device, index, value = self.zebra.outputQueue.get_nowait()
                if hasattr(self, 'Z'+device):
                    getattr(self, 'Z'+device)(index, value)
                    if COMMDEBUG:
                        print("Zebra->UI:", device, index, value)
                else:
                    print("Unexpected message from zebra:", device, index, value)
        except queue.Empty: pass
        except:
            print("Exception in processing message from zebra:")
            traceback.print_exc()
        #We do this a bit more often than the update time
        self.afterListen = self.after(self.updateTime//3, self.listen)

    def zebraStopped(self):
        """We just heard the zebra has stopped (speed=0 or 1).
        Adjust traceWindow if available.
        Adjusting the zebra image is done separately,
        because that depends on stop reason.
        """
        #tell traceWindow
        if self.traceWindow is not None: self.traceWindow.stop()

    def zebraStarted(self):
        """We just heard the zebra has started (speed>1). Adjust interface.
        Adjusting the zebra image is done separately,
        because that depends on the speed.
        """
        #tell traceWindow
        if self.traceWindow is not None: self.traceWindow.run()
        #ask for efficiency updates
        self.after(self.updateTime, self.requestZebra, 'efficiency')

    def Zbreak(self, index, addr):
        """There was a break at this point"""
        print("Zbreak received. Is this important?")
        self.zebraStopped()

    def Zdebug(self, index, data):
        """Show reply from debug request"""
        from pprint import pprint
        print("Debug:", index)
        pprint(data)

    def Zefficiency(self, efficiency, values):
        """Update efficiency meter and elapsed time.
        We get a measurement time, and an efficiency back.
        When the machine stops initially, the time is 0
        """
        time, stepCounter = values
        self.controlPanel.word.meter['value'] = efficiency
        self.controlPanel.word.clock['text'] = CLOCKFORMAT.format(
                stepCounter, int(math.floor(time/60)), time%60)
        if self.zebraSpeed>1:
            self.after(self.updateTime, self.requestZebra, 'efficiency')

    def Zerror(self, index, message):
        print("Exception in Zebra subprocess:", message, sep='\n')

    def Zmemory(self, addr, value):
        """Receive memory values, request update"""
        self.afterZmemory = None
        if self.registerPanel.processUpdate(str(addr), value):
            #we get visibility flag back
            #only request new value if it was visible
            self.afterZmemory = self.after(self.updateTime,
                self.requestZebra,
                "memory", addr, value)

    def Zprinter(self, index, char):
        """Receive printed character"""
        self.ioPanel.printChar(char)

    def Zpunch(self, index, char):
        """Receive character to punch."""
        self.ioPanel.punchChar(char)

    def Zregister(self, regNum, value):
        """Receive register values, request update
        Special register Counter is the current instruction counter
        """
        #clear old after routine
        self.afterZregister = None
        if regNum==-3:
            #stepCounter; this is interesting for traceWindow
            updateFlag = self.traceWindow is not None
            if updateFlag:
                self.traceWindow.processCounter(value)
        else:
            #we get visibility flag back
            updateFlag = self.registerPanel.processUpdate(regNum, value)
        if updateFlag:
            #request new value when it changes
            self.afterZregister = self.after(self.updateTime,
                self.requestZebra,
                "register", regNum, value)

    def Zspeed(self, index, value):
        """Zebra speed change message.
        index can be "set", "stop", "step", "dial"
        indicating reason of speed change
        The combination set,1 is ignored,
        because we wait for the acknowledge step,1 that comes after it.
        """
        controlPanel = self.controlPanel
        self.zebraSpeed = int(value)
        if value==0:
            #stopped: figure out reason
            if self.controlPanel.stopVar.get():
                #user requested stop explicitly
                controlPanel.setImage(controlPanel.GRAZE)
            elif index=="dial":
                #we hit a dial instruction and the zebra stopped
                controlPanel.setImage(controlPanel.DIAL)
                self.controlPanel.dial()
            elif index=="break":
                #breakpoint hit or end of tape
                controlPanel.setImage(controlPanel.STAND)
            elif index=="tape":
                #TODO 3: make cool image for end of tape?
                controlPanel.stopButton.invoke()
            else:
                #the zebra stopped waiting for U7
                controlPanel.setImage(controlPanel.ACTIVE)
            self.zebraStopped()
        elif value==1:
            if index=="set":
                #acknowledge of reception of step command: no reaction needed
                pass
            elif index=="step":
                #step has executed: do a simplistic animation
                controlPanel.setImage(controlPanel.STEP)
                self.after(ANIMATIONSPEED,
                    controlPanel.setImage,
                    controlPanel.STAND)
                #tell traceWindow
                if self.traceWindow is not None: self.traceWindow.step()
                #get efficiency explicitly: is not updated when stopped
                self.requestZebra('efficiency')
            elif index=="dial":
                #dialed number received: let's run again
                self.controlPanel.setSpeed()
                #we will adjust UI when acknowledge is received
        elif value==2:
            #run because of dialed number or speed request
            controlPanel.setImage(controlPanel.WALK)
            self.zebraStarted()
        elif value==3:
            #run because of dialed number or speed request
            controlPanel.setImage(controlPanel.RUN)
            self.zebraStarted()
        else:
            raise NotImplementedError

    def Zstart(self, index, value):
        """The Zebra has started.
        """
        self.controlPanel.setImage(self.controlPanel.STAND)

    def Ztape(self, index, char):
        """Character was read, update pointer"""
        if index is None: return
        line, col = index
        self.ioPanel.showReadPosition(line, col, char)

    def Zundo(self, index, value):
        """Process undo reply from zebra.
        """
        if self.traceWindow is not None:
            self.traceWindow.update(index, value)

    def getNumber(self, event):
        """Open the "enter number dialog" that allows to enter register number
        and memory address (and instruction, for that matter)
        The event letter determines the cursor position:
        r is register and a is address
        """
        #set focus dependent on event
        if isinstance(event, str): focusLine = event
        elif event.char=='\x12': focusLine = "reg"
        elif event.char=='\x01': focusLine = "addr"
        elif event.char=='\x06': focusLine = "func"
        else: raise NotImplementedError
        self.numberWindow = NumberWindow(focusLine)

    def nope(self): print("This command doesn't work")
    def makeMenu(self):
        menubar = tk.Menu(root)
        filemenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="File", menu=filemenu)
        filemenu.add_command(label="Open tape file",
            accelerator='O',
            command=self.ioPanel.loadButton.invoke)
        filemenu.add_command(label="Save print output", command=self.nope)
        filemenu.add_command(label="Save punch output", command=self.nope)
        filemenu.add_separator()
        filemenu.add_command(label="Open memory (FRD) file", command=self.loadFRD)
        filemenu.add_command(label="Save memory (FRD) file", command=self.nope)
        filemenu.add_separator()
        filemenu.add_command(label="Quit",
            accelerator='Ctrl-Q',
            command=root.destroy)

        viewmenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="View", menu=viewmenu)
        viewmenu.add_command(label="Add register/memory",
            command=self.registerPanel.addEntry)
        viewmenu.add_command(label="Delete selected register",
            command=self.registerPanel.deleteRegister)
        viewmenu.add_command(label="Basic register set",
            command=lambda:self.registerPanel.setRegisters(
                RegisterPanel.SMALLSET))
        viewmenu.add_command(label="Default register set",
            command=lambda:self.registerPanel.setRegisters(
                RegisterPanel.INITIALREGISTERS))
        viewmenu.add_separator()
        viewmenu.add_command(label="Show memory dump", command=self.nope)
        viewmenu.add_command(label="List execution path", command=self.nope)
        viewmenu.add_command(label="Show trace window",
            command=self.openTraceWindow,
            accelerator = 't')
        #menu: explain instruction

        #save runmenu so the undo item can be enabled/disabled
        runmenu = self.runmenu = tk.Menu(menubar, tearoff=0)
        #submenu for conditional keys
        condmenu = tk.Menu(runmenu)
        word = self.controlPanel.word
        condmenu.add_command(
            label="set A",
            accelerator='a/A/x/X',
            command=word.allKeys[0].keyRotate)
        for i,low,up in zip(range(1,15), "kqlribcdev421w", "KQLRIBCDEV$@!W"):
            condmenu.add_command(
                label="set "+(low if low.isdigit() else up),
                accelerator=low+'/'+up,
                command=word.allKeys[i].keyRotate)
        condmenu.add_separator()
        condmenu.add_command(label="Set function",
            accelerator='^F',
            command=lambda:self.getNumber("func"))
        condmenu.add_command(label="Set register",
            accelerator='^R',
            command=lambda:self.getNumber("reg"))
        condmenu.add_command(label="Set address",
            accelerator='^A',
            command=lambda:self.getNumber("addr"))

        menubar.add_cascade(label="Run", menu=runmenu)
        runmenu.add_checkbutton(label="Fast",
            accelerator='f',
            variable=self.controlPanel.fastVar,
            command=self.controlPanel.setSpeed)
        runmenu.add_separator()
        runmenu.add_checkbutton(label="Stop",
            accelerator='g',
            command=self.controlPanel.setSpeed,
            variable=self.controlPanel.stopVar)
        runmenu.add_command(label="Step",
            accelerator='s',
            command=self.controlPanel.stepPushed)
        runmenu.add_command(label="Manual",
            accelerator='m',
            command=self.controlPanel.manualPushed)
        runmenu.add_command(label="Toggle start key",
            accelerator='F7',
            command=self.controlPanel.startButton.invoke)
        runmenu.add_cascade(label="Conditional stop keys", menu=condmenu)
        runmenu.add_separator()
        runmenu.add_checkbutton(label="Enable undo",
            command=self.enableUndo,
            variable=self.controlPanel.undoVar)
        runmenu.add_command(label="Undo", command=self.undo,
            state="disabled", accelerator='<-')

        helpmenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="Help", menu=helpmenu)
        self.useBalloons = tk.BooleanVar(value=tk.FALSE)
        helpmenu.add_checkbutton(label='BalloonHelp',
            command=self.switchBalloonHelp,
            accelerator='h',
            variable=self.useBalloons)
        helpmenu.add_separator()
        helpmenu.add_command(label="About this program",
            command=about.AboutThisProgram)
        helpmenu.add_command(label="About van der Poel",
            command=about.AboutWillem)
        helpmenu.add_command(label="About the Stantec Zebra",
            command=about.AboutZebra)

        # display the menu
        root['menu'] = menubar
        return menubar

    def loadFRD(self):
        dialog = tk.ExFileSelectDialog(self,
            title="Select an FRD file:",
            command=self.doLoadFRD,
            )
        fsbox = dialog.subwidget('fsbox')
        fsbox['filetypes'] = \
            "{{*}     {.* All files}} " \
            "{{*.FRD} {.FRD Zebra memory dump}} "
        fsbox['directory'] = FILEDIR
        fsbox['pattern'] = "*.FRD"
        #make the window comfortably big
        screen_height = root.winfo_screenheight()
        fileDirPane = fsbox._nametowidget('lf.pane')
        #use half the screen height for the dir/file lists
        fileDirPane['height'] = min(screen_height//2, 800)
        #make the dir/file lists reasonably wide
        fileDirPane['width'] = 450
        dialog.bind('<Escape>', lambda e:dialog.popdown())
        dialog.popup()

    def doLoadFRD(self, path):
        self.tellZebra("loadfrd", path)
        self.registerPanel.redrawRegisters()
        #make sure we get the key presses again, now the window disappeared

    def undo(self, event=None):
        """Both menu and event routine: event code ignored.
        Tell zebra to undo a step.
        """
        #TODO 1: make this a special case of a routine that does multiple undo/redo for the double click in traceWindow
        self.tellZebra("undo")

    def enableUndo(self):
        undoActive = self.controlPanel.undoVar.get()
        self.tellZebra("undo", undoActive)
        self.runmenu.entryconfig("Undo",
            state='normal' if undoActive else 'disabled')
        #remove trace window if undo is inactive
        if not undoActive: self.traceWindow = None

    def openTraceWindow(self, event=None):
        """Both menu and keyboard handler.
        """
        if not self.controlPanel.undoVar.get():
            #switch undo on if needed (not done if already set to save the buffer)
            self.controlPanel.undoVar.set(True)
        #this command causes the zebra to return with an acknowledge,
        #which results in the first request for update
        self.enableUndo()
        #stop the zebra; the trace window is static
        self.controlPanel.stopButton.select()
        self.controlPanel.setSpeed()
        #open window when needed
        if self.traceWindow is None: self.traceWindow = TraceWindow()

    def setupBalloonHelp(self):
        self.balloon = tk.Balloon(initwait=BALLOONDELAY,
            relief=tk.RIDGE,
            borderwidth=3,
            state=tk.NONE,
            name='helpBalloon')
        cp = self.controlPanel
        bind_widget = self.balloon.bind_widget
        bind_widget(cp.lock, msg=
            "Determine which memory pages of 512 bytes are write protected.\n"
            "1: read-only; 0: allow writing.\n"
            "T0 is a separate switch to write protext track 0 (000-031).")
        bind_widget(cp.lock.enable, msg="Override lock keys:\n"
            "If depressed, all memory pages are writeable.\n"
            "Shortcut: U")
        bind_widget(cp.userKeys, msg=
            "Switches U1-U6: can be tested by conditional jumps.\n"
            "1: jump succeeds; 0: jump fails.\n"
            "Shortcut: F1-F6 to toggle;\n"
            'The Start key is U7: it is "off" while depressed.\n'
            "(Use shift-F7 to toggle the Start key.)")
        bind_widget(cp.word.meter, msg=
            "This is the famous efficiency meter")
        bind_widget(cp.word.clock, msg=
            "Number of instructions executed and elapsed time.\n"
            "Fortunately, we have fast mode.")
        bind_widget(cp.word.setButtons, msg=
            "Shortcut for function keys: the letter (with or without shift)\n"
            "Shortcut for entering register number: Ctrl-R\n"
            "Shortcut for entering drum address: Ctrl-A")
        bind_widget(cp.word, msg=
            "These 33 switches have two functions:\n"
            "They can be used to set a conditional stop,\n"
            "or they can be used to input a manual instruction.\n"
            "The middle position means \"don't care\" for conditions,\n"
            "and is seen as 0 by the manual button.\n")
        bind_widget(cp.speedButton, msg=
            "Status of the zebra machine.\n"
            "Standing attentively: machine is waiting for user input\n"
            '(in the old days, a 700 Hz "hooter" was heard at that time).\n'
            "Walking: the computer is emulated in real time.\n"
            "Galloping: the computer is emulated much faster.\n"
            "Standing lazily: the stop key is depressed.\n"
            "Dial: computer is waiting for you to dial a number.\n"
            "Click or press F to switch between real time (depressed)\n"
            "and full speed (raised)."
            )
        bind_widget(cp.condStopKey, msg=
            "Activate the conditional stop set by the selected bits\n"
            "of the instruction in the C register with the 33\n"
            "three-position keys.\n"
            "Shortcut: =")
        bind_widget(cp.stopButton, msg=
            "Stop the machine if depressed.\n"
            "You can then single step the machine or give manual instructions.\n"
            "Shortcut: G or H")
        bind_widget(cp.stepButton, msg=
            "Single step the machine when stopped.\n"
            "Shortcut: S")
        bind_widget(cp.manualButton, msg=
            "Enter an instruction in the C register\n"
            "via the three state switches (middle position=0)\n"
            "and execute it immediately.\n"
            "Shortcut: M")
        bind_widget(cp.clearButton, msg=
            "Clears registers A,B,C,D and the carry trap.\n"
            "Press right mouse button to keep it depressed.\n"
            "Shortcut: Z (shift to lock)")
        bind_widget(cp.startButton, msg=
            'Set U7 to "off" temporarily.\n'
            "Press right mouse button to keep it depressed.\n"
            "Shortcut: F7 (shift to lock)")
        io = self.ioPanel
        bind_widget(io.currentName, msg="Name of input tape file.\n"
            "Click to reload.")
        bind_widget(io.loadButton, msg="Open a new input tape file.\n"
            "Shortcut: O")
        bind_widget(io.reader, msg=
            "Shows the contents of the input tape file.\n"
            "Whitespace and {...} comments are skipped when reading.\n"
            "Blue text is already read by the machine;\n"
            "some read characters may not be marked\n"
            "if the user interface cannot keep up with the zebra.\n")
        bind_widget(io.punch, msg="Show the punched output.\n"
            "The way the output is interpreted for display\n"
            "is determined by the buttons below.")
        bind_widget(io.punchType.subwidget("normal"), msg=
            "Show punched output as normal code")
        bind_widget(io.punchType.subwidget("simple"), msg=
            "Show punched output as simple code")
        bind_widget(io.punchType.subwidget("print"), msg=
            'For "fast" output, they used the punch to produce\n'
            "Baudot code for an external teleprinter.\n"
            "Show the output as this teleprinter would print it.")
        bind_widget(io.punchType.subwidget("binary"), msg=
            "Show the binary punched tape in 7 column format.")
        bind_widget(io.printer, msg="Show output of the printer.\n"
            "The upward arrow is the position of the print head.")
        regs = self.registerPanel
        bind_widget(regs, msg="Improved version of the Zebra screen "
            "that originally showed\nbinary values of A,B,C "
            "and one selectable register\nas dots on a pale CRT tube.\n"
            "In this version, you can edit numbers.\n"
            "Also, you can replace a register number or address by one or\n"
            "more names or addresses to view other information.\n"
            "To enter multiple registers or addresses,\n"
            "replace number by n,m,..., or n-m for a range.")

    def switchBalloonHelp(self, toggle=False):
        """Set balloon to follow balloon help option.
        If toggle=True, do the toggling yourself (not for menu!)
        """
        #for use as a menu command, toggle is automatic
        if toggle:
            self.useBalloons.set(not self.useBalloons.get())
        self.balloon['state'] = (tk.NONE, tk.BALLOON)[self.useBalloons.get()]
        #TODO 4: remove balloon immediately if turned off; this doesn't work
        #print(self.balloon.event_generate(sequence='<FocusOut>'))

    def bindKeyboardShortcuts(self):
        """Make the keyboard shortcuts.
        In use: _=   abcdefghi klm o qrstuvwx z   F1-7 <BS>
        shift:    !@$ABCDE   I KL    QR   VWX Z   F7
        ctrl:        A    F          QR
        """
        #general
        #TODO 3: figure out how bindings are supposed to be used; information is in http://effbot.org/tkinterbook/tkinter-events-and-bindings.htm, under "Instance and class bindings". Apparently, you get the event potentially four times: widget (bind to widget), window (bind to window), class (don't use that, internal for tk), application (bindall). Return "break" from event routine if you don't want to pass it on any further. You can add a binding instead of replace by adding '+' as third argument
        self.bind_all('<Control-q>', lambda e:root.destroy())
        cp = self.controlPanel
        #speed
        self.bind_all('<f>', lambda e:cp.speedButton.invoke())
        #memory lock
        self.bind_all('<u>', lambda e:cp.lock.enable.invoke())
        #user keys
        for i in range(1,7):
            #the user keys are numbered backwards
            key = cp.userKeys.allKeys[-i]
            #don't change the original function key links, so just root
            root.bind('<F{}>'.format(i), key.keyToggle)
        #control keys
        self.bind_all('<=>', lambda e:cp.condStopKey.invoke())
        self.bind_all('<g>', lambda e:cp.stopButton.invoke())
        self.bind_all('<s>', lambda e:cp.stepPushed())
        self.bind_all('<space>', lambda e:cp.stepPushed)
        self.bind_all('<m>', lambda e:cp.manualPushed())
        self.bind_all('<KeyPress-z>', lambda e:cp.clearButton.invoke())
        self.bind_all('<KeyRelease-z>', lambda e:cp.clearButton.invoke())
        self.bind_all('<Shift-Z>', lambda e:cp.clearButton.invoke())
        self.bind_all('<KeyPress-F7>', lambda e:cp.startButton.invoke())
        self.bind_all('<KeyRelease-F7>', lambda e:cp.startButton.invoke())
        self.bind_all('<Shift-KeyPress-F7>', lambda e:cp.startButton.invoke())
        #Bug in tkinter: for some reason, Shift-KeyRelease-F7 was bound, undo it
        self.bind_all('<Shift-KeyRelease-F7>', lambda e:None)
        #I/O
        io = self.ioPanel
        self.bind_all('<o>', lambda e: io.loadButton.invoke())
        #breakpoint letters. These will only work if window has focus
        word = cp.word
        #TODO 3: this doesn't work, how do I get focus without getting it during NumberWindow? (answer: return 'break' in numberWindow's routines, or bind to the widget, or ...)
        for i,low,up in zip(range(15), "akqlribcdev421w", "AKQLRIBCDEV$@!W"):
            self.bind('<KeyPress-'+low+'>', word.allKeys[i].keyToggle0)
            self.bind('<Shift-KeyPress-'+up+'>', word.allKeys[i].keyToggle1)
        self.bind('<KeyPress-x>', word.allKeys[0].keyToggle1)
        self.bind('<Shift-KeyPress-X>', word.allKeys[0].keyToggle0)
        self.bind_all('<Control-r>', self.getNumber)
        self.bind_all('<Control-a>', self.getNumber)
        self.bind_all('<Control-f>', self.getNumber)
        #temp code for testing tcl meter
        self.bind_all('<F10>', self.tcltest)
        #stop the zebra process allowing for profile dump (for debug only)
        self.bind_all('<F12>', lambda e: self.zebra.inputQueue.put(('Q',None)))
        #other
        self.bind_all('<h>', lambda e: self.switchBalloonHelp(toggle=True))
        self.bind('<BackSpace>', self.undo)    #otherwise dial window doesn't work
        self.bind_all('<t>', self.openTraceWindow)

    def cleanup(self, *args):
        """Suppress as many tkinter errors as we can."""
        #TODO 1: remove all these "self.after..." when we are sure there are no more errors
        return
        if self.afterListen is not None:
            self.after_cancel(self.afterListen)
        if self.afterZregister is not None:
            self.after_cancel(self.afterZregister)
        if self.afterZmemory is not None:
            self.after_cancel(self.afterZmemory)
        if self.controlPanel.afterAnimate is not None:
            self.after_cancel(self.controlPanel.afterAnimate)

    def tcltest(self, event):
        """Test for running Tcl code.
        Makes a new toplevel with a meter in it.
        """
        #TODO 3: translate meter to Python to we can actually use it
        self.tk.eval('''
            toplevel .m
            set ::pi 3.14 ;# Good enough accuracy for gfx...

            # Create a meter 'enabled' canvas
            proc makeMeter {w} {
                global meter angle
                canvas $w -width 200 -height 110 -borderwidth 2 -relief sunken -bg white
                for {set i 70;set j 0} {$i<100} {incr i 2;incr j} {
                    set meter($j) [$w create line 100 100 10 100 \
                        -fill grey$i -width 3 -arrow last]
                    set angle($j) 0
                    $w lower $meter($j)
                    updateMeterLine $w 0.2 $j
                }
                $w create arc 10 10 190 190 -extent 108 -start 36 -style arc -outline red
                return $w
            }

            # Draw a meter line (and recurse for lighter ones...)
            proc updateMeterLine {w a {l 0}} {
                global meter angle pi
                set oldangle $angle($l)
                set angle($l) $a
                set x [expr {100.0 - 90.0*cos($a * $pi)}]
                set y [expr {100.0 - 90.0*sin($a * $pi)}]
                $w coords $meter($l) 100 100 $x $y
                incr l
                if {[info exist meter($l)]} {updateMeterLine $w $oldangle $l}
            }

            # Convert variable to angle on trace
            proc updateMeter {name1 name2 op} {
                upvar #0 $name1 v
                set min [.m.s cget -from]
                set max [.m.s cget -to]
                set pos [expr {($v - $min) / ($max - $min)}]
                updateMeterLine .m.c [expr {$pos*0.6+0.2}]
            }

            grid [makeMeter .m.c]
            grid [scale .m.s -orient h -from 0 -to 100 -variable v]
            trace variable v w updateMeter

            # Fade over time
            proc updateMeterTimer {} {
                set ::v $::v
                after 20 updateMeterTimer
            }
            updateMeterTimer
            ''')

if __name__=='__main__':
    from sys import argv
    root = tk.Tk()
    root.title("Stantec Zebra Emulator")
    about.setRoot(root)
    app = Application(root, ZebraProcess(name="backgroundZebra"))
    if len(argv)>1: fileName = argv[1]
    else: fileName = 'DEMO1'
    try: path = zebra.Tape.expandPath(fileName)
    except ValueError: path = None
    app.ioPanel.loadTape(path)
    root.focus_set()

    #import cProfile, pstats
    #pr = cProfile.Profile()
    #pr.enable()
    app.mainloop()
    #pr.disable()
    #pstats.Stats(pr).strip_dirs().sort_stats('tottime').print_stats(25)
