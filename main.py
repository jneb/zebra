#!python3
import os, sys, io, difflib, re
sys.path.append(os.path.join(os.path.dirname(__file__),'trace'))
from zebra import assemble, split, useless, display
from ztrace import Interpreter, drum, readFRD, ZLOT6, main as ztraceMain
from zebra import PrintZebra,Drum,TAPELINES,WRITEMEM,INSTRUCTION,TAPESYMBOLS




#read ZLOT
for adr,f,r,d in readFRD(ZLOT6):
    drum[adr] = f<<18|r<<13|d

def compareTrace(p1,p2):
    """determine differences between traces
    """
    # stop instead of interact
    import state
    def stop(s):
        raise EOFError
    state.input = stop
    # catch output
    sys.stdout = io.StringIO()
    try:
        Interpreter(22, p1).main(10**4)
    except:
        output1 = sys.stdout.getvalue()
    sys.stdout = io.StringIO()
    try:
        interpreter = Interpreter(22, p2)
        interpreter.tapeReader.faketape = p1
        interpreter.main(10**4)
    except:
        output2 = sys.stdout.getvalue()
    sys.stdout = sys.__stdout__
    print("computing diff")
    for line in difflib.unified_diff(
            output1.splitlines(keepends=True),
            output2.splitlines(keepends=True),
            p1,
            p2,
            n=5):
        print(line, end="")

def looksLikeCode(s):
    if not s[0] in "0AXN": return False
    return set(s) <= set(TAPESYMBOLS + "/\\%SH")

def main(choice):
    if not choice:
        choice = "demo2"
    if looksLikeCode(choice):
        intrp = Interpreter(22, "0" + choice)
        intrp.verbose = False
        intrp.main(10**4)
    else:
        zebra = PrintZebra()
        zebra.readLOT()
        zebra.drum.resetClock(realTime=False)
        zebra.inputTape.load('logsq')
        zebra.displayMode = TAPELINES|WRITEMEM
        zebra.switches.pressU7()
        zebra.run(breakPoint=0)
     
if __name__ == "__main__":
    print("Options:\n"
          "<commands>: run symbolic tracer\n"
          "<commands>: run symbolic tracer\n"
          "or give name of program\n")
    main(input("Choice:"))
