#!python3
"""
Test code for ztrace
"""
from unittest import TestCase, main, skip

from zebra import assemble, split, useless, MASK33

from trace.explain import Explainer
def explain(adr, func, reg, mem):
    return Explainer(adr, func, reg, mem)()

__test__ = {
    'RegisterBank': '''
    #>>> digit = BoundedProperty(0, 9)
    #>>> dice = BoundedProperty(1, 6)
    #>>> quad = SetProperty(4,8,12)
    #>>> zero = KnownProperty(0)
    #>>> properties = {'digit':digit, 'dice':dice, 'quad':quad, 'zero':zero}
    #>>> z = RegisterBank().zebraSetup()
    #>>> sorted(z.keys(), key='{!r:>02s}'.format)
    ['A', 'B', 'C', 'D', 'U1', 'U2', 'U3', 'U4', 'U5', 'U6', 'U7', 'V', 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    #>>> z.context.update(properties)
    #>>> z[10] = 'digit'
    #>>> z[10]
    digit:(0..9)
    #>>> z[10].asProperty()
    (0..9)

    #>>> modified = z.modify({'digit':dice})
    #>>> modified[10].asProperty()
    (1..6)

    #>>> z['J'] = z.makeExpression('iJ', BoundedProperty(), 33)
    #>>> z['J']
    iJ:(..)
    #>>> 'digit' in z
    False
    #>>> 'J' in z
    True
    #>>> z['V']
    V:(0..1)
    #>>> z['A'] = z['D'] = 17
    #>>> z.cleanupParameters()
    #>>> ','.join(sorted(z.context))
    '1,U1,U2,U3,U4,U5,U6,U7,V,digit,iB,iC,iJ,iR11,iR12,iR13,iR14,iR15,iR4,iR5,iR6,iR7,iR8,iR9'


''',

    'State': '''
    #>>> s=State(22)
    #>>> s.step()
    Start: 000000016 X022                   goto 022
    #>>> s
    State: 006034017 X023BC26
    #>>> def bounded(name, lo, hi):
    ...      "Produce a OldExpression containing a named BoundedProperty"
    ...      R = s.registers
    ...      R.context[name] = BoundedProperty(lo, hi)
    ...      R['A'] = OldExpression(name)
    ...      return R['A']
    #>>> s.computeCarry(bounded('x', 10, 19), bounded('y', -10, -3))
    (1)
    #>>> s.computeCarry(bounded('x', -19, -10), bounded('y', 3, 10))
    (?)
    #>>> s.computeCarry(bounded('x', -19, -10), bounded('y', 3, 9))
    (0)
    #>>> s.bit=FunctionBits(1<<9)
    #>>> s.computeCarry(bounded('x',0,0), bounded('y',0,0))
    (1)
''',

    'Shift': '''
    #>>> drum[100] = assemble('NR', 100)
    #>>> drum[101] = assemble('NL', 101)
    #>>> s=State(100)
    #>>> s.step()
    Start: 000000064 X100                   goto 100
    #>>> s.registers['A'] = 17
    #>>> s.registers['B'] = 3
    #>>> s.step()    #XR
     0100: 010000065 NR                     AB>>
    #>>> s.registers['A']
    17>>1:(8)
    #>>> hex(int(s.registers['B']))
    '0x100000001'
    #>>> s.step()    #XL
     0101: 020000066 NL                     AB<<
    #>>> s.registers['A']
    146028888067>>33:(17)
    #>>> s.registers['B']
    3&-2:(2)
''',

    'ParseAndCompute': '''
    #>>> R = Interpreter().registers
    #>>> P = ParseAndCompute(R)
    #>>> P.process('x')
    x
    #>>> P.process('~bit0-(bit1+2)<<3')
    -24-bit0*8-bit1*8
''',

    'Interpreter': '''
    #>>> i = Interpreter()
    #>>> [i.isNumericReg(s) for s in 'R04 R10 R23 A V'.split()]
    [True, True, False, False, False]
    #>>> i.showRegister('C')
          ->   C = 000000016 = 22
    #>>> i.assignRegister(10, 'p+(q<<2)')
          ->   R10 = p+q*4
    #>>> i.registers[10]
    p+q*4:(..)
    #>>> i.showParameter('p')
    p = (..)
    #>>> i.showMemory(22)
          ->   (022) = 006034017 X023BC26

    #>>> setCarry, assignParameter, forceJump  #doctest: +SKIP

''',
    'recognize': '''
    #>>> i = Interpreter()
    #>>> i.makeRecognizeDB()
    #>>> for x in range(6): i.step()
    Start: 000000016 X022                   goto 022
     0022: 006034017 NBC26                  B=R26
     0023: 024036018 NLB27                  AB<<; B+=R27
     0024: 024038019 NLB28                  AB<<; B+=R28
     0025: 02403a01a NLB29                  AB<<; B+=R29
     0026: 02403c01b NLB30                  AB<<; B+=R30
    #>>> i.registers['B']
    -t0b0-t0b1*2-t0b2*4-t0b3*8-t0b4*16:(-31..0)
    #>>> i.recognize()
          Rewritten: B
    #>>> i.registers['B']
    -tapeChar:(-31..0)
''',

    'OldExpression': '''
    #>>> a = OldExpression('A')
    #>>> a.context['A'] = BoundedProperty()
    #>>> x = OldExpression(-2); x.SRA(3); x
    -2>>3:(-1)
    #>>> x = OldExpression(10); x.SRL(1); x
    10>>1:(5)
''',
}

class TestUseless(TestCase):
    """Test zebra.useless"""
    def testInvalidCondition(self):
        self.assertIs(useless(assemble("X100V")), None)
        self.assertIs(useless(assemble("X100V1")), None)
        self.assertIs(useless(assemble("X100V2")), None)
        self.assertIs(useless(assemble("X100V3")), None)
        self.assertIs(useless(assemble("X100V4")), None)
        self.assertEqual(useless(assemble("X100V5")), "invalid condition")
        self.assertEqual(useless(assemble("X100V6")), "invalid condition")
        self.assertEqual(useless(assemble("X100V7")), "invalid condition")

    def testAKDEW(self):
        self.assertEqual(useless(assemble("X100")), None)
        self.assertEqual(useless(assemble("X")), "Jump to zero")
        self.assertEqual(useless(assemble("X100E")), None)
        self.assertEqual(useless(assemble("XE")), "Jump to zero")
        self.assertEqual(useless(assemble("X100D")), "Jump to zero")
        self.assertEqual(useless(assemble("XD")), "Jump to zero")
        self.assertEqual(useless(assemble("X100DE")), "Jump to zero")
        self.assertEqual(useless(assemble("XDE")), "Jump to zero")
        self.assertEqual(useless(assemble("X100K")), None)
        self.assertEqual(useless(assemble("XK4")), None)
        self.assertEqual(useless(assemble("X100KE")), None)
        self.assertEqual(useless(assemble("XE")), "Jump to zero")
        self.assertEqual(useless(assemble("X100KD")), None)
        self.assertEqual(useless(assemble("XKD")), None)
        self.assertEqual(useless(assemble("X100KDE")), None)
        self.assertEqual(useless(assemble("XKDE")), "Jump to zero")
        self.assertEqual(useless(assemble("A100")), None)
        self.assertEqual(useless(assemble("A")), None)
        self.assertEqual(useless(assemble("A100E")), None)
        self.assertEqual(useless(assemble("AE")), None)
        self.assertEqual(useless(assemble("A100D")), None)
        self.assertEqual(useless(assemble("AD")), "Write to nowhere")
        self.assertEqual(useless(assemble("A100DE")), None)
        self.assertEqual(useless(assemble("ADE")), "Write to nowhere")
        self.assertEqual(useless(assemble("A100K")), None)
        self.assertEqual(useless(assemble("AK")), None)
        self.assertEqual(useless(assemble("A100KE")), None)
        self.assertEqual(useless(assemble("AE")), None)
        self.assertEqual(useless(assemble("A100KD")), None)
        self.assertEqual(useless(assemble("AKD")), "Write to nowhere")
        self.assertEqual(useless(assemble("A100KDE")), None)
        self.assertEqual(useless(assemble("AKDE")), "Write to nowhere")


class AsmTestCase(TestCase):
    """Assemble and explain all van der Poel examples"""
    def sumAssemble(self, s, adr):
        """Assemble even with - and +"""
        result = 0
        pos = 0
        while pos < len(s):
            # find instruction end
            plusPos = s.find("+", pos)
            if plusPos < 0: plusPos = len(s)
            minusPos = s.find("-", pos)
            if minusPos < 0: minusPos = len(s)
            endPos = min(plusPos, minusPos)
            instr = assemble(s[pos:endPos], adr)
            if pos > 0 and s[pos-1] == '-': result -= instr
            else: result += instr
            pos = endPos + 1
        return result & MASK33

    def explainAsm(self, asm, *outcome):
        result = []
        adr = 100
        for line in asm.split(";"):
            instr = self.sumAssemble(line.strip().upper(), adr)
            result.append(str(adr))
            result.append(" ")
            result.append(explain(adr, *split(instr)))
            result.append("\n")
            expl = useless(instr)
            if expl:
                result.append("*** ")
                result.append(expl)
                result.append("\n")
            adr += 1
        self.assertEqual(
        	"".join(result),
        	"\n".join(outcome) +"\n")

class TestvdPoel(AsmTestCase):
    def testExplainRound(self):
        self.explainAsm(
            "nib23;nv",
            "100 B-=1/2",
            "101 A+=V")

    def testExplainQforA(self):
        self.explainAsm(
            "nbi;nv",
            "100 B-=0 (set V)",
            "101 A+=V")

    def testExplainRepeat(self):
        self.explainAsm(
            "n;nke6;x5k7",
            "100 No operation",
            "101 R6=D",
            "102 next=R5 (rept 7)")

    def testExplainMul(self):
        self.explainAsm(
            "ne5;nkkce15;alr;nke6lrc;x5k15lr;nlri",
            "100 R5=A",
            "101 R15=A;A=[102];next=D;D=R4",
            "102 Tb=B;AB>>;A+=R15*(Tb&1);next=D;D=R4 (fast)",
            "103 R6=D;A=R15*(B&1);B/=2",
            "104 Tb=B;AB>>;A+=R15*(Tb&1);next=R5 (rept 15)",
            "105 Tb=B;AB>>;A-=R15*(Tb&1)")

    def testExplainDivision(self):
        "AB/-R15"
        self.explainAsm(
            "nce6;nkkc;aqi15v1;nke7;x6k31qld;n",
            "100 R6=A;A=0",
            "101 A=[102];next=D;D=R4",
            "102 if A<0:A-=R15;B--;next=D;D=R4 (fast)",
            "103 R7=D",
            "104 AB<<;A+=R15;B++;next=R6 (rept 31)",
            "105 No operation")

    def testExplainNormalise(self):
        self.explainAsm(
            "nr23;nke6v1;nke6;xk6l",
            "100 AB>>;A+=1/2",
            "101 if A<0:R6=D",
            "102 R6=D",
            "103 AB<<;next=R6 (fast)")

    def testExplainBlockmove(self):
        self.explainAsm(
            "nc5;nkkbc;x002.1;nkkbce15;a200ce5;nke4;x3k6bd",
            "100 A=R5",
            "101 B=[102];next=D;D=R4",
            "102 A+=1;goto 002",
            "103 R15=B;B=[104];next=D;D=R4",
            "104 R5=A;A=[200];next=D;D=R4",
            "105 R4=D",
            "106 next=B;B+=R15 (rept 6)")

    def testExplainZeroSearch(self):
        self.explainAsm(
            "nkkbc;a200cq;nke4icv;x3k50qv3;x150v3;n;nc6;nk3qibc;ad000k3q-a002cq;n",
            "100 B=[101];next=D;D=R4",
            "101 A=[200];B++;next=D;D=R4",
            "102 R4=D;A=-1+V",
            "103 if A!=0:next=B;B++ (rept 50)",
            "104 if A!=0:goto 150",
            "105 No operation",
            "106 A=R6",
            "107 next=[108]+B;B=-1",
            "108 [8190]=B;A=A/2-R15*(B&1);B=-R15-1;next=X000",
            "*** Jump to zero",
            "109 No operation")

    def testExplainSearchList(self):
        self.explainAsm(
            "nkkbc;a200qe24;nke4icv;x3k50qcdv3;x150v3",
            "100 B=[101];next=D;D=R4",
            "101 A+=([200]&R5);B++;next=D;D=R4",
            "102 R4=D;A=-1+V",
            "103 if A!=0:next=B;A=R15;B++ (rept 50)",
            "104 if A!=0:goto 150")

    def testExplainRandom(self):
        self.explainAsm(
            "nbe5;nkkbc;alrce15;nke6bc;x5k20dq",
            "100 R5=B",
            "101 B=[102];next=D;D=R4",
            "102 Ta=A;A=R15*(B&1);B/=2;R15=Ta;next=D;D=R4 (fast)",
            "103 R6=D;B=0",
            "104 A+=R15;B++;next=R5 (rept 20)")

    def testExplainRepeatRoutine(self):
        self.explainAsm(
            "nke7;x103ke5;x110;nke6;x6k10;n;n;n;n;xk5;n",
            "100 R7=D",
            "101 R5=D;goto 103",
            "102 goto 110",
            "103 R6=D",
            "104 next=R6 (rept 10)",
            "105 No operation",
            "106 No operation",
            "107 No operation",
            "108 No operation",
            "109 next=R5 (fast)",
            "110 No operation")

    def testExplainClearDrum(self):
        self.explainAsm(
            "nce15;a103bcke4;n;x000k3qcd",
            "100 R15=A;A=0",
            "101 Tr=R4;R4=D;B=[103];next=D;D=Tr",
            "102 No operation",
            "103 next=B;[000]=A;A=R15;B++")

    def testExplainClassSort(self):
        self.maxDiff = None
        self.explainAsm(
            "n;nkkbc;x114;nkkbce4;a108k3qv1-x114;nk3q;ak23-a108k3qv1+x114;"
            "n;x010;x020;x030;a000-x040;n",
            "100 No operation",
            "101 B=[102];next=D;D=R4",
            "102 goto 114",
            "103 Tr=R4;R4=B;B=[104];next=D;D=Tr",
            "104 if A<0:next=D+A;D=R4;A+=[8186];B++",
            "105 next=[106]+B;B++",
            "106 if not start:A=A/2-R15*(B&1);B=-0;next=D;D=R4 (fast)",
            "*** Write to nowhere",
            "107 No operation",
            "108 goto 010",
            "109 goto 020",
            "110 goto 030",
            "111 if ?:A=A/2-R15*(B&1);B=-R15-1;next=X000 (rept 20)",
            "*** invalid condition",
            "112 No operation")

    def testExplainSumStore(self):
        self.explainAsm(
            "a000ic;n;nkkbc;x54;nkkbce4;a200k3q-x54;"
                "xk3q;a109k23qic-a200k3q+x54;n",
            "100 A=-[000];next=D;D=R4",
            "101 No operation",
            "102 B=[103];next=D;D=R4",
            "103 goto 054",
            "104 Tr=R4;R4=B;B=[105];next=D;D=Tr",
            "105 next=D+B;D=R4;A+=[146];B++",
            "106 next=B;B++ (fast)",
            "107 A=-0;goto 8155",
            "108 No operation")

    def testExplainDisplace(self):
        self.explainAsm(
            "nkkc3;xd001k2qv3-x109k3q;nkkb;ac000q;nke4;x109k3q;n;x000k2",
            "100 A=[101]+B;next=D;D=R4",
            "101 if B<0:A+=R15;next=X000 (rept 54)",
            "*** Jump to zero",
            "102 B+=[103];next=D;D=R4",
            "103 A=[000];B++;next=D;D=R4",
            "104 R4=D",
            "105 next=[109]+B;B++",
            "106 No operation",
            "107 next=[000]+A")

    def testExplainFastDivision(self):
        self.explainAsm(
            "nbe5;nkkbc;xk5qdv1;nke4bc;xk5qd;nI15Q",
            "100 R5=B",
            "101 B=[102];next=D;D=R4",
            "102 if A<0:A+=R15;B++;next=R5 (fast)",
            "103 R4=D;B=0",
            "104 A+=R15;B++;next=R5 (fast)",
            "105 A-=R15;B--")

    def testExplainThreeWord(self):
        self.explainAsm(
            "nqibe6;nkkib;nkkbde61v7;nqie1;nkqie4;nkib3;nki",
            "100 R6=B;B-=1",
            "101 B-=[102];next=D;D=R4",
            "102 if ?:next=D;D=R4 (fast)",
            "*** invalid condition",
            "103 B--",
            "104 R4=D;B--",
            "105 next=[106]+B;B-=0 (set V)",
            "106 No operation")

    def testExplainFourWord(self):
        self.explainAsm(
            "nbe4;nkk;a000ce4;nkkbc;aqbe4;nkkbck23;a110bce4;x001-x000bc;"
            "n;n;x114;x001b;x001.1-x000b;x001b1;n",
            "100 R4=B",
            "101 A+=[102];next=D;D=R4",
            "102 Tr=R4;R4=A;A=[000];next=D;D=Tr",
            "103 B=[104];next=D;D=R4",
            "104 Tr=R4;R4=B;B+=1;next=D;D=Tr (fast)",
            "105 B=[106];next=D+1/2;D=R4",
            "106 Tr=R4;R4=B;B=[110];next=D;D=Tr",
            "107 A=-[001]-R15*(B&1);B=B/2-1;next=D;D=R4",
            "108 No operation",
            "109 No operation",
            "110 goto 114",
            "111 goto 001",
            "112 Tb=B;AB>>;A-=R15*(Tb&1);B-=[001]+1;next=D+1;D=R4",
            "113 B+=1;goto 001",
            "114 No operation")

    def testExplainSmallFactors(self):
        self.explainAsm(
            "nlc3;al2;n;nlc3;nlb3;nlb3;nlib2;n;nlc3;nlb3;nlb3;nlib2",
            "100 A=B;B*=2",
            "101 Ta=A;AB<<;A+=Ta;next=D;D=R4 (fast)",
            "102 No operation",
            "103 A=B;B*=2",
            "104 Tb=B;AB<<;B+=Tb",
            "105 Tb=B;AB<<;B+=Tb",
            "106 Ta=A;AB<<;B-=Ta",
            "107 No operation",
            "108 A=B;B*=2",
            "109 Tb=B;AB<<;B+=Tb",
            "110 Tb=B;AB<<;B+=Tb",
            "111 Ta=A;AB<<;B-=Ta")

    def testExplainDialer(self):
        self.explainAsm(
            "n;n;n;n;xk14qcd;a;x011k3;ad000;x5k96u7;ae15;n;xk11bcu7;"
            "x017.3;x9k2400cbu7;x7k8d;x000",
            "100 No operation",
            "101 No operation",
            "102 No operation",
            "103 No operation",
            "104 A=R15;B++;next=R14 (fast)",
            "105 next=D;D=R4 (fast)",
            "106 next=[011]+B",
            "107 [000]=A;next=D;D=R4",
            "108 if not start:next=R5 (rept 96)",
            "109 R15=A;next=D;D=R4 (fast)",
            "110 No operation",
            "111 if not start:B=0;next=R11 (fast)",
            "112 A+=B;goto 017",
            "113 if not start:B=0;next=R9 (fast)",
            "114 A+=R15;next=R7 (rept 8)",
            "115 goto 000")

    def testExplainTrack0(self):
        self.maxDiff = None
        self.explainAsm(
            "x000ke4u7;x0003lib26;x020e4;nlib26;nlib27;nlib28;nlib29;nlib30;"
            "nl31v1;x001rv1;nrb31;x012rb9v4;x12k5;x11k1iv;nle28q;nle29;nle30;"
            "ne31;x13k1;nkkbck3;x001bce11v1;x029u6;x019be12;"
            "nbc26;nlb27;nlb28;nbl29;nlb30;n31q;x32k3qibcv2;x34u7;x018ic1;x8191",
            "100 if not start:R4=D;goto 000",
            "101 AB<<;B-=tapebit4;goto 003",
            "102 R4=A;goto 020",
            "103 AB<<;B-=tapebit4",
            "104 AB<<;B-=tapebit3",
            "105 AB<<;B-=tapebit2",
            "106 AB<<;B-=tapebit1",
            "107 AB<<;B-=tapebit0",
            "108 if A<0:AB<<;advance tape",
            "109 if A<0:AB>>;goto 001",
            "110 AB>>;advance tape",
            "111 if B&1:AB>>;B+=R9;goto 012",
            "112 next=R12 (rept 5)",
            "113 A-=1-V;next=R11 (rept 1)",
            "114 AB<<;B++",
            "115 AB<<",
            "116 AB<<",
            "117 No operation",
            "118 next=R13 (rept 1)",
            "119 next=D+B;D=R4;B=[120]",
            "120 if A<0:R11=B;B=0;goto 001",
            "121 if U6:goto 029",
            "122 R12=B;goto 019",
            "123 B=tapebit4",
            "124 AB<<;B+=tapebit3",
            "125 AB<<;B+=tapebit2",
            "126 AB<<;B+=tapebit1",
            "127 AB<<;B+=tapebit0",
            "128 B++;advance tape",
            "129 if B<0:next=[032]+B;B=-1",
            "130 if not start:goto 034",
            "131 A=-1;goto 018",
            "132 goto 8191")

class TestExplainAll(AsmTestCase):
    def testExplainA(self):
        "combinations of 0,1,2,3,e15 with b, c"
        self.explainAsm(
            "n0;n1;n2;n3;ne15;"
            "nc0;nc1;nc2;nc3;nce15;"
            "nb0;nb1;nb2;nb3;nbe15;"
            "nbc0;nbc1;nbc2;nbc3;nbce15",
            "100 No operation",
            "101 A+=1",
            "102 A+=A",
            "103 A+=B",
            "104 R15=A",
            "105 A=0",
            "106 A=1",
            "107 A=A",
            "108 A=B",
            "109 R15=A;A=0",
            "110 No operation",
            "111 B+=1",
            "112 B+=A",
            "113 B+=B",
            "114 R15=B",
            "115 B=0",
            "116 B=1",
            "117 B=A",
            "118 No operation",
            "119 R15=B;B=0")

    def testExplainL(self):
        "combinations of 0,1,2,3,e15 with b, c, all with L"
        self.maxDiff = None
        self.explainAsm(
            "nl0;nl1;nl2;nl3;nle15;"
            "nlc0;nlc1;nlc2;nlc3;nlce15;"
            "nlb0;nlb1;nlb2;nlb3;nlbe15;"
            "nlbc0;nlbc1;nlbc2;nlbc3;nlbce15",
            "100 AB<<",
            "101 AB<<;A+=1",
            "102 Ta=A;AB<<;A+=Ta",
            "103 Tb=B;AB<<;A+=Tb",
            "104 R15=A;AB<<",
            "105 A=0;B*=2",
            "106 A=1;B*=2",
            "107 A=A;B*=2",
            "108 A=B;B*=2",
            "109 R15=A;A=0;B*=2",
            "110 AB<<",
            "111 AB<<;B+=1",
            "112 Ta=A;AB<<;B+=Ta",
            "113 Tb=B;AB<<;B+=Tb",
            "114 R15=B;AB<<",
            "115 B=0;A*=2",
            "116 B=1;A*=2",
            "117 B=A;A*=2",
            "118 A*=2",
            "119 R15=B;B=0;A*=2")

    def testExplainQ(self):
        "combinations of 0,1,2,3,e15 with b, c, all with Q"
        self.explainAsm(
            "n0q;n1q;n2q;n3q;ne15q;"
            "nc0q;nc1q;nc2q;nc3q;nce15q;"
            "nb0q;nb1q;nb2q;nb3q;nbe15q;"
            "nbc0q;nbc1q;nbc2q;nbc3q;nbce15q",
            "100 B++",
            "101 A+=1;B++",
            "102 A+=A;B++",
            "103 A+=B;B++",
            "104 R15=A;B++",
            "105 A=0;B++",
            "106 A=1;B++",
            "107 A=A;B++",
            "108 A=B;B++",
            "109 R15=A;A=0;B++",
            "110 B+=1",
            "111 B+=1+1",
            "112 B+=A+1",
            "113 B+=B+1",
            "114 R15=B;B+=1",
            "115 B=1",
            "116 B=1+1",
            "117 B=A+1",
            "118 B=B+1",
            "119 R15=B;B=1")

    def testExplainLQ(self):
        "combinations of 0,1,2,3,e15 with b, c, all with LQ"
        self.maxDiff = None
        self.explainAsm(
            "nl0q;nl1q;nl2q;nl3q;nle15q;"
            "nlc0q;nlc1q;nlc2q;nlc3q;nlce15q;"
            "nlb0q;nlb1q;nlb2q;nlb3q;nlbe15q;"
            "nlbc0q;nlbc1q;nlbc2q;nlbc3q;nlbce15q",
            "100 AB<<;B++",
            "101 AB<<;A+=1;B++",
            "102 Ta=A;AB<<;A+=Ta;B++",
            "103 Tb=B;AB<<;A+=Tb;B++",
            "104 R15=A;AB<<;B++",
            "105 A=0;B=B*2+1",
            "106 A=1;B=B*2+1",
            "107 A=A;B=B*2+1",
            "108 A=B;B=B*2+1",
            "109 R15=A;A=0;B=B*2+1",
            "110 AB<<;B+=1",
            "111 AB<<;B+=1+1",
            "112 Ta=A;AB<<;B+=Ta+1",
            "113 Tb=B;AB<<;B+=Tb+1",
            "114 R15=B;AB<<;B+=1",
            "115 B=1;A*=2",
            "116 B=1+1;A*=2",
            "117 B=A+1;A*=2",
            "118 B=B+1;A*=2",
            "119 R15=B;B=1;A*=2")

    def testExplainLR(self):
        "combinations of 0,1,2,3,e15 with b, c, all with LR"
        self.maxDiff = None
        self.explainAsm(
            "nlr0;nlr1;nlr2;nlr3;nlre15;"
            "nlrc0;nlrc1;nlrc2;nlrc3;nlrce15;"
            "nlrb0;nlrb1;nlrb2;nlrb3;nlrbe15;"
            "nlrbc0;nlrbc1;nlrbc2;nlrbc3;nlrbce15",
            "100 Tb=B;AB>>;A+=R15*(Tb&1)",
            "101 Tb=B;AB>>;A+=R15*(Tb&1)",
            "102 Tb=B;AB>>;A+=R15*(Tb&1)",
            "103 Tb=B;AB>>;A+=R15*(Tb&1)",
            "104 Ta=A;Tb=B;AB>>;A+=R15*(Tb&1);R15=Ta",
            "105 A=R15*(B&1);B/=2",
            "106 A=R15*(B&1);B/=2",
            "107 A=R15*(B&1);B/=2",
            "108 A=R15*(B&1);B/=2",
            "109 Ta=A;A=R15*(B&1);B/=2;R15=Ta",
            "110 Tb=B;AB>>;A+=R15*(Tb&1)",
            "111 Tb=B;AB>>;A+=R15*(Tb&1);B+=1",
            "112 Ta=A;Tb=B;AB>>;A+=R15*(Tb&1);B+=Ta",
            "113 Tb=B;AB>>;A+=R15*(Tb&1);B+=Tb",
            "114 Tb=B;AB>>;A+=R15*(Tb&1);R15=Tb",
            "115 A=A/2+R15*(B&1);B=0",
            "116 A=A/2+R15*(B&1);B=1",
            "117 Ta=A;A=A/2+R15*(B&1);B=Ta",
            "118 A=A/2+R15*(B&1)",
            "119 A=A/2+R15*(B&1);R15=B;B=0")

    def testExplainLRQ(self):
        "combinations of 0,1,2,3,e15 with b, c, all with LRQ"
        self.maxDiff = None
        self.explainAsm(
            "nlr0q;nlr1q;nlr2q;nlr3q;nlre15q;"
            "nlrc0q;nlrc1q;nlrc2q;nlrc3q;nlrce15q;"
            "nlrb0q;nlrb1q;nlrb2q;nlrb3q;nlrbe15q;"
            "nlrbc0q;nlrbc1q;nlrbc2q;nlrbc3q;nlrbce15q",
            "100 Tb=B;AB>>;A+=R15*(Tb&1);B++",
            "101 Tb=B;AB>>;A+=R15*(Tb&1);B++",
            "102 Tb=B;AB>>;A+=R15*(Tb&1);B++",
            "103 Tb=B;AB>>;A+=R15*(Tb&1);B++",
            "104 Ta=A;Tb=B;AB>>;A+=R15*(Tb&1);B++;R15=Ta",
            "105 A=R15*(B&1);B=B/2+1",
            "106 A=R15*(B&1);B=B/2+1",
            "107 A=R15*(B&1);B=B/2+1",
            "108 A=R15*(B&1);B=B/2+1",
            "109 Ta=A;A=R15*(B&1);B=B/2+1;R15=Ta",
            "110 Tb=B;AB>>;A+=R15*(Tb&1);B+=1",
            "111 Tb=B;AB>>;A+=R15*(Tb&1);B+=1+1",
            "112 Ta=A;Tb=B;AB>>;A+=R15*(Tb&1);B+=Ta+1",
            "113 Tb=B;AB>>;A+=R15*(Tb&1);B+=Tb+1",
            "114 Tb=B;AB>>;A+=R15*(Tb&1);B+=1;R15=Tb",
            "115 A=A/2+R15*(B&1);B=1",
            "116 A=A/2+R15*(B&1);B=1+1",
            "117 Ta=A;A=A/2+R15*(B&1);B=Ta+1",
            "118 A=A/2+R15*(B&1);B=B+1",
            "119 A=A/2+R15*(B&1);R15=B;B=1")

    def testExplainR15(self):
        "XD and LR side effects"
        # examples: ALRCE15, XLRE15, XKLRE15
        self.explainAsm(
            "alrce15;alrce14;a200lrce15;xlre15;xklre15;xlre15",
            "100 Ta=A;A=R15*(B&1);B/=2;R15=Ta;next=D;D=R4 (fast)",
            "101 R14=A;A=R15*(B&1);B/=2;next=D;D=R4 (fast)",
            "102 Ta=A;A=[200]+R15*(B&1);B/=2;R15=Ta;next=D;D=R4",
            "103 Ta=A;Tb=B;AB>>;A+=R15*(Tb&1);R15=Ta;next=X000 (fast)",
            "*** Jump to zero",
            "104 Ta=A;Tb=B;AB>>;A+=R15*(Tb&1);R15=D;next=X000 (fast)",
            "*** Jump to zero",
            "105 Ta=A;Tb=B;AB>>;A+=R15*(Tb&1);R15=Ta;next=X000 (fast)",
            "*** Jump to zero")

    def testExplainAE4(self):
        "CE4 with variants of B,C"
        self.explainAsm(
            "ae4;abe4;ace4;abce4",
            "100 Tr=R4;R4=A;next=D;D=Tr (fast)",
            "101 Tr=R4;R4=B;next=D;D=Tr (fast)",
            "102 Tr=R4;R4=A;A=0;next=D;D=Tr (fast)",
            "103 Tr=R4;R4=B;B=0;next=D;D=Tr (fast)")

    def testOptimized(self):
        "Optimized seldom used commands"
        self.explainAsm(
            "nlrbce15;nlrbceq15;a200lrbce15",
            "100 A=A/2+R15*(B&1);R15=B;B=0",
            "101 A=A/2+R15*(B&1);R15=B;B=1",
            "102 A=A/2+R15*(B&1);R15=B;B=[200];next=D;D=R4")

if __name__ == "__main__":
    main()
