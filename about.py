#!python3
"""About boxes for zebra program.
"""

import os
import sys
import webbrowser

import tkinter.tix as tk
from tkinter.font import Font

#find our local files
CURDIR = os.path.dirname(sys.executable if hasattr(sys, 'frozen') else __file__)
IMAGEDIR = os.path.join(CURDIR, 'images/')

class HyperlinkManager:
    """From http://effbot.org/zone/tkinter-text-hyperlink.htm
    Added web option
    >>> text = Text(...)
    >>> hyperlink = HyperlinkManager(text)
    >>> text.insert(INSERT, "this is a link", hyperlink.add(callback),
    ...     hyperlink.web("http://www.google.nl"), hyperlink.add(),
    ...     "text", 'tag')
    """

    def __init__(self, text):

        self.text = text

        self.text.tag_config("hyper", foreground="blue", underline=1)

        self.text.tag_bind("hyper", "<Enter>", self._enter)
        self.text.tag_bind("hyper", "<Leave>", self._leave)
        self.text.tag_bind("hyper", "<Button-1>", self._click)

        self.reset()

    def reset(self):
        self.links = {}

    def add(self, action=None):
        # add an action to the manager.  returns tags to use in
        # associated text widget
        if action is None:
            #make web link to url set by .web()
            url = self.url
            action = lambda:webbrowser.open(url)
        tag = "hyper-%d" % len(self.links)
        self.links[tag] = action
        return "hyper", tag

    def _enter(self, event):
        self.text.config(cursor="hand2")

    def _leave(self, event):
        self.text.config(cursor="")

    def _click(self, event):
        for tag in self.text.tag_names(tk.CURRENT):
            if tag[:6] == "hyper-":
                self.links[tag]()
                return

    def web(self, url, text=None):
        """Add a link at the end of text, with a callback to webbrowser.open
        """
        self.url = url
        return url if text is None else text

def setRoot(root):
    """Set screen width and height for optimizing layout"""
    global screenWidth, screenHeight
    screenWidth = root.winfo_screenwidth()
    screenHeight = root.winfo_screenheight()

class AboutBox(tk.Toplevel):
    """General about box, to be subclassed.
    You only have to provide the fill method.
    """
    def __init__(self, width=800):
        super().__init__()
        #setup a window with scrollbars
        scrolledText = tk.ScrolledText(self,
            height=screenHeight*3//4,
            width=min(width, screenWidth*3//4),
            scrollbar=tk.Y)
        scrolledText.pack(fill=tk.BOTH, expand=True)
        self.text = text = scrolledText.text

        #setup the fonts and styles
        standardFont = Font(family="Helvetica", size=11)
        text.config(font=standardFont, wrap=tk.WORD)
        italicFont = Font(family="Helvetica", size=11, slant="italic")
        boldFont = Font(family="Helvetica", size=11, weight="bold")
        headingFont = Font(family="Helvetica", size=14, weight="bold")
        BULLET, CITATION, BOLD, HEADING = 'BULLET', 'CITATION', 'BOLD', 'HEADING'
        text.tag_config(BULLET, lmargin2=25)
        text.tag_config(CITATION, font=italicFont)
        text.tag_config(BOLD, font=boldFont)
        text.tag_config(HEADING, font=headingFont)

        #key bindings
        self.bind("<Escape>", lambda e:self.destroy())
        self.bind("<Return>", lambda e:self.destroy())

        self.fill()
        #at a line and a button at the bottom
        text.insert(tk.END,
            "\nThis program is written by Jurjen Bos: jnebos@gmail.com   ",
            CITATION)
        text.window_create(tk.END, window=tk.Button(self,
            text="OK", command=self.destroy))
        self.focus()

    def fill(self):
        """Here you can write, using the four styles.
        Don't forget to store the pictures."""
        self.title("AboutBox")
        text = self.text
        BULLET, CITATION, BOLD, HEADING = 'BULLET', 'CITATION', 'BOLD', 'HEADING'
        #add text
        text.insert(tk.END,
            "Blahblah", HEADING)
        #or image
        text.image_create(tk.END, image=self.picture1)
        #or hyperlink with HyperlinkManager

class AboutThisProgram(AboutBox):
    def __init__(self):
        super().__init__(500)
    def fill(self):
        self.title("About this program")
        text = self.text
        hyperlink = HyperlinkManager(text)
        BULLET, CITATION, BOLD, HEADING = 'BULLET', 'CITATION', 'BOLD', 'HEADING'
        self.willemPic = tk.PhotoImage(file=IMAGEDIR+'Willem.gif')
        text.image_create(tk.END, image=self.willemPic)
        text.insert(tk.END,
            "\nAbout 30 years ago, I met Willem van der Poel (shown here at age 90) during a dinner of an event. I had a nice conversation, were he explained some of the design ideas behind the Zebra computer. This brief encounter planted a seed in my brain that resulted in a search for information about his design. This project went a bit too far, and this program is the result of that endeavour. So, here it is: an emulator for the very very old computer (1954), the Stantec Zebra.\n"

            "\nDesigned by Willem Louis van der Poel, the design of this computer used a stunningly small number of parts, still it was a relatively successful product: 55 were sold, mainly in Britain and the Netherlands.\n"

            "The main reason for writing this program for me was to really understand how the design worked, and to really enjoy it.\n"

            "In 1995, Don Hunter and Willem van der Poel himself wrote an emulator for this machine in Pascal. It is available on the web site of the awesome ", (),
            hyperlink.web("http://www.computerconservationsociety.org/software/zebra/base.htm", "Computer Conservation Society"), hyperlink.add(),
            ". This emulator unfortunately doesn't run under Windows anymore; you need another emulator (DosBox) for that. This emulator inspired me to write this program in Python. This program does everything the original emulator did, and tries to be more accurate.  One important improvement is that it does emulate real Zebra time to get the original programmer's frustration. Of course, there is an option to run it at full machine speed (about ten times faster on my machine) if you are impatient.\n\n", (),
            )

class AboutZebra(AboutBox):
    def fill(self):
        self.title("About the Stantec Zebra")
        #pictures found at
        #http://www.alanflavell.org.uk/zebra/zebra.jpg
        #and http://www.stlqcc.org.uk/images/zebracontrol_200.jpg
        self.zebraPic = tk.PhotoImage(file=IMAGEDIR+'Stantec Zebra.gif')
        self.controlPic = tk.PhotoImage(file=IMAGEDIR+'Zebra controls.gif')
        self.drumPic = tk.PhotoImage(file=IMAGEDIR+'Drum.gif')

        text = self.text
        hyperlink = HyperlinkManager(text)
        BULLET, CITATION, BOLD, HEADING = 'BULLET', 'CITATION', 'BOLD', 'HEADING'

        #text.insert alternates text and type of the preceding text
        text.insert(tk.END,
            "Introduction\n", HEADING,
            "\nThis is what the Zebra looked like:\n"
            )
        text.image_create(tk.END, image=self.zebraPic)
        text.insert(tk.END,
            "\nThe big cabinets in the back contained the actual computer; the desk in the front is just for I/O.\n"

            "\nIt was a 33 bit machine with 8192K words of memory (equivalent to 33 Kbyte), stored on a drum running at 6000RPM.  Thanks to the ingenuity of the design, the electronics consisted of only a handful of latches and adders. The accumulators used a special area of the drum where the read and write head are close together.\n"

            '\nThis design predated many things we now take for granted: not only this this computer not have a stack, in a sense it did have only one instruction and no program counter! Instead, there we 15 "function bits" that modified the operation of an instructions. Since every instruction contained the address of the next, a program counter was not needed. Multiplication was possible using a clever "repeat" construct, that allowed to repeat one or two instructions at full speed (that is, 3200 instructions per second). This saved a lot of hardware. Choose "About the Stantec Zebra" to learn more about the computer and its design.\n', (),

            "\nDesign principles\n", HEADING,
            "The design principles of the Zebra were, as stated by the original designer, Willem van der Poel:\n", (),
            "1. The logical structure of the arithmetic and control units of the machine have been simplified as much as possible; there is not even a built-in multiplier nor a divider.\n"
            "2. The separate bits in an instruction word are used functionally and can be put together in any combination.\n"
            "3. Conventional two stage operation (set-up, execution) has been abandoned. Each unit time interval can be used for arithmetical operations.\n"
            "4. A small number of fast access registers is used as temporary storage; at the same time these registers serve as modifier registers (B-lines).\n"
            "5. Optimum programming is almost automatically done to a very great extent. The percentage of word times effectively used is usually greater than 60%.\n"
            "6. An instruction can be repeated and modified while repeated by using an accumulator as next instruction source and the address counter as counter. This can be done without any special hardware. This has resulted in a machine which has a very simple structure and hence contains only a very moderate number of components, giving high reliability and easy maintenance.\n",
            (CITATION, BULLET),
            "Because of the functional bit coding, the programming is extremely flexible. In fact the machine code is a sort of micro-programming. Full-length multiplication or half-length multiplication in half the time are just as easy, only require a different micro-programme. The minimum latency programming together with the effective use of word times lost in other systems results in a very high speed of operation compared to the basic clock pulse frequency.\n",
            CITATION,

            "\nDetailed description\n", HEADING,
            "The rest of the description will be in my own words, using more modern terminology. For example, the bit numbering is changed into the modern notation: most significant bit is 32, least significant is 0.\n"
            "\nMemory and registers of this machine are all stored on a magnetic drum. All operations were serial, processing data while reading and/or writing to the drum. This reduced the electronic components to a minimum.\n"
            "The drum had separate read and write heads for every track, so there were quite a few cables. It looked like this:\n", '')
        text.image_create(tk.END, image=self.drumPic)
        text.insert(tk.END,
            "\n\nThe machine has two accumulators A and B, and a register that collects the next instruction C. Furthemore, there are 12 general purpose registers named R4 through R15, and a few special purpose registers for I/O operations. All register consisted of the unlikely number of 33 bits; accumulator A has an extra overflow bit making it 34 bits.\n"
            "An instruction starts with copying C into a set of latches E that control the operation of the instruction. Then instruction is executed in serial fashion, routing the bits through five adders into the accumulators. This routing of bits is controlled by the fifteen function bits.\n"
            "An instruction is encoded as the fifteen function bits, five bits encoding the register number and 13 bits for the drum address (eight for the track number and five for the position on the track), totaling 33 bits. This is also visible on the control panel switches.\n"
            "The action of the instructions is most easily explained by treating the bits separately. All these actions are performed simulatenously, leading to surprising complex instructions.\n"
            "If all functional bits are cleared, the instruction consists of adding the given register to accumulator A, and loading register C with the given drum address (you ", (),
            "could", CITATION,
            "call this a jump, but it isn't, since the value can be modified; see below).\n"
            "Each of the registers A,B, and C are fed from an adder, that may get additional inputs, depending on the function bits. Register D is filled with the previous value of register C, plus 2. (Why this is the useful, will be explained below; this is one of these strange yet ingenious aspects of the Zebra design.)\n"
            'The regular registers are numbered 4 through 15. Registers R0 through R3 have special meaning: 0 is zero, 1 is one, 2 is the value of A, and 3 is the value of B. Writing to these registers has no effect. Registers 16 through 31 have special functions; you could call this "register mapped I/O".\n'
            "In order of function word, from most significant bit (32) to least significant (18), the operation of the function bits is:\n", (),

            "\nBit A: ", BOLD,
            "sends the output of the drum selector to the accumulator (A or B) instead of to the next instruction register C. Register C now is fed with register D instead. Register D is loaded in its turn with R4. This strange behaviour is one of the most fundamental design choices of the Zebra. As a result of this, if an A instruction is executed after a non-A instruction, the previous instruction is executed ", BULLET,
            "again", (BULLET, CITATION),
            " but incremented by 2, which conveniently is the drum address that the drum rotated to in the mean time. Also, this behaviour can be used to repeat instructions: see below.\n", BULLET,
            "Bit K: ", BOLD,
            "the counterpart of bit A: it sends the output of the register selector to the C register (adding it to the memory contents or register D contents, dependent on the A bit). This makes it possible to do indexed jumps, array fetches, and executing registers in combination with the W bit, for repeated instructions.\n",
            BULLET,
            "Bit Q: ", BOLD,
            "increments register B.\n", BULLET,
            "Bit L: ", BOLD,
            "shift A and B to the left as a double accumulator before doing additions. If C is set, shifts the accumulators separately.\n", BULLET,
            "Bit R: ", BOLD,
            "shift A and B to the left as a double accumulator before doing additions. If C is set, shifts the accumulators separately. R precedes over L; if R and L are both set, the multiply functionality springs into action: R15 is added to the selected accumulator if bit 0 of B is set.\n", BULLET,
            "Bit I: ", BOLD,
            "subtract instead of add. This influences everything that is added to the accumulators; for example I and Q together subtract one from B\n", BULLET,
            "Bit B: ", BOLD,
            "Use accumulator B instead of A for the result of the operation. Note that L and R are not affected.\n", BULLET,
            "Bit C: ", BOLD,
            "Clear the relevant accumulator before addition or subtraction: this turns the addition into a load, and a subtraction into a load negative. The other accumulator is unaffected.\n", BULLET,
            "Bit D: ", BOLD,
            "Write to the drum instead of reading it. The read value is 0. The value written is the original register value.\n", BULLET,
            "Bit E: ", BOLD,
            "Write to the given register instead of reading it. The read value is 0. If bit A is set, the value written is that of register D (note: this is different from the D bit!)\n", BULLET,
            "Bit V: ", BOLD,
            'Add the carry bit to A. If L is set and R is not set, add two times the carry bit. When subtracting, interpret 0 as "borrow" and 1 as "no borrow". The carry bit was set by any instruction with a B or a Q from the addition to the B register, allowing double precision additions into AB.\n',
            BULLET,
            "Bits 4,2,1: ", BOLD,
            "Set condition; if the condition fails do not execute the instruction, but execute AW instead. If V is not set, read the corresponding key on the front panel. U1 through U6 are flip switches, U7 is a bottow that is OFF when pressed. If V is set, you get the following conditions: V1:A<0; V2:B<0; V3:A!=0; V4:B is odd. V5,V6,V7 always fail.\n", BULLET,
            "Bit W: ", BOLD,
            "Do not wait for the drum; instead read 0. This is very useful for repeated instructions such as multiplication, division and normalisation.\n",
            BULLET,

            "\nNotation of instructions\n", HEADING,
            'Instructions are written by naming the instruction letters in any order. The drum address and register address are interspersed, where the letters can be used as separator, or a period. bit A is written first; if missing, the instruction is written starting with an X. An X to the next address is writted as N. A load of the next address is written as NKK, which as a result of the way instructions are assembled will work (K+K=A, see?). The W bit cannot be encoded, as there is no W character in the tape alphabet; instead, if the register number is written first, the second number is a "repeat count", where the assembler multiplies the value with -2 and encodes this as address.\n', (),

            "\nRepeat instructions\n", HEADING,
            "The design of the Zebra allowed repeating instructions with minimal hardware support. The idea is: if you execute an XKW instruction, the next instruction will be from the register in that instruction. If this is an AW instruction, the instruction after that will be two more than the XKW instruction, having the same effect (because W ignore the drum address) unless the drum address overflows into the register address. If that happens, the next register is executed, ending the repetition.\n", (),
            "For the repeat instructions, a few tiny ingenious tweaks allow to do for example multiplication and division:\n", (),
            "Bit LR: ", BOLD,
            "Shift left, and if the lower bit of B is set (that is, B is odd), add or subtract register 15 to A instead of the given value.\n",
            BULLET,
            "Bit XD: ", BOLD,
            "Instead of the given value, add or subtract the value of register 15 to a or B depending on the B bit.\n",
            BULLET,

            "\nSpecial registers\n", HEADING,
            "R23: ", BOLD,
            "Contains the constant 2^32: only the highest bit (the sign bit) is set.\n",
            BULLET,
            "R24: ", BOLD,
            "For doing the logical AND operation. Reading it reads the logical AND of R5 and the current accumulator. More confusingly, writing it influences the value read from memory in that same instruction, which reads as that value AND R5.\n", BULLET,
            "R25: ", BOLD,
            "Printer.\n", BULLET,
            "R26: ", BOLD,
            'Reads bit 4 of the character in the tape reader. The value is read in all bits, so -1 means "hole" and 0 means "no hole". Writing it sets the corresponding bit of the tape punch buffer to the sign bit of the value written.\n',
            BULLET,
            "R27: ", BOLD,
            "Bit 3 of tape reader or punch.\n", BULLET,
            "R28: ", BOLD,
            "Bit 2 of tape reader or punch.\n", BULLET,
            "R29: ", BOLD,
            "Bit 1 of tape reader or punch.\n", BULLET,
            "R30: ", BOLD,
            "Bit 0 of tape reader or punch.\n", BULLET,
            "R31: ", BOLD,
            "Advance the tape reader (read) or punch (write) to the next character. Reads as 0.\n", BULLET,

            "\nNormal Code\n", HEADING,
            "In these days, there was no concept of separation of program modules. The program we today would call the assembler was mixed with the operating system; this had the convenient feature that you could load binary tapes, normal code tapes and simple code tapes without any setting in the machine: the first symbol on the tape would jump to the appropriate part of the operating system.\n", (),
            "Normal Code was a mixture of instructions to be assembled and stored in memory, with a number of special codes that allowed to control the process.\n"
            'For example, jump addresses could be computed during assembly. It was also possible to write "tape programs" that did all their work at compile time.\n'
            "Unfortunately, since I couldn't find the Normal Code manual on the internet, I had to reverse engineer it from the object code of the operating system. The encoding of these functions is determine by the fact that since there were five bits on a tape word, only 32 symbols were possible, so that only part of the alphabet was available.\n", (),
            "A,T,N: ", BOLD,
            "Start assembly of an instruction.\n", BULLET,
            "Y: ", BOLD,
            "Store the previous instruction in R13, the register that is executed after each instruction. Normally, this would contain the store instruction AD<addr>.\n", BULLET,
            "Z: ", BOLD,
            "Execute the previous instruction immediately. This was used by so called \"tape programs\", that didn't store anything in memory. Normally, this would contain the store instruction AD<addr>.\n", BULLET,
            "P: ", BOLD,
            "Relative addressing\n", BULLET,
            "TE: ", BOLD,
            "Subtract the previous two instructions. For clarity, it was allowed to put a minus sign in between the instructions, but it was ignored.\n",
            BULLET,
            "T: ", BOLD,
            "Labels and computations with instructions\n", BULLET,
            "+ and -: ", BOLD,
            "A number, which could either be an integer or a fraction, if a period was present. Fractions were fixed point numbers with 32 bits behind the decimal point (range -1...+1).\n", BULLET,

            "\nI/O devices\n", HEADING,
            "Tape reader: ", BOLD,
            "The tape reader could read 200 characters per second, but this required some clever programming since that implied you had to have two character decode routines in a rotation of the drum. In practice, getting the machine to read 100 characters per second was hard enough. The tape was advanced by reading register 31, after which the five bits were available by reading registers 26 through 30.\n", BULLET,
            "Tape punch: ", BOLD,
            'The punch was used for "high speed" output, since it allowed to punch 100 characters per seconds, which was a lot faster that the printer. This allowed to save valuable computer time by printing offline. The punch window in this program allows to interpret the punched code in different ways, since the punch was used to punch normal code, simple code and printer code as well as binary programs.\n', BULLET,
            "Printer: ", BOLD,
            "The printer used serial interface, with a bit speed of 50 baud, resulting in up to 7 characters per second. Since it used a variant of Baudot code, it could print all characters of the alphabet.\n", BULLET,

            "\nSimple Code\n", HEADING,
            'Simple code was an attempt to make the Zebra accessible for "non programmers". This code used floating point numbers only, and had an execution time of 15 milliseconds per instructions minimum, or more for "harder" instructions. Still it was still pretty hard to use; for example, making nested loops was pretty complicated. Interestingly, you didn\'t need to do anything to run Simple Code; the computer recognized the tapes by looking at the first letter (which was Y for Simple Code). Since there were only 32 symbols on a tape, not entire alphabet was available. Therefore, in Simple code, the tape symbols B and C of normal code were written as H and S. The tape reader in this program allows to use both interchangeably, which the punch can be set to interpret the tape symbols either way.\n', (),

            "\nOperating the computer\n", HEADING,
            "The control panel looked like:\n")
        text.image_create(tk.END, image=self.controlPic)
        text.insert(tk.END,
            '\nThis computer was definitely not for beginners!', (),
            '\nAs you can see, this program tries to emulate all these buttons, including the dialer. I think I emulated all the buttons like the original, but I am not sure about the "manual" button. My guess was that it would load the value from the switches in register C, which would make the most sense: you could then press "Step" to execute it. If you happen to know that that is incorrect, please drop me a mail!\n', CITATION,

            "\nReferences\n", HEADING,
            hyperlink.web("https://link.springer.com/chapter/10.1007%2F978-3-322-96260-7_7", '"Micro-programming and Trickology."'), hyperlink.add(),
            ' In: Digitale Informations-wandler, E.W. Hoffmann. Vieweg, Braunschweig (1961), p. 269-311.\n', BULLET,
            hyperlink.web("http://gordonbell.azurewebsites.net/computer_structures__readings_and_examples/00000220.htm", "Computer Structures: Readings & Examples: Description of Zebra by Willem van der Poel"), hyperlink.add(),
            " (4 pages)\n", BULLET,
            hyperlink.web("http://www.rug.nl/society-business/centre-for-information-technology/organisation/pictogram/2006-6/software.pdf", "Pictogram 2006:6: Pioneering Software in the 1960s by Jan Kraak"), hyperlink.add(),
            "\n", BULLET,
            hyperlink.web("http://www.rug.nl/society-business/centre-for-information-technology/organisation/pictogram/2006-6/software.pdf"), hyperlink.add(),
            ' (download link, in Dutch)\n', BULLET,
            hyperlink.web("http://www.stlqcc.org.uk/docs/computers_03.htm", "Computing Memories: The Stantec Zebra Computer, by Don Hunter"), hyperlink.add(),
            '\n', BULLET)

class AboutWillem(AboutBox):
    def fill(self):
        self.title("About Willem Louis van der Poel")
        self.willemPic = tk.PhotoImage(file=IMAGEDIR+'Willem.gif')

        text = self.text
        hyperlink = HyperlinkManager(text)
        BULLET, CITATION, BOLD, HEADING = 'BULLET', 'CITATION', 'BOLD', 'HEADING'
        #text.insert alternates text and type of the preceding text
        text.insert(tk.END,
            "This is Willem Louis van der Poel at age 90, as pictured by Lambert Meertens:\n",
            '')
        text.image_create(tk.END, image=self.willemPic)
        text.insert(tk.END,
            "\nI have to admit that Willem van der Poel is one of my heroes. Even though I learned to program from Edsger Dijkstra, the ideas of van der Poel did influence my thinking. Especially the idea to simplify the hardware at the cost of software really inspired me.\n"
            "The design of the Zebra is a good example of the genius that van der Poel was. Many of the things we now all take for granted were not invented yet those days."
            "\nBy the way, it was Willem van der Poel who later invented the stack in 1969!\n", (),
            "\nDijkstra and van der Poel\n", HEADING,
            "Edsger W. Dijkstra adn Willem L. van der Poel are arguably the two most important computer pioneers in the Netherlands.\n"
            'Their methods were quite different. Where Dijkstra is famous for his mathematical rigour and error-free programs, van der Poel was closer to the hardware design, and preferred clever programs that saved hardware cost. Where van der Poel really loved "trickology", Dijkstra famously said "Microprogramming an inefficient algorithm does not make it efficient", a remarks that is clearly addressed at van der Poel. Some of this can be found in the last reference "Number Crunchers".\n'
            "Nowadays, it is obvious that the complexity of the design of a computer prevents being too clever; Dijkstra's way of thinking is more or less the only way to make software and hardware that is reliable. But if you have seen the source code of this program, you will see that I can't resist a clever hack here and there.\n"
            'Let us all honour van der Poels way of thinking by using his word "trickology" instead of "hacking", since this latter word has been taken over by the media to mean something much too nefarious.', (),
            "\nReferences\n", HEADING,
            hyperlink.web('https://en.wikipedia.org/wiki/Willem_van_der_Poel', "Wikipedia entry on Willem van der Poel"), hyperlink.add(),
            '\n', BULLET,
            hyperlink.web('http://www-set.win.tue.nl/UnsungHeroes/heroes/vdpoel.html', "Unsung Heroes in Dutch Computing History: Willem van der Poel"), hyperlink.add(),
            '\n', BULLET,
            hyperlink.web("https://repository.tudelft.nl/islandora/search/title%3A%20%22number%20crunchers%22?collection=research", "Article by Frans Godfroy about Dijkstra and Van der Poel:"), hyperlink.add(),
            '\n', BULLET,
            )
