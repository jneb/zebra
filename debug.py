#!python 3
"""Simple decoration for debugging for function calls.
"""
from functools import wraps
from itertools import chain
def debug(f):
    "Decorator printing input and output of a function"
    @wraps(f)
    def wrapper(*args, **kwds):
        argdescr = ', '.join(chain(
            map(repr, args),
            map('%s=%r'.__mod__, kwds.items())))
        print("Calling %s(%s)"%(f.__name__, argdescr))
        try:
            result = f(*args, **kwds)
            print("%s(%s) returns %r"%(f.__name__, argdescr, result))
            return result
        except Exception as e:
            print("%s(%s) raises %r"%(f.__name__, argdescr, e))
            raise
    return wrapper
