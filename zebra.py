#!python3
"""This program emulates a Stantec Zebra computer.

To get an idea of the speed of the zebra:
If the efficiency is high enough during an extended period of time,
you could get over a hundred thousand instructions per minute...
(We are over a million times faster now, that's why this program works.)

Notes:
About the carry trap. Van der Poel says: "On every B- or Q- operation the
carry-over is stored in an intermediate storage of one digit, named carry-trap.
[...] The carry from the carry-trap is introduced on the carry entrance of the
pre-adder of A as if it were a carry from "a33". On a left shifting instruction
with V it is introduced one digit time late as if it were a carry from a32.
This implies that an instruction of the form A200L5V can give wrong results,
because the addition of (200) and (5) in the pre-adder can give rise already to
a carry from a33 to a32 so that no other carry can be added at the same time.
[...] A subtraction in B is performed by adding the inverse of the number
together with introducing an extra complimentary 1 on the carry entrance of the
main adder of B as if it were a carry from "b33". When a number is added, the
resulting carry is just the opposite of what it would be, when the same number
is subtracted. For example, subtracting 0 gives a carry 1.
"""

#TODO 5: deeper emulation of dialer

import time
import sys
import struct
import os
import collections
import queue
import re
import traceback
from operator import methodcaller

#path where the zebra programs are searched
ZEBRAEXTENSIONS = ('SRC', 'ZNC', 'ZSC', 'ZBI', 'ZTP', 'ZRB')
MASK33 = ~(-1<<33)
MASK34 = ~(-1<<34)
SIGNBIT = 1<<33
#default number of instruction times used for efficiency meter
#we measure over 5 rotations of the drum (=50 ms)
EFFICIENCYWINDOW = 32*5

#the preferred ASCII representation of the tape symbols (optimized for NC)
CURDIR = os.path.dirname(sys.executable if hasattr(sys, 'frozen') else __file__)
ZEBRAPATH = os.path.join(os.path.dirname(__file__), 'tapes')
TAPESYMBOLS = '0123456789KQ.LRIBCDETUVNAX+-YZP#'
FILEDIR = os.path.join(CURDIR, 'tapes')
SIP = os.path.join(FILEDIR, 'SIP.SRC')
ZLOT6 = os.path.join(FILEDIR, 'ZLOT6.FRD')

#The "Dutch alphabet"
LETTERS = "AKQLRIBCDEV421W"

#display mode bits to make the emulator show what's happening; see displayMode
#for showing thing, the callback function is called with appropriate arguments
#show all instructions (doesn't use callback)
INSTRUCTION = 1<<0
#show all memory writes
WRITEMEM = 1<<1
#show tape input (source lines)
TAPELINES = 1<<2
#show tape input (character by character, especially nice in real time mode)
TAPECHARS = 1<<3
#show what data is read for A instructions. Not much use without INSTRUCTION
ADATAREAD = 1<<4
#changes of registers
WRITEREG = 1<<5

#masks
#register and memory bits
REGMASK = 31<<13
ADDRMASK = 0x1fff
#condition bits from functional bits
fUVMASK = 0x1E
#just the condition number, without the V bit
fUMASK = 0xE

class assemble:
    """Assemble Zebra instruction, with optional address"""
    #pattern of zebra instruction:
    #initial letter, two groups of digits, and condition
    #and function letters anywhere
    functionLetters = "([KQLRIBCDEW.]*)"
    initialLetters = "([AXN])"
    digits = "(\d*)"
    condition = "([UV]?\d?)"
    #allow " if <explanation>" at end (doesn't contain a group)
    ending = "(?: if .*)?$"
    #total number of groups: 8
    assemblyPat = re.compile(
        functionLetters.join(
            (initialLetters, digits, digits, condition, ending)
        )   )

    letterValues = {c:1<<i for c,i in zip(
        LETTERS,
        range(32, 17, -1) ) }
    for letter in 'XNU.': letterValues[letter] = 0

    #abuse new method to have local constants
    def __new__(cls, s, addr=0):
        """Assemble a Zebra instruction, where addr is the location.
        """
        #determine the important information from the string
        m = cls.assemblyPat.match(s)
        if m is None: raise SyntaxError("Invalid Zebra instruction: " + s)
        (   startLetter, letters1,
            firstNum, letters2,
            secondNum, letters3,
            UV, letters4) = m.groups()
        letters = letters1+letters2+letters3+letters4
        firstValue = int('0'+firstNum, 10)
        secondValue = int('0'+secondNum, 10)
        UVvalue = int('0'+UV[1:], 10)

        #build the result: startletter
        result = cls.letterValues[startLetter]

        #decide mode: W if not N and first number is not there or register
        if startLetter=='N':
            assert not secondNum
            result += firstValue<<13 | addr+1
        elif firstNum and (firstNum[0]=='0' or firstValue>=32):
            #not W
            result += secondValue<<13 | firstValue
        else:
            #W, or just a register
            letters += 'W'
            result += firstValue<<13 | (-secondValue<<1)&ADDRMASK

        #other letters
        result += sum(map(cls.letterValues.get, letters))
        if UV: result += cls.letterValues[UV[0]] + (UVvalue<<19)
        return result

X8190IB30 = assemble("X8190IB30")
AD8191LK29 = assemble("AD8191LK29")
ARCK5 = assemble("ARCK5")
X003LIB26 = assemble("X003LIB26")
X000 = assemble("X000")
X10K = assemble("X10K")
X11KBCU7 = assemble("X11KBCU7")
#only the functional bit part (that is, instruction>>18)
fA000W = assemble("A")>>18
fXadrBCV7 = assemble("X000BCV7")>>18
fX11KBCU7 = X11KBCU7>>18

def split(n):
    """Split a 33 bit number into function, register, address"""
    return n>>18, n>>13&0x1f, n&ADDRMASK

def func2letters(f):
    result = "" if f>>14&1 else "X"
    return result+''.join(
        letter if f>>14-i&1 else ""
        for i, letter in enumerate(LETTERS))

def display(adr, func, reg, mem, offsetReg=0, explainCondition=True):
    """Display address, func, reg, drum, condition and optional + R:
    adr may be a string, then it is shown explicitly
    1234 000000000 X345V2 (if B<0) +1
    tip: use display(adr, *split(value)) if you don't have func,reg,mem
    end address with '=' if you don't want adr: ... format
    """
    if isinstance(adr, int): adrS = " {:04d}:".format(adr)
    elif adr[-1:] in '=(': adrS = adr
    else: adrS = "{:>5s}:".format(adr)
    hexS  = "{:09x}".format(func<<18|reg<<13|mem)
    #A or X part
    instrS = "A" if func>>14&1 else "X"
    letters = func2letters(func&0x3fe1)[1:]
    # order determined by W: with W set, do register first
    # W instructions ignore lower mem bit since it is -2*repeat count)
    if func>>0&1:
        #implicit W: write reversed, clip off W
        letters = letters[:-1]
        if mem: instrS += str(reg)+(letters or '.')+str(-(mem>>1)&0xfff)
        else: instrS += str(reg or "")+letters
    elif mem-1==adr:
        #shorten N,NKK
        instrS = "NKK" if func>>14&1 else "N"
        instrS += letters+str(reg or "")
    elif reg:
        #standard with registers:add . if needed
        instrS += "{:03d}{}{!s}".format(mem, (letters or "."), reg)
    else:
        #no registers
        instrS += "{:03d}{}".format(mem, letters)
    #show condition
    if func&fUVMASK:
        instrS += 'V' if func&0x10 else 'U'
        if func&fUMASK: instrS += str(func>>1&0x7)
    result = [adrS, hexS, instrS]
    #add register indication
    if offsetReg:
        regS = "01AB"[offsetReg] if offsetReg<4 else 'R'+str(offsetReg)
        result.append("+"+regS)
    #add condition explanation
    if func&fUVMASK and explainCondition:
        cond = func>>1&0xf
        conditions = {1:"A<0", 2:"B<0", 3:"A!=0", 4:"B&1"}
        if 0<cond<7: result.append("if U"+str(cond))
        elif cond==7: result.append("if not start")
        elif cond-8 in conditions: result.append("if "+conditions[cond-8])
    return ' '.join(result)

# table of useless instruction of the form mask, value, explanation
uselessTable = (
    (assemble("X000V5"), assemble("X000V5"), "invalid condition"), # (V5 and V7)
    (assemble("X000V6"), assemble("X000V6"), "invalid condition"), # (V6 and V7)
    (assemble("AD"), assemble("AD"), "Write to nowhere"),          # (A.D.W)
    (assemble("AK"), assemble("X"), "Jump to zero"),               # (ak..W)
    (assemble("AKD000"), assemble("XD000"), "Jump to zero"),       # (akD..)
    (assemble("AEK"), assemble("XEK"), "Jump to zero"),            # (aK.EW)
    (assemble("XE12"), assemble("XE3"), "Write to readonly reg"),   # (...E.[0-3])
    )

def useless(instr):
    """Check for useless instructions
    Returns explanation or None
    """
    for mask, value, explanation in uselessTable:
        if instr & mask == value: return explanation

class UnknownInstruction(Exception): pass

def readFRD(path):
    """yield adr,f,r,d for an FRD file"""
    frdfile = open(path, 'rb')
    def readline():
        data = frdfile.read(5)
        if not data: raise StopIteration
        return struct.unpack('<HBH', data)
    while True:
        try:
            func, reg, start = readline()
            assert func==reg==0
            func,reg, stop = readline()
            assert func==reg==0
        except StopIteration: break
        for adr in range(start, stop+1):
            yield (adr,)+readline()

class Registers:
    """The state of the registers, including A,B,C,D and special registers.
    """
    #the number of instruction times the tape reader takes to advance
    #we use the same delay for punching, because it makes no difference
    ADVANCEDELAY = 10
    def readConstant(self, number):
        """Read register returning a constant"""
        return {0:0, 1:1, 23:1<<32}[number]

    def writeIgnore(self, number, value):
        """Write constant register"""
        pass

    def __getitem__(self, number):
        """Read real register, no side effects"""
        return self.values[number]

    def writeReal(self, number, value):
        """Write real register, normalize"""
        self.values[number] = value&MASK33

    def readSpecial(self, number):
        if number==2: return self.A
        if number==3: return self.B
        if number==24: return self.A&self.B
        raise NotImplementedError("Read register: "+str(number))

    def writeAnd(self, number, value):
        """This is detected by Zebra.step,
        since the write action would be too late.
        """

    def writePrinter(self, number, value):
        self.parent.printer.bit(value>>32)

    def readMissing(self, number):
        return 0

    def readTape(self, number):
        if self.tapeBits is None:
            #at the latest possible moment, read a char from tape file
            try: self.tapeBits = next(self.parent.inputTape)
            except StopIteration:
                #no tape: read as holes
                #doesn't set tapeBits, so will check for tape every time
                #TODO 3: do we want to stop zebra when rerunning without setting tape?
                return -1
        return -(self.tapeBits>>30-number&1)

    def advanceTape(self, number):
        """produce next tape character.
        Doesn't actually read the next character from the tape file until
        really needed: see readTape.
        """
        #ignore quick succession of advance commands
        #but if lastAdvanceTime is higher than current time,
        #the clock has been reset, so ignore that
        now = self.parent.drum.time
        lastAdv = self.lastAdvanceTime
        if lastAdv>now: lastAdv = now-self.ADVANCEDELAY
        if now >= lastAdv+self.ADVANCEDELAY:
            #don't actually read a character, wait until it is requested
            self.tapeBits = None
            self.lastAdvanceTime = now
        #remember, it is still a register read, so something needs to be returned
        return 0

    def initTape(self):
        self.tapeBits = None
        #act as if the last advance was a long time ago
        self.lastAdvanceTime = -self.ADVANCEDELAY
        #same for punch
        self.punchBits = 0

    def tapePunch(self, number, value):
        """The sign bit in value determines what is punched;
        bits are kept in self.punchBits
        """
        mask = 1<<30-number
        self.punchBits &= ~mask
        if value>>32&1: self.punchBits |= mask

    def advancePunch(self, number, value):
        """Advance the punch and punch the character given by self.punchBits
        Calls a callback function if a character is punched.
        """
        #ignore quick succession of advance commands
        #but if lastAdvanceTime is higher than current time,
        #the clock has been reset, so ignore that
        now = self.parent.drum.time
        lastAdv = self.lastAdvanceTime
        if lastAdv>now: lastAdv = now-self.ADVANCEDELAY
        if now >= lastAdv+self.ADVANCEDELAY:
            #delay has passed; punch. Note that we give the raw data.
            self.parent.punchCallback(self.punchBits)
            self.lastAdvanceTime = now

    readTable = [
        readConstant, readConstant, readSpecial, readSpecial,   #0-3
        __getitem__, __getitem__, __getitem__, __getitem__,     #4-7
        __getitem__, __getitem__, __getitem__, __getitem__,     #8-11
        __getitem__, __getitem__, __getitem__, __getitem__,     #12-15
        readMissing, readMissing, readMissing, readMissing,     #16-19
        readMissing, readMissing, readMissing, readConstant,    #20-23
        readSpecial, readSpecial, readTape, readTape,           #24-27
        readTape, readTape, readTape, advanceTape,              #28-31
        ]

    writeTable = [
        writeIgnore, writeIgnore, writeIgnore, writeIgnore,     #0-3
        writeReal, writeReal, writeReal, writeReal,             #4-7
        writeReal, writeReal, writeReal, writeReal,             #8-11
        writeReal, writeReal, writeReal, writeReal,             #12-15
        writeIgnore, writeIgnore, writeIgnore, writeIgnore,     #16-19
        writeIgnore, writeIgnore, writeIgnore, writeIgnore,     #20-23
        writeAnd, writePrinter, tapePunch, tapePunch,           #24-27
        tapePunch, tapePunch, tapePunch, advancePunch,          #28-31
        ]

    def read(self, number):
        "Read a register, with side effects"
        #find relevant routine, call it, normalize answer
        return self.readTable[number](self, number) &MASK33

    def write(self, number, value):
        "Write a register"
        return self.writeTable[number](self, number, value)

    def __init__(self, parent):
        """initialize real registers
        """
        self.values = {r:0 for r in range(4, 16)}
        #for handling special effects
        self.parent = parent
        self.initTape()
        self.clear()

    def clear(self):
        """Clear registers A,B,C,D."""
        self.A = self.B = self.C = self.D = 0
        self.origin = 'cleared'

class FunctionBits:
    """Represent the function bits from a 15 bit integer.
    Apart from AKQLRIBCDEVW, it also sets W, X, Vn, LR, XD
    """
    def __init__(self, func):
        """Set up the bits by name from the function.
        """
        assert 0 <= func < 1<<15, "function must be 15 bits number"
        self.all = func
        #This is the fastest method I could find
        (self.A, self.K, self.Q, self.L, self.R,
            self.I, self.B, self.C, self.D, self.E,
            self.V) = (func>>i&1 for i in range(14,3,-1))
        self.W = func>>0&1
        #handy shortcuts
        self.X = not self.A
        #the number corresponding to V421 (e.g. 8=V)
        self.Vn = func>>1&0xf
        self.LR = self.L and self.R
        self.XD = self.X and self.D

    def __repr__(self): return func2letters(self.all)

class Tape:
    """An input tape.
    """
    def __init__(self, parent=None):
        """Parent is needed for callback to print routine.
        Omit if all you need is the character map"""
        self.makeCharacterMap()
        self.line = self.column = 0
        self.reader = None
        self.parent = parent

    def makeCharacterMap(self):
        self.charValue = charValue = {}
        for i,c in enumerate(TAPESYMBOLS): charValue[c] = i
        #some extra duplicates
        charValue['/'] = charValue['\\'] = charValue['%'] = charValue['#']
        charValue['H'] = charValue['B']
        charValue['S'] = charValue['C']

    def __next__(self):
        if self.reader is None: raise StopIteration
        else: return next(self.reader)

    def reload(self):
        self.line = self.column = 0
        self.reader = self.tapeReadGen()

    @staticmethod
    def expandPath(name):
        """Expands a short name to a full path name.
        name may be a path name, file name with or without extension.
        Find the first file in local directory, or recursively in ZEBRAPATH
        raises ValueError if no files are found.
        bug: if there are files with the same name except for case,
        it picks a random one.
        """
        if os.path.exists(name): return name
        #construct all possible lower case names
        allowedFiles = {name}
        for extension in ZEBRAEXTENSIONS:
            allowedFiles.add((name+'.'+extension).lower())
        callLower = methodcaller('lower')
        #local dir
        fn = map(callLower, os.listdir(os.curdir))
        match = allowedFiles.intersection(map(callLower, fn))
        if match: return match.pop()
        #ZEBRAPATH, recursive
        for dp, dn, fn in os.walk(ZEBRAPATH):
            #ignore directories with periods
            dn = [d for d in dn if not d.startswith('.')]
            match = allowedFiles.intersection(map(callLower, fn))
            if match: return os.path.join(dp, match.pop())
        raise ValueError('Not found: '+name)

    def load(self, name):
        """Load file. Allows "short" names."""
        self.fileName = self.expandPath(name)
        self.reload()

    def tapeReadGen(self):
        """Generator for the characters.
        Calls the callback function for lines or chars that are read.
        """
        comment = False
        for self.line, line in enumerate(open(self.fileName, 'r'), start=1):
            #announce new tape line before processing it
            if self.parent.displayMode&TAPELINES:
                self.parent.tapelineCallback(line.rstrip())
            for self.column, c in enumerate(line, start=0):
                #read character
                if c=='}':
                    #end comment at }
                    comment = False
                elif comment:
                    #ignore comment
                    pass
                elif c in self.charValue:
                    #actual useful contents, translate to zebra code
                    if self.parent.displayMode&TAPECHARS:
                        self.parent.tapecharCallback((self.line, self.column, c))
                    yield self.charValue[c]
                elif c.isspace():
                    #ignore whitespace
                    pass
                elif c=='{':
                    #start comment at {
                    comment = True
                else:
                    #invalid symbol treated as end of tape
                    self.parent.tapeendCallback(c)
                    self.reader = None
        #end of tape
        self.parent.tapeendCallback(c)
        self.reader = None

class Printer:
    """The teletype printer.
    It uses a UART to convert the baudot output to ASCII.
    It doesn't do the timing right; it just uses the instructions.
    So don't use it to test your own printing routine :-)
    The actual speed is a staggering 50 baud (7 chars/second).
    """
    #Baudot table
    #n:null, x:undef l:LF t:tab a:? c:CR? s:LS f:FS b:bell
    #figures, then letters
    #the # and & were not on the Zebra, replaced by the Baudot symbols
    #Zebra uses ! for '
    charTable = [
        "nElA SIUcDRJNFCKTZLWHYPQOBGfMXVs",
        "n3l- !87ct4b,x:(5+)2#6019?&f./=s",
        ]
    specialChars = {
        'b': '\x07',    #bell
        'c': '\r',      #carriage return
        'l': '\n',      #line feed
        'n': '',        #null
        't': '\t',      #tab?  (ENQ in Baudot)
        'x': '(!)',     #! in Baudot, unused in Zebra
        }

    def __init__(self, parent):
        #True: digits, False: letters
        #The RANDOM demo starts with figures shift, so we will do that too.
        self.parent = parent
        self.shift = True
        #0-4: bit number of next bit to be received
        #5: waiting for stop bit
        #6: waiting for start bit
        self.UARTbit = 6
        self.char = 0

    def bit(self, bit):
        """Receive a bit, print if a character was found.
        """
        assert 0<=bit<2, "Printer.bit"
        UARTbit = self.UARTbit
        if UARTbit<5:
            #receive bit
            self.char |= bit<<UARTbit
        elif UARTbit==5:
            #char is ready: print immediately
            self.parent.printerCallback(self.decode(self.char))
            #wait for stop bit, ignore 0
            if not bit: return
        else:
            #start new char
            self.char = 0
            #wait for start bit, ignore 1
            if bit: return
        #increase bit #
        self.UARTbit = (UARTbit+1)%7

    def decode(self, char):
        """Output character in ASCII"""
        c = self.charTable[self.shift][char]
        #lower case are special characters
        if c<'`': return c
        if c=='f': self.shift = True
        elif c=='s': self.shift = False
        elif c in self.specialChars: return self.specialChars[c]
        return ''

class Drum:
    """The drum memory, including time.
    startTime is the clock time (as returned by time.monotonic) in real mode,
    or None in full-speed mode
    time is the elapsed simulated time in instructions
    """
    #speed of drum in rotations per second
    DRUMSPEED = 6000
    LOCKBLOCKSIZE = 512
    DRUMSIZE = 8192

    def __init__(self, realTime=False):
        self.data = [0]*self.DRUMSIZE
        self.time = -1
        self.resetClock(realTime)
        #locks per LOCKBLOCKSIZE words. locks[-1] is for track 0
        self.locks = [False]*(self.DRUMSIZE//self.LOCKBLOCKSIZE+1)

    @property
    def IPS(self):
        """Instructions per second"""
        #DRUMSPEED is rotations per minute; there are 32 instructions on a drum
        return 32/60*self.DRUMSPEED

    def rotate(self, address):
        """Adjust time to show rotation delay for address.
        Give address=None if you don't read the drum, but still do an instruction.
        """
        if address is not None:
            #determine number of steps to rotate the drum
            delay = (address-self.time-1 & 31)+1
        else:
            #for W instructions
            delay = 1
        #increase time
        self.time += delay
        #if needed, wait to simulate Zebra time
        if self.startTime is not None:
            simulatedElapsedTime = self.time/self.IPS
            realElapsedTime = time.monotonic()-self.startTime
            sleepTime = simulatedElapsedTime-realElapsedTime
            if sleepTime>0: time.sleep(sleepTime)

    def __getitem__(self, address):
        """Read item from drum, increase time"""
        self.rotate(address)
        return self.data[address]

    def __setitem__(self, address, data):
        """Write item to drum, increase time"""
        self.rotate(address)
        #don't write to locked blocks
        if address<32 and self.locks[-1]: return
        if self.locks[address//self.LOCKBLOCKSIZE]: return
        self.data[address] = data &MASK33

    def resetClock(self, realTime=False):
        """Reset the clock, for switching on or off real mode.
        The next read on address 0 is "immediate".
        """
        #compute virtual start time so that
        #the next call to rotate() would not cause delay
        if realTime: self.startTime = time.monotonic()-self.time/self.IPS
        else: self.startTime = None

class Switches:
    """The switches on the board.
    U7 is a pushbutton that is ON, and becomes OFF when pressed.
    The others are flip switches.
    """
    def __init__(self, parent):
        self.value = 1<<7
        self.parent = parent

    def __repr__(self):
        return ', '.join(
            'U{}:O{}'.format(
                i,
                ('n' if self.readSwitch(i) else 'ff'))
            for i in range(1, 8))

    def readSwitch(self, number):
        """Value of given switch as boolean"""
        if not 0<number<8: raise ValueError(
            "Invalid switch number: {}".format(number))
        return bool(self.value>>number&1)

    def setSwitch(self, number):
        if not 0<number<8: raise ValueError(
            "Invalid switch number: {}".format(number))
        self.value |= 1<<number

    def resetSwitch(self, number):
        if not 0<number<8: raise ValueError(
            "Invalid switch number: {}".format(number))
        self.value &= ~(1<<number)

    def pressU7(self):
        """Press U7 during three instructions"""
        self.resetSwitch(7)
        for i in range(3): self.parent.step()
        self.setSwitch(7)

class Zebra:
    """The state of the computer.
    drum: the memory drum as a list of ints
    registers: the contents of the registers, including special ones
    carryTrap: the contents of the carry trap (bool)
    origin: user friendly explanation of the current value of C
    the callback routines are called when something of interest happens
    """
    def __init__(self, realTime=False):
        #initialize hardware
        self.drum = Drum(realTime)
        self.registers = Registers(self)
        self.carryTrap = 0
        self.switches = Switches(self)
        self.inputTape = Tape(self)
        self.printer = Printer(self)
        #user interface stuff
        self.displayMode = 0
        self.origin = ""
        self.efficiencyWindow = EFFICIENCYWINDOW
        #and we can begin
        self.clear()

    def step0(self):
        """Execute an instruction, and register for the efficiency computation.
        Sorry, this routine is way too long.
        This is the original version, written for legibility and correctness.
        The version below is actually used, faster but slightly less readable.
        """
        #set the execution bits (E register is implicit, just like the real one)
        R = self.registers
        func, reg, mem = split(R.C)
        if self.displayMode&INSTRUCTION:
            print("{:.3f}s. Executing: {}".format(
                self.time(), display(self.origin, func, reg, mem)))
        bit = FunctionBits(func)

        #check condition and skip if needed
        #funcVn=1..7 for U1..U7, 9..15 for V1..V7
        funcVn = bit.Vn
        if funcVn==0: doInstr = True
        elif funcVn<8: doInstr = self.switches.readSwitch(funcVn)
        elif funcVn==8: doInstr = True
        elif funcVn==9: doInstr = R.A>>32&1
        elif funcVn==10: doInstr = R.B>>32&1
        elif funcVn==11: doInstr = R.A&MASK33!=0
        elif funcVn==12: doInstr = R.B&1
        elif funcVn==15: raise ValueError("Dialer")
        else: doInstr = True
        #if condition fails, replace instruction by A000W
        if not doInstr:
            bit, reg, mem = FunctionBits(fA000W), 0, 0
            funcVn = 0

        #make accumulator shorthands
        Am = R.A &MASK33
        B = R.B

        #read applicable register, including side effects
        if bit.E: registerValue = 0
        else: registerValue = R.read(reg)
        #read memory, including AND mask effect of write to reg 24
        if bit.W:
            drumValue = 0
            #allow the drum to rotate for one step for this instruction
            self.drum.rotate(None)
            #otherwise, the delay is automatic by the reading or writing action
        elif bit.D: drumValue = 0
        else:
            drumValue = self.drum[mem]
            if bit.A and self.displayMode&ADATAREAD:
                print("{:.3f}s. Reading:   {}".format(
                    self.time(),
                    display(mem, *split(drumValue))))
        #special action for E24
        if bit.E and reg==24: drumValue &= R[5]

        #pre-adder of A: follow the wire
        if bit.L and bit.R: preAddA0 = R[15]&-(B&1)
        elif bit.B: preAddA0 = 0
        elif bit.XD: preAddA0 = R[15]
        elif bit.K: preAddA0 = 0
        else: preAddA0 = registerValue
        if not bit.B and bit.A: preAddA1 = drumValue
        else: preAddA1 = 0
        if funcVn==8:
            preAddAC = self.carryTrap^bit.I
            #delay carry on left shift, as specified
            #but since this is implemented as an OR on the carry bit,
            #we have to emulate the "wrong result" too
            if bit.L and not bit.R:
                if (preAddA0&preAddA1&1)==0: preAddAC <<= 1
                else: preAddAC = 0
        else: preAddAC = 0
        addA1 = preAddA0+preAddA1+preAddAC
        #pre-adder of B: follow the wire
        if not bit.B: preAddB0 = 0
        elif bit.XD: preAddB0 = R[15]
        elif bit.K: preAddB0 = 0
        else: preAddB0 = registerValue
        if bit.B and bit.A: preAddB1 = drumValue
        else: preAddB1 = 0
        preAddBC = bit.Q
        addB1 = preAddB0+preAddB1+preAddBC

        #according to van der Poel, seems to be correct as stated
        #flag to set the carry trap
        setCarryTrap = bit.Q or bit.B

        #adder of A, including inversion and sign extend
        if bit.C and not bit.B: addA0 = 0
        #keep sign bit (bit 33) when shifting right
        elif bit.R: addA0 = R.A>>1|R.A&SIGNBIT
        elif bit.L: addA0 = Am<<1|(0 if bit.C else B>>32)  #no shift carry if C
        else: addA0 = R.A
        #sign extend
        addA1 &= MASK33
        if addA1>>32: addA1 -= SIGNBIT
        if bit.I: addA1 = -addA1
        #adder of B
        if bit.C and bit.B:
            addB0 = 0
        elif bit.R:
            #R dominates L
            addB0 = B>>1|(0 if bit.C else (Am&1)<<32)  #no shift carry if C
        elif bit.L:
            addB0 = B<<1 &MASK33
        else:
            addB0 = B
        if bit.I:
            #addition of -0 causes a carry: make sure this is handled correctly
            addB1 = ~addB1 &MASK33
        #adder of C
        if bit.A: addC0 = R.D
        else: addC0 = drumValue
        if not bit.K or bit.E: addC1 = 0
        else: addC1 = registerValue

        #preserve R4 simulating simultaneous writing of accumulators and regs
        R4 = R[4]
        #store results
        writeValue = B if bit.B else Am
        if bit.E:
            regValue = R.D if bit.K else writeValue
            if 4<=reg<16: oldValue = R[reg]
            else: oldValue = None
            R.write(reg, regValue)
            if self.displayMode&WRITEREG:
                self.registerCallback((reg, oldValue))
        if bit.D and not bit.W:
            # read the old data without counting rotation
            oldValue = self.drum.data[mem]
            self.drum[mem] = writeValue
            if self.displayMode&WRITEMEM:
                self.memoryCallback((mem, oldValue))

        #update registers; do D before C since D needs the old value of C
        if bit.X: R.D = R.C+2 &MASK33
        else: R.D = R4
        R.A = addA0+addA1 &MASK34
        Bsum = addB0+addB1+bit.I
        #only set carry trap if there actually is an addition
        if setCarryTrap:
            self.carryTrap = Bsum>>33&1
        R.B = Bsum &MASK33
        R.C = addC0+addC1 &MASK33

        #explain previous instruction to user in self.origin
        #this flag indicates the origin is just a memory location
        simpleFlag = False
        if bit.A: origin = "(D)"
        elif not (bit.W or bit.D):
            if bit.E and reg==24: origin = "({})&R5".format(mem)
            else:
                origin = "({})".format(mem)
                simpleFlag = True
        else: origin = "0"
        if bit.K and not bit.E:
            regS = "01AB"[reg] if reg<4 else 'R'+str(reg)
            origin += "+"+regS
            simpleFlag = False
        #simple case: origin is numeric if nothing special is going on, else str
        if simpleFlag: self.origin = mem
        else: self.origin = origin

        self.countStep()

    def checkCondition(self, A33, B, C, ADDRMASK=ADDRMASK):
        """Compute func, reg, mem depending on the condition.
        """
        #check condition
        #funcVn=1..7 for U1..U7, 9..15 for V1..V7
        funcVn = C>>19&0xf
        if funcVn==0: doInstr = True
        elif funcVn<8: doInstr = self.switches.readSwitch(funcVn)
        elif funcVn==8: doInstr = True
        elif funcVn==9: doInstr = A33>>32&1
        elif funcVn==10: doInstr = B>>32&1
        elif funcVn==11: doInstr = A33!=0
        elif funcVn==12: doInstr = B&1
        elif funcVn==15: raise ValueError("Dialer")
        else: raise UnknownInstruction
        #compute func, reg, mem
        #if condition fails, replace instruction by A000W
        if doInstr:
            return C>>18, C>>13&31, C&ADDRMASK
        else:
            return fA000W, 0, 0

    def step(self,
            MASK33=MASK33, MASK34=MASK34, SIGNBIT=SIGNBIT,
            INSTRUCTION=INSTRUCTION, WRITEREG=WRITEREG, WRITEMEM=WRITEMEM,
            ):
        """Optimized version of step routine above.
        Execute an instruction, and register for the efficiency computation.
        Is actually about twice as fast as the previous routine.
        """
        #set the execution bits (E register is implicit, just like the real one)
        R = self.registers
        A33,B,C = R.A&MASK33, R.B, R.C
        func, reg, mem = self.checkCondition(A33, B, C)
        #announce instruction
        if self.displayMode&INSTRUCTION:
            print("{:.3f}s. Executing: {}".format(
                self.time(), display(self.origin, func, reg, mem)))

        #determine instruction bits
        bitA, bitK, bitQ = func&(1<<14), func&(1<<13), func&(1<<12)
        bitL, bitR = func&(1<<11), func&(1<<10)
        bitI = func>>9&1     #this is either 0 or 1
        bitB = func&(1<<8)
        bitnC = ~func&(1<<7)
        bitD, bitE = func&(1<<6), func&(1<<5)
        bitW = func&1

        #read applicable register, including side effects
        if bitE: registerValue = 0
        else: registerValue = R.read(reg)

        #read memory, including AND mask effect of write to reg 24
        drumValue = 0
        if bitW:
            #allow the drum to rotate for one step for this instruction
            self.drum.rotate(None)
            #otherwise, the delay is automatic by the reading or writing action
        elif not bitD:
            drumValue = self.drum[mem]
            if bitA and self.displayMode&ADATAREAD:
                print("{:.3f}s. Reading:   {}".format(
                    self.time(),
                    display(mem, *split(drumValue))))
        #special action for E24 that influenced the memory value read
        if bitE and reg==24: drumValue &= R[5]

        #left input of both preadders: XD, K bits
        if not bitA and bitD: leftInput = R[15]
        elif bitK: leftInput = 0
        else: leftInput = registerValue
        #bring to selected preadder
        if bitB: addA, addB = 0, leftInput
        else: addA, addB = leftInput, 0
        #multiplication feature, only works for A
        if bitL and bitR: addA = R[15]&-(B&1)

        #add carry to A: this only matters if carry is requested and set
        #we do this here to simplify the bug simulation
        funcVn = C>>19&0xf
        if funcVn==8 and self.carryTrap^bitI:
            addA += 1
            if bitL and not bitR:
                #delay carry on left shift, as specified: add carry again
                #but first, check if we have to imitate the zebra bug
                if bitA and not bitB and addA&drumValue&1:
                    #since this is implemented as an OR on the carry bit,
                    #we will get a wrong result; emulate this faithfully
                    addA -=1
                else:
                    #nothing to worry about, compute normally
                    addA +=1

        #increment B if needed
        if bitQ: addB +=1

        #add memory output to right input of preadders
        if bitA:
            if bitB: addB += drumValue
            else: addA += drumValue

        #sign extend A
        addA &= MASK33
        if addA>>32: addA -= SIGNBIT

        #invert output of preadders if needed
        if bitI:
            #A is done the simple way
            addA = -addA
            #addition of -0 causes a carry: make sure this is handled correctly
            addB = ~addB &MASK33
            addB +=1

        #do the shift, clear and add operation of A and B (R,L,C,B bits)
        if bitR:
            if bitnC:
                addA += R.A>>1|R.A&SIGNBIT
                addB += B>>1|(A33&1)<<32
            elif bitB: addA += R.A>>1|R.A&SIGNBIT
            else: addB += B>>1                #no A->B transport!
        elif bitL:
            if bitnC:
                addA += A33<<1|B>>32
                addB += B<<1 &MASK33
            elif bitB: addA += A33<<1         #no B->A transport!
            else: addB += B<<1 &MASK33
        elif bitnC:
            addA += R.A
            addB += B
        elif bitB: addA += R.A
        else: addB += B

        #adder of C: add D or memory data to optional register
        if bitA: addC = R.D
        else: addC = drumValue
        if bitK: addC += registerValue

        #preserve R4 to simulate simultaneous writing of accumulators and regs
        R4 = R[4]
        #store results to register and/or memory
        writeValue = B if bitB else A33
        if bitE:
            regValue = R.D if bitK else writeValue
            if 4<=reg<16: oldValue = R[reg]
            else: oldValue = None
            R.write(reg, regValue)
            if self.displayMode&WRITEREG:
                self.registerCallback((reg, oldValue))
        if bitD and not bitW:
            # read the old data without counting rotation
            oldValue = self.drum[mem]
            self.drum[mem] = writeValue
            if self.displayMode&WRITEMEM:
                self.memoryCallback((mem, oldValue))

        #update registers; do D before C since D needs the old value of C
        if bitA: R.D = R4
        else: R.D = R.C+2 &MASK33
        R.A = addA &MASK34
        #set carry trap if needed
        if bitQ or bitB: self.carryTrap = addB>>33&1
        R.B = addB &MASK33
        R.C = addC &MASK33

        #explain previous instruction to user in self.origin
        #depending on bitA, bitW, bitD, bitE, reg, mem, bitK
        #this flag indicates the origin is just a memory location
        simpleFlag = False
        if bitA: origin = "(D)"
        elif bitW or bitD: origin = "0"
        else:
            if bitE and reg==24: origin = "({})&R5".format(mem)
            else:
                origin = "({})".format(mem)
                simpleFlag = True
        if bitK and not bitE:
            regS = "01AB"[reg] if reg<4 else 'R'+str(reg)
            origin += "+"+regS
            simpleFlag = False
        #simple case: origin is numeric if nothing special is going on, else str
        if simpleFlag: self.origin = mem
        else: self.origin = origin

        #administration for efficiency
        self.countStep()

    def clear(self, doSteps=True):
        """Press the clear key, reset timer, and do three steps.
        The three steps are necessary because normally the instruction at 0
        is X000KE4U7, so that:
        Step one: go to 0 from an unknown location, with D=0.
        Step two: save D=0 in R4, and set D correctly to X002KE4U7.
        Step three: save the valid value of D in R4.
        """
        self.registers.clear()
        self.carryTrap = 0
        #reset efficiciency window size to current value
        self.efficiencyTimestamps = collections.deque(
            maxlen=self.efficiencyWindow)
        self.drum.resetClock(realTime=self.drum.startTime is not None)
        self.stepCounter = 0
        if doSteps: self.step(); self.step(); self.step()

    def boot(self):
        """Load the boot program, and install boot tape"""
        self.clear()
        self.drum.locks = [False]*17
        #boot 1: the PRESIP
        print("Bootstrap routine: [0]=X8190IB30, [8190]=AD8191LK29")
        self.drum[0] = X8190IB30
        self.drum[-2] = AD8191LK29

        #boot 2: read four instruction routine
        self.inputTape.load(SIP)
        while self.registers.C!=ARCK5: self.step()
        if self.displayMode&TAPECHARS: print()
        print("{:.3f}s: four instruction routine loaded.".format(self.time()))

        #boot 3: read thirteen instructions of binary routine
        while self.registers.C!=X003LIB26: self.step()
        if self.displayMode&TAPECHARS: print()
        print("{:.3f}s: first 13 instructions of binary routine loaded.".format(
            self.time()))

        #boot 4: read rest of track one, until machine stops
        zebra.run(False, False)
        if self.displayMode&TAPECHARS: print()
        print("{:.3f}s: Track 0 loaded!".format(self.time()))

    def time(self):
        """Return zebra time in seconds."""
        return self.drum.time/3200.

    def status(self, registers=False):
        """Dump A-D and, when requested, nonzero registers.
        Also shows value of the carry trap and the way C was computed.
        """
        print("Time: {:.3f} seconds".format(self.time()))
        print(display('A', *split(self.registers.A)).ljust(38),
            "Carry =", self.carryTrap)
        print(display('B', *split(self.registers.B)))
        origin = self.origin
        if isinstance(origin, int): origin = "({})".format(origin)
        print(display('C', *split(self.registers.C)).ljust(38),
            "=", origin)
        print(display('D', *split(self.registers.D)).ljust(38),
            "eff.: {:.1%}%".format(self.efficiency()))
        if not registers: return
        for r in range(4, 16):
            if self.registers[r]:
                print(display("R"+str(r), *split(self.registers[r])))

    def memdump(self, lo, hi=None):
        if hi is None: hi = lo+20
        for adr in range(lo, hi):
            print(display(adr, *split(self.drum[adr])))

    def run(self, clear=True, U7=True, breakPoint=None, traceLength=0):
        """Run code, simulate dialer, until breakpoint, or stop.
        breakPoint can be:
            None: no breakpoint
            int: any memory access at that address
            (mask, value): any instruction with C&mask==value
        traceLength: if >0, that many last instructions are kept in self.trace
        Note: QueueZebra does not have a trace function.
        """
        if breakPoint is None:
            breakMask, breakValue = 0, 1
        elif isinstance(breakPoint, int):
            #W plus mem: W must be zero
            breakMask, breakValue = 0x41fff, breakPoint
        else:
            breakMask, breakValue = breakPoint
        trace = collections.deque(maxlen=traceLength)
        if traceLength: self.trace = trace
        #handle the clear and U7 keys
        if clear:
            self.clear()
        if U7:
            self.switches.pressU7()
        R = self.registers
        #loop until breakpoint
        while R.C&breakMask!=breakValue:
            #detect special instructions
            func = R.C>>18
            if func&fUMASK==fUMASK:
                stopped = self.handleSpecialInstructions()
                if stopped: return "Stop"
            #now do a step
            if traceLength: trace.append(self.registers.C)
            self.step()

    def handleSpecialInstructions(self):
        """Handle U7 and V7 instructions.
        Returns True if we have to stop because of that.
        """
        R = self.registers
        func = R.C>>18
        #V7: maybe a stop
        if func&fUVMASK==fUMASK and R.C+2==R.D: return True
        #check for dialer:
        #mode   instruction   action
        #NC     X...BCV7      just return from subroutine via R15
        #tape   X11KBCU7W     X10K with A=value
        if func==fXadrBCV7 and R.C&REGMASK==0 \
        or func==fX11KBCU7 and R.C==X11KBCU7:
            R.A = self.dial()
            R.D = R[15]-1
            if func==fX11KBCU7:
                #tapedial: do X10K with R15=result
                R.values[15] = R.A &MASK33
                R.C = X10K
            else:
                #normal code: return via R15
                R.C = R.D
        return False

    def dial(self):
        return int(input("Dial a number: "), 10) &MASK34

    def readLOT(self):
        """Read the LOT in memory, write protect, and reset"""
        for b in range(10, 17): self.drum.locks[b] = False
        for adr,f,r,d in readFRD(ZLOT6):
            self.drum[adr] = f<<18|r<<13|d
        #lock the default tracks: blocks 10 through 15, and track 0 (16)
        for b in range(10, 17): self.drum.locks[b] = True
        #reset
        self.clear()

    def countStep(self):
        """Update efficiency and stepCounter
        """
        self.stepCounter +=1
        now = self.drum.time
        if self.efficiencyTimestamps and now<self.efficiencyTimestamps[-1]:
            self.efficiencyTimestamps.clear()
        self.efficiencyTimestamps.append(now)
        tooOld = now-self.efficiencyWindow
        #remove old timestamps
        while self.efficiencyTimestamps[0]<=tooOld:
            self.efficiencyTimestamps.popleft()

    def efficiency(self):
        """Compute the percentage of time that instructions are actually
        performed. The original Zebra had a gauge for this.
        If the efficiency is high enough during an extended period of time,
        you could get over a hundred thousand instructions per minute...
        (We are over a million times faster now, that's why this program works.)
        """
        #as efficiencyTimestamps keeps all instructions that were done
        #for a period of length efficiencyWindow, we just divide
        #but if the elapsed time is smaller than that, the buffer isn't full
        #of course, this can lead to zero divisions
        try: return len(self.efficiencyTimestamps)/min(
                self.efficiencyWindow, self.drum.time)
        except ZeroDivisionError: return 0.5

    def dumpTrace(self, length=None):
        """If you made a trace by choosing run with traceLength>0,
        dump the trace.
        """
        if not hasattr(self, 'trace'): raise ValueError('There is no trace')
        if length is None: length = len(self.trace)
        for i in range(-length,0):
            print(display(str(i), *split(zebra.trace[i])))

    def memoryCallback(self, data):
        return self.callback("memory", data)
    def tapecharCallback(self, data):
        return self.callback("tapechar", data)
    def tapeendCallback(self, data):
        return self.callback("tapeend", data)
    def printerCallback(self, data):
        return self.callback("printer", data)
    def punchCallback(self, data):
        return self.callback("punch", data)
    def tapelineCallback(self, data):
        return self.callback("tapeline", data)
    def registerCallback(self, data):
        return self.callback("register", data)
    def accumulatorCallback(self, data):
        return self.callback("accumulators", data)
    def callback(self, device, data):
        """You can use this general routine,
        or speed up by using the specific ones"""
        raise NotImplementedError

class PrintZebra(Zebra):
    """Default Zebra implementation, with standard out as UI.
    """
    def callback(self, device, data):
        if device=="memory":
            addr, oldValue = data
            print("{:.3f}s. Writing:   {} = {}".format(
                self.time(),
                display(addr, *split(self.drum[addr])),
                (self.drum[addr]^(1<<32))-(1<<32)))
        elif device=="tapechar":
            print(data[2], end='', flush=True)
        elif device=="tapeend":
            print("tape: invalid char {:!r}".format(c))
        elif device=="printer":
            print(data, end='', flush=True)
        elif device=="punch":
            print('P{!r}'.format(TAPESYMBOLS[data]), flush=True)
        elif device=="tapeline":
            print("{:.3f}s. Tape: {}".format(self.time(), data))
        elif device=="register":
            reg, oldValue = data
            print("{:.3f}s. Writing:   {}".format(
                self.time(),
                display(reg, *split(self.registers[reg]))))
        else:
            #accumulator shouldn't happen here, only used by QueueZebra
            raise NotImplementedError("Unknown device in callback: "+device)

def runfile(name):
    os.chdir(os.path.dirname(__file__))
    zebra = PrintZebra()
    zebra.readLOT()
    zebra.drum.resetClock(realTime=True)
    zebra.inputTape.load(name)
    zebra.displayMode = TAPELINES|WRITEMEM
    print(zebra.run())
    print("Time: {:.1f}s".format(zebra.time()))

if __name__=='__main__':
    print("Welcome to the Zebra demo!")
    zebra = PrintZebra()
    zebra.readLOT()
    #TODO 4: command line interface with speed choice, displayMode, etc.
    if len(sys.argv)>1 and sys.argv[1]=='boot':
        #remove LOT, otherwise it isn't fair
        zebra.drum = Drum(True)
        zebra.displayMode = TAPECHARS
        print("Cold boot...")
        zebra.boot()
        zebra.status()

        print("Loading NIP...")
        #TODO 1: the tape dialer doesn't seem to work
        zebra.inputTape.load("ptt002rb")
        print("\n"+zebra.run(False, True))
        zebra.status()
    elif len(sys.argv)>1 and sys.argv[1]=='debug':
        zebra.inputTape.load('demo3')
        zebra.displayMode = TAPELINES
        zebra.run()
        zebra.switches.setSwitch(6)
        zebra.run()
        zebra.status()
    elif len(sys.argv)>1:
        #run a program immediately in real time
        zebra.drum.resetClock(realTime=True)
        zebra.inputTape.load(sys.argv[1])
        zebra.displayMode = TAPELINES|WRITEMEM
        print(zebra.run())
        print("Time: {:.1f}s".format(zebra.time()))
