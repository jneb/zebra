#!python3
"""Queued version of the zebra executable for running in the background
by the UI process.
Also adds an "undo" function, storing enough information at every instruction to allow to retrace execution.
"""

import queue
import traceback
from collections import deque
#just in case we have an old version of Python3
try: from time import perf_counter
except ImportError: from time import clock as perf_counter

import zebra
#displayMode constants for undo mode
UNDO = 1<<6
UNDOBUFSIZE = 3333

#print communication received from UI.
#This flag is manipulated by tkzebra
#don't change it here
COMMDEBUG = False

#time period to check for input commands when running full speed
LISTENDELAY = .1

class QueueZebra(zebra.Zebra):
    """Zebra implementation with queue control to be used as background process.
    Events are of three possible types:
    Name     Direction   Form                    Meaning
    read     UI->Zebra   ("R", device, args)     Reply if value is different
    set      UI->Zebra   ("S", device, args)     Set value
    reply(Z) Zebra->UI   (device, index, value)  Current value

    Device     RSZ  Meaning
    break       SZ  breakpoint as (mask, value) tuple. read: stopped with C
    debug      R Z  get debug data from zebra and print it
    dial        S   dialed value
    efficiency R Z  efficiency and elapsed time
    history    R Z  instruction history buffer   (not yet implemented)
    loadfrd     S   load FRD file                (not yet implemented)
    memlock     S   memory locks i=512*i..512*i+511, -1=track 0
    memory     RSZ  memory
    printer      Z  printer. Index: position in file
    punch        Z  punch. Index: position in file
    quit      Q     quit the background program
    register    S   register index: -2=origin, -1=carry, 0=C, 1=D, 2=A, 3=B
    register   R Z  register index: 4=4, ..., 31=31
    register     Z  counter index=-3 when undo is executed
    speed        Z  if 000 reached, breakpoint, or dialer
    speed       S   set speed: value 0=stop 1=step 2=real time 3=fast
    start        Z  tell UI that zebra is running
    switch      S   switch 1-7, 0=clear key
    tape        SZ  S: input tape file name (UI-Zebra); Z: index=pos, value=char
    undo        S   True/False: enable/disable undo mode, None: back 1 instr.
    undo        SZ  n: send n-th last C value (0=current C reg)
    """
    def __init__(self, inputQueue, outputQueue):
        self.inputQueue = inputQueue
        self.outputQueue = outputQueue
        self.memoryCheckValues = {}
        self.accumulatorCheckValues = {}
        self.registerCheckValues = {}
        self.punchCounter = self.printCounter = 0
        super().__init__()
        #these two flags are always on for the queued version
        self.displayMode = zebra.TAPECHARS | zebra.WRITEREG
        self.readLOT()

    def run(self):
        """Run loop for background processing, communicating with a UI program.
        """
        self.running = True
        self.stepping = False
        self.dialReason = ''
        #invalidate breakpoint
        self.breakMask = 0
        self.breakValue = 1
        #tell the UI that we are alive
        self.outputQueue.put(("start", None, None))
        timeToTalk = perf_counter()+LISTENDELAY
        while True:
            #poll input queue once every few instructions if running
            if not self.running or perf_counter()>timeToTalk:
                #check for commands
                #returns True if program is over
                if self.pollInputQueue(): return
                timeToTalk = perf_counter()+LISTENDELAY
            #break if requested or at special instruction (not if stepping)
            if not self.stepping:
                #stop at special instructions
                if (self.registers.C>>18)&zebra.fUMASK==zebra.fUMASK:
                    if self.handleSpecialInstructions():
                        #outputQueue is filled by the routine: stop now
                        self.running = False
                elif self.registers.C&self.breakMask==self.breakValue:
                    self.running = False
                    self.outputQueue.put(("speed", "break", 0))
            if self.running:
                #do one step
                try: self.step()
                except ValueError as e:
                    #if trouble, just restart and pass the error
                    self.outputQueue.put(("error", None, traceback.format_exc()))
                    self.running = False
                    continue
                #update accumulators if requested
                if self.accumulatorCheckValues:
                    self.accumulatorCallback("all")
            #stop machine and reply if stepping
            if self.stepping:
                self.outputQueue.put(("speed", "step", 1))
                self.running = self.stepping = False

    def setUndo(self, undo=True):
        """Turn undo on or off"""
        #note that WRITEREG is always on already
        #keep WRITEMEM on if needed for checking memory locations
        if undo:
            self.displayMode |= UNDO|zebra.WRITEMEM
            self.undoBuffer = deque(maxlen=UNDOBUFSIZE)
        else:
            if self.memoryCheckValues: self.displayMode &= ~UNDO
            else: self.displayMode &= ~UNDO & ~zebra.WRITEMEM
            del self.undoBuffer

    def step(self):
        """Step instruction, allow for undo if displayMode&UNDO
        """
        if self.displayMode&UNDO:
            #save relevant undo information
            R = self.registers
            undoInfo = { 'A': R.A, 'B': R.B, 'C': R.C, 'D': R.D,
                'origin': self.origin, 'counter': self.stepCounter}
            #get func shifted by one bit: AK QLRI BCDE V421
            func1 = R.C>>19
            #bit B or Q: save carry
            if func1&0x880: undoInfo['V'] = self.carryTrap
            #instruction dependent on switch: remember switch value
            if 0<func1&0xf<8: undoInfo['U'] = self.switches.readSwitch(func1&7)
            #these will be modified if a register or memory is written
            self.registerData = self.memoryData = None
            #do the step. registerCallback and memoryCallback will save old data
            super().step()
            if self.registerData is not None: undoInfo['reg'] = self.registerData
            if self.memoryData is not None: undoInfo['mem'] = self.memoryData
            #put all saved stuff in info buffer
            self.undoBuffer.append(undoInfo)
        else:
            super().step()

    def pollInputQueue(self):
        """Handle incoming results from zebra.
        return True if zebra has quit
        """
        try:
            #get next commands; block if nothing to do
            while True:
                (command, device, *args) = self.inputQueue.get(
                    block=not self.running)
                if COMMDEBUG: print("UI->Zebra:",command,device,*args)
                #quit on any command not equal to R,S
                if command not in ("R", "S"):
                    self.outputQueue.put(("quit",None,None))
                    return True
                #execute command
                getattr(self, command+device)(*args)
        except queue.Empty:
            pass
        except Exception as e:
            #report exceptions to UI, for user friendly error message
            self.outputQueue.put(("error",
                None, traceback.format_exc(limit=5)))
        return False

    def clear(self):
        #don't do three steps
        super().clear(False)

    def handleSpecialInstructions(self):
        """Handle U7 and V7 instructions.
        Returns True if we have to stop because of that.
        Dialing is a very special dance:
        - this routine detects the dial instruction, sends the (speed, dial, 0)
          and return True
        - caller .run() stops the process by setting running = False
        - UI gets the (speed, dial, 0) and generates a dialog for the user
        - dialog does callback, producing a (dial, value)
        - this process gets message in Sdial and sets A,C correctly
        - this process sends a (speed, dial, 1) to show that value was set
        - UI gets the message, calls .setSpeed() to send a speed command
        - this program gets the speed command in Sspeed and starts again
        """
        R = self.registers
        func = R.C>>18
        if func&zebra.fUVMASK==zebra.fUMASK and R.C+2==R.D==R[4]:
            self.outputQueue.put(("speed", "stop", 0))
            return True
        if func==zebra.fXadrBCV7 and R.C&zebra.REGMASK==0:
            #the dial routine in the LOT
            self.dialReason = '39P'
            self.outputQueue.put(("speed", "dial", 0))
            return True
        if func==zebra.fX11KBCU7 and R.C==zebra.X11KBCU7:
            #apparently a dial routine from tape
            self.dialReason = 'tape'
            self.outputQueue.put(("speed", "dial", 0))
            return True
        return False

    def memoryCallback(self, data):
        """Notify UI that memory has changed
        register old value in memoryData for undo
        """
        #save old data for undo
        address, self.memoryData = data
        assert 0<=address<8192
        value = self.drum.data[address]
        if address in self.memoryCheckValues \
                and value!=self.memoryCheckValues[address]:
            self.outputQueue.put(("memory", address, value))
            #turn off checking for this location
            del self.memoryCheckValues[address]
            #remove check flag if nothing to check anymore
            if not self.memoryCheckValues and not self.displayMode&UNDO:
                self.displayMode &= ~zebra.WRITEMEM

    def registerCallback(self, data):
        """Notify UI that register has changed
        old value of register is saved for undo
        """
        #save old data for undo
        address, self.registerData = data
        if 4<=address<16:
            value = self.registers[address]
            if address in self.registerCheckValues \
                    and value!=self.registerCheckValues[address]:
                self.outputQueue.put(("register", address, value))
                #turn off checking for this location from now on
                try: del self.registerCheckValues[address]
                except KeyError: pass
        elif 16<=address<32:
            #writes to special registers are not sent to host
            #TODO 3: undo punch and printer
            pass
        elif 0<=address<4:
            #ignored by hardware
            pass

    def accumulatorCallback(self, data):
        """Notify UI that one of A,B,C,D,V,origin,Counter has changed
        old value of register is saved for undo
        most of the time, data = "all" so we do everything
        """
        for data, value in (
                (2, self.registers.A),
                (3, self.registers.B),
                (0, self.registers.C),
                (1, self.registers.D),
                (-1, self.carryTrap),
                (-2, self.origin),
                (-3, self.stepCounter)):
            if data in self.accumulatorCheckValues \
                    and value!=self.accumulatorCheckValues[data]:
                self.outputQueue.put(("register", data, value))
                #turn off checking for this location from now on
                try: del self.accumulatorCheckValues[data]
                except KeyError: pass

    def tapecharCallback(self, data):
        """Notify UI that tape character is read
        """
        line, col, char = data
        self.outputQueue.put(("tape", (line, col), char))

    def callback(self, device, data):
        """General callback from running a Zebra instruction: something happened
        that may need a special action.
        frequent cases are handled above by specific callback functions above.
        We tell the world via outputQueue
        """
        if device=="tapeend":
            self.outputQueue.put(("tape", None, ''))
            #stop; tell UI
            self.running = False
            self.outputQueue.put(("speed", "tape", 0))
        elif device=="printer":
            self.outputQueue.put(("printer", self.printCounter, data))
            self.printCounter +=1
        elif device=="punch":
            self.outputQueue.put(("punch", self.punchCounter, data))
            self.punchCounter +=1
        else:
            print("Warning: unknown device:", device, data)

    def setRunning(self):
        """Set self.running to True,
        and nudge the zebra if it was in a stop condition.
        """
        if not self.running:
            #do a step to get out of the stop condition
            self.step()
        self.running = True

    def Sbreak(self, mask, value):
        """Set breakpoint as (mask, value) tuple
        mask = index, value = value
        """
        self.breakMask, self.breakValue = mask, value

    def Rdebug(self, attr=None):
        """For debug: read any attribute
        """
        if attr is None: attr = "dir(self)"
        self.outputQueue.put(("debug", attr, eval(attr)))

    def Sdial(self, value):
        """User dialed a number; tell the UI that we want to run again.
        """
        value &= zebra.MASK34
        R = self.registers
        #enter value in A
        R.A = value
        #to be honest, I am not sure why this is needed;
        #but the subroutine would do this
        R.D = R[15]-1
        if self.dialReason=='39P':
            #normal code: return via R15
            R.C = R.D
        elif self.dialReason=='tape':
            #tapedial: do X10K with R15=result
            R.values[15] = R.A &zebra.MASK33
            R.C = zebra.X10K
        else: raise NotImplementedError("Unexpected dial request")
        #pass on register update
        self.accumulatorCallback("all")
        #tell what happened
        self.outputQueue.put(("speed", "dial", 1))

    def Refficiency(self):
        """Read efficiency,
        elapsed time and number of instructions.
        """
        self.outputQueue.put(("efficiency",
            self.efficiency(),
            (self.time(), self.stepCounter)))

    def Sloadfrd(self, path):
        """Read FRD file"""
        #save old value of locks
        oldLocks = list(self.drum.locks)
        #clear locks so we can write
        for i in range(17): self.drum.locks[i] = False
        #do the actual writing
        for adr,f,r,d in zebra.readFRD(path):
            self.drum[adr] = f<<18|r<<13|d
        #reset locks
        for i,b in enumerate(oldLocks): self.drum.locks[i] = b

    def Smemlock(self, index, value):
        """Set memory locks
        index 0..15: lock track 16*i..16*i+15, address 512*i..512*i+511
        index 16: track 0, address 0..31
        """
        assert -1<=index<16 and value in (0,1)
        self.drum.locks[index] = bool(value)

    def Rmemory(self, index, value=None):
        """Read memory: reply to UI if value different from value.
        """
        self.memoryCheckValues[index] = value
        #activate callback
        self.displayMode |= zebra.WRITEMEM
        #check immediately
        self.memoryCallback((index, None))

    def Smemory(self, index, value):
        """Set memory.
        """
        assert 0<=index<8192 and 0<=value<=1<<33
        self.drum.data[index] = value

    def Rregister(self, index, value=None):
        """Read register: reply to UI if value different from value
        index: -1=carry, 0=C, 1=D, 2=A, 3=B, 4=4, ..., 31=31
        """
        if -3<=index<4:
            self.accumulatorCheckValues[index] = value
            #check immediately, there may be a difference already
            self.accumulatorCallback(index)
        elif 4<=index<16:
            self.registerCheckValues[index] = value
            #check immediately, there may be a difference already
            self.registerCallback((index, None))
        else:
            #TODO 3: 1369 read special registers without side effects, warn in register panel: R31*
            #R23 always is 0x100000000
            #R24 always is A&B: should be treated as accumulator
            #R26-R30 are bits of the tape input: update at advanceTape
            #R31 reads as 0, triggers advanceTape (that we won't do of course)
            #others return 0
            raise NotImplementedError("Read special register")

    def Sregister(self, index, value):
        """Set register
        index: -1=carry, 0=C, 1=D, 2=A, 3=B, ...
        """
        if index==-1:
            assert value in (0,1)
            self.carryTrap = value
        elif 0<=index<4:
            name = 'CDAB'[index]
            #A is one bit longer
            assert 0<=value<1<<33+(index==2)
            setattr(self.registers, name, value)
            if name=='C': self.origin = 'Set'
            self.accumulatorCallback(index)
        elif index<16:
            self.registers.writeReal(index, value)
            self.registerCallback((index, None))
        else:
            raise NotImplementedError("Write special register")

    def Sspeed(self, value):
        """Set speed as requested by master process.
        Confirm the change.
        0=stop 1=step 2=real time 3=fast
        """
        if value==0:
            #stop
            self.running = False
        elif value==1:
            #step (running will be reset after one instruction)
            self.running = self.stepping = True
        elif value<4:
            #speed setting
            self.drum.resetClock(realTime=value==2)
            self.setRunning()
        else:
            raise NotImplementedError("Sspeed")
        self.outputQueue.put(("speed", "set", value))

    def Sswitch(self, index, value):
        """Set switch 1-7, 0=clear key, clearing all registers
        0=off 1=on
        clear key is a "push button", any value clears.
        """
        if index==0:
            #clear registers
            self.clear()
            self.accumulatorCallback(index)
        elif 0<index<8:
            self.switches.value &= ~(1<<index)
            if value: self.switches.value |= 1<<index
        else:
            raise NotImplementedError("Sswitch")

    def Stape(self, value):
        """Set input tape file name.
        """
        self.inputTape.load(value)
        #reset tape char counter

    def Sundo(self, value=None):
        """Enable/disable or read undo buffer.
        Value is False/True: enable or disable.
        Value is None: perform an undo step;
        will lead to error if undo is disabled.
        """
        R = self.registers
        if value is None:
            undoInfo = self.undoBuffer.pop()
            for reg in 'ABCD': setattr(R, reg, undoInfo[reg])
            self.origin = undoInfo["origin"]
            self.stepCounter = undoInfo["counter"]
            if 'V' in undoInfo: self.carryTrap = undoInfo['V']
            self.accumulatorCallback("all")
            if 'reg' in undoInfo:
                reg = R.C>>13&31
                R.writeReal(reg, undoInfo['reg'])
                self.registerCallback((reg, None))
            if 'mem' in undoInfo:
                mem = R.C&8191
                self.drum[mem] = undoInfo['mem']
                self.memoryCallback((mem, None))
            #tell UI where we are so it can update trace window
            #TODO 2: make -3, -2 etc special values for self-documentation
        else:
            #switch undo on or off
            self.setUndo(value)

    def Rundo(self, firstRequested, length):
        """Send previous values of C,origin for a range of step values
        if firstRequested=0: stepCounter-length+1 up to stepCounter
        else: firstRequested up to firstRequested+length-1
        A None at the beginning of the result list means
        no earlier data is available
        """
        result = []
        #compute offset in undo buffer
        if firstRequested==0:
            #length-1 values from the buffer, one from current value
            #restrict length to available values
            if length>len(self.undoBuffer)+1:
                result = [None]
                length = len(self.undoBuffer)+1
            firstReturned = self.stepCounter-length+1
            for offset in range(1-length, 0):
                undoEntry = self.undoBuffer[offset]
                result.append((
                    undoEntry['C'],
                    undoEntry['origin'],
                    ))
            #add current value
            result.append((self.registers.C, self.origin))
        else:
            #the oldest instruction available
            firstAvailable = self.undoBuffer[0]["counter"]
            firstReturned = firstRequested
            offset = firstRequested-firstAvailable
            end = offset+length
            #clip off values that aren't available
            if offset<0:
                result = [None]
                firstReturned = firstAvailable
                offset = 0
            for step in range(offset, end):
                undoEntry = self.undoBuffer[step]
                result.append((
                    undoEntry['C'],
                    undoEntry['origin'],
                    ))
        self.outputQueue.put(("undo", firstReturned, result))

