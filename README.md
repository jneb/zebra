# Stantec Zebra emulator #

Emulator for the very very old computer (1954), the Stantec Zebra.
Designed by Willem Louis van der Poel; the design of this computer used a
stunningly small number of parts (about 1000 valves and transistors), but still
it was a relatively successful product: 55 were sold, mainly in Britain and the
Netherlands.
Due to reliability and cost constraints, it was necessary to be very creative,
and Willem van der Poel did not disappoint. The ingenuity of the design
resulted in a 33 bit machine with 8192K words of memory, while the electronics
was only a handful of latches and adders. Even the accumulator registers were
not electronic: they used a special area of the drum where the read and write
head were closer together.
Multiplication was possible using a clever "repeat" construct, that allowed to
repeat one or two instructions at full speed (here, the program counter was
reused as a repetition counter!). This made is possible to do a multiplication
in 33 cycles (one instruction per bit), division in two instructions per bit,
but also other things like checksumming the drum.

Inspired by the zebra emulator in Pascal from the awesome Computer Conservation
Society, I wrote one in Python.
The original doesn't run under current versions of Windows anymore, you need
another emulator for that. This one is more flexible, faster, and allows to do
things like tracing and stuff.

This emulator has one important feature that the original lacked: it can
compute the actual time a Zebra computer would take, and even delay to that
speed to get the original frustration. For example, solving a 50\*50 system of
equations took 40 minutes!
Also, this emulator probably does do the carry trap right; the original version
cannot be exactly correct (considering the source).  It is a bit hard to check
without an original Zebra at hand :-).

### Running the program ###

The UI version is the most interesting: just run "tkzebra.py" to see everything
in action.  You can also add a filename to run that program instead of the default
"tapes\demo1.zsc".
The actual Zebra emulator, zebra.py, is also useable as a separate command line
program.
You can give it "boot" as parameter, to emulate the actual reboot process, or
"debug" to run DEMO3 with printing enabled, or a zebra file name to run it.
For example, you can run "zebra demo1" to see the Simple Code program for
squares in real Zebra speed.

Interactively, you can run "python -i zebra.py" and use the variable "zebra" to
work with.

There is also a notes file (in the docs directory) that shows my efforts to
write a disassembly of the operating system. This is a still a work in
progress, though many of it was needed to understand this machine well enough
to get the emulator running.

### trace ###

There is an experimental symbolic tracer in the trace directory: this has
helped me to reverse engineer the NIP (Normal Input Progam in order to be able
to understand normal code tapes in the tapes directory. See docs/nipnotes.txt.
If you run "ztrace" in the trace subdirectory you will get a lot of output
consisting of all instructions executed, annotated automatically in a way
that should be easier to understand with the current level of understanding of
how computers work.
The annotator will automatically determine contents of registers in a generic
way if it can; you can help in by adding better explanations in regnames.txt

### Files ###

The most important file is zlot6.frd, which is encoded in base64 and added in
the tapes directory.
To get the full set of zebra files, get the zebra repository from
http://www.computerconservationsociety.org/software/zebra/zebra.zip.
Unpack (with unzip, not winzip), and the path to this in the program, then run.
It works best in interactive mode, where you can play with the controls.

There are no dependencies.

### Contribution ###

All help is welcome, send me a mail.
I am especially looking for information on the "Manual" button, and would like
to have a copy of the Normal Code reference manual.

### Thanks ###

First, thanks to my daughter, who was willing to draw a set of cute zebra
pictures to liven the thing up.
And to Willem van der Poel, whose minimalistic design inspired me to start on
this whole project, and who Don Hunter, who made the predecessor of this
emulator, which was so good that I had to raise my standard.

### Who do I talk to? ###

Jurjen N.E. Bos

Email: jnebos on the com domain of the google-based mail server.
