#!python3
"""The state of a traced zebra
"""
#TODO: TVP doesn't appear to work properly

from collections import defaultdict, ChainMap
import linecache
import sys
from operator import attrgetter

from expression import Expression, signExtend, ParseAndCompute
from expression import KnownVariable, BoundedVariable, BoolVariable
from expression import Depends, DependsOnCondition, VariableInstruction
from expression import LOW34, HIGH34, NEGATIVE, DRUMSIZE, HALF, WORDSIZE
from zebra import assemble, display, split, useless
from zebra import MASK33, ADDRMASK, fA000W
from zebra import Tape, UnknownInstruction, FunctionBits
from explain import Explainer

ZTRACEDB = 'regnames.db'
INDENT = 5*" "

class UnusedInstruction(UnknownInstruction): pass

class RegisterBank:
    """A dictionary like class with names and Properties.
    reg: dict from names to OldExpression of values in parameters
    Context of registers is set at retrieval time.
    #>>> z = RegisterBank().zebraSetup()
    #>>> z['A'].signedness = True
    #>>> z['A']<10
    Traceback (most recent call last):
      ...
    Depends: ('Unpredictable comparison', 'iA:(..)<10')
    """
    #TODO 2: store into V needs to go via variable
    def __init__(self):
        self.reg = {}
        defaultMapping = {'1': KnownVariable('1', 1)}
        self.context = ChainMap(defaultMapping)
        self.verbose = True
    def __repr__(self):
        return 'RegisterBank({'+', '.join(
            '{}:{}'.format(r,v)
            for r,v in self.reg.items()
            if str(v) not in ('i'+str(r), 'iR'+str(r))
            )+'})'

    def modify(self, parameterValues):
        """Make copy of self with given modified parameters.
        """
        raise NotImplementedError
        result = RegisterBank()
        #set new context
        context = result.context = self.context.new_child(parameterValues)
        #copy all the register assignments with the new context
        result.reg = {
            name:OldExpression(
                value, value.wordsize, value.signedness, context)
            for name, value in self.reg.items()}
        return result

    def zebraSetup(self):
        """Make a zebra computer setup.
        Sets up all registers except C.
        #>>> z = RegisterBank().zebraSetup()
        #>>> z['B']
        iB:(..)
        """
        self['A'] = Expression(BoundedVariable('iA', LOW34, HIGH34))
        for regName in 'BCD':
            self[regName] = Expression(BoundedVariable('i'+regName))
        for regNum in range(4, 16):
            self[regNum] = Expression(BoundedVariable('iR'+str(regNum)))
        #R26 to R31 are virtual
        #carry flag and switches are OldExpression of boolean variables
        #switches to off, U7 to on
        for switch in range(1,7):
            switch = 'U'+str(switch)
            self[switch] = Expression(BoolVariable(switch))
        #U7 is the start key that's default on
        self['U7'] = Expression(KnownVariable('U7', True))
        self['U7'] = Expression(BoolVariable('U7'))
        #the carry flag
        self['V'] = Expression(BoolVariable('V'))
        return self

    def __getitem__(self, name):
        """Get a register value within the proper context
        """
        return self.reg[name]

    def __setitem__(self, name, value):
        """Set register.
        """
        if name in (0, 1, 2, 3, 23):
            #TODO 7: extend this to "anything not initialized by zebraSetup"
            if self.verbose:
                print(INDENT, "->   R{}: write ignored".format(name))
            return
        self.reg[name] = self.reduce(value, 33+(name=='A'))

    def __contains__(self, name):
        return name in self.reg

    def keys(self):
        return self.reg.keys()

    @staticmethod
    def reduce(value, bits=33):
        """Interpret value properly to fit in 33 bits.
        Not implemented; to be improved.
        """
        word = 1<<bits
        half = word>>1
        #don't change if in range
        try:
            if -half<=value<word: return value
        except Depends: pass

        #if value is not known yet, don't do a thing
        try: value = int(value)
        except Depends: return value
        return value&word-1

class TapeReader:
    """Special version of the tape reader,
    that is used to keep track of current tape position,
    output to the registers, and chosen tape characters.
    """
    #TODO 8: allow reading files
    def __init__(self, state, tape=''):
        """Connect to a state for accessing the parameters and registers.
        """
        self.state = state
        #initialize the character table
        self.charValue = Tape().charValue
        #set the tape to empty, character unknown, don't advance when reading
        self.pos = 0
        self.tape = tape
        self.advanceFlag = False
        if tape: self.show()

    def readReg(self, reg, verbose=True):
        """Give reply of reading R26..R30
        Advances tape when the first bit of a new character is read.
        """
        assert 26<=reg<31
        if self.advanceFlag:
            self.pos += 1
            self.advanceFlag = False
            char = self.tape[self.pos:self.pos+1]
            if char:
                self.show()
        bitNumber = 30-reg
        char = self.tape[self.pos:self.pos+1]
        name = 't{}b{}'.format(self.pos, bitNumber)
        if char in self.charValue:
            #readable char, choose proper bit
            char = self.charValue[char]
            prop = KnownVariable(name, char>>bitNumber&1, reuse=True)
        else:
            #unreadable or unknown char
            prop = BoolVariable(name, reuse=True)
        return -Expression(prop)

    def adjustParams(self):
        """Set parameters to reflect new value of current tape character
        This is meant to change a tape character that is already read!
        """
        #TODO 4: make this work again
        return
        char = self.tape[self.pos:self.pos+1]
        context = self.state.registers.context
        for bitNumber in range(5):
            param = 't{}b{}'.format(self.pos, bitNumber)
            if char: bit = KnownVariable(param, self.charValue[char]>>bitNumber&1)
            else: bit = BoolVariable()
            #create parameter
            context[param] = bit

    def advance(self):
        """Call this when R31 is read to set the advance tape flag.
        Next access to a tape register adjusts registers.
        """
        self.advanceFlag = True

    def setTape(self, newTape):
        """Set the next characters read to the characters of newTape.
        The first character immediately replaces the character in R26..R30,
        but also the parameters that control them,
        effectively changing other registers that depend on it!
        Set newTape to empty in order to "unknow" the current tape character.
        Unsets the advance flag.
        Use ''.join(self.readTapeFile(path)) if you want the contents of a file.
        """
        #put the newTape string at the proper location in the total tape string
        #cut off old future characters, fill unknown characters with '?'
        self.tape = self.tape[:self.pos].rjust(self.pos, '?')+newTape
        self.advanceFlag = False
        self.adjustParams()

    def show(self):
        """Show current state of tape.
        """
        lo = self.pos-12
        if lo<0: lo = 0
        hi = lo+24
        if hi>len(self.tape): hi = len(self.tape)
        print(*map('{:3d}'.format, range(lo, hi)), sep='')
        if hasattr(self, 'faketape'):
            tape = self.faketape
        else:
            tape = self.tape
        print(*('{:>3s}'.format(tape[i]) for i in range(lo, hi)), sep='')
        print('   '*(self.pos-lo), ' ',
            (' !' if self.advanceFlag else ' ^'),
            '=', self.charValue[self.tape[self.pos]],
            sep='')

    def readTapeFile(self, path):
        """Convert a tape file to a character generator.
        """
        comment = False
        for lineNo, line in enumerate(open(path, 'r'), start=1):
            for c in line:
                #read character
                if c=='}':
                    #end comment at }
                    comment = False
                elif comment:
                    #ignore comment
                    pass
                elif c in self.charValue:
                    #actual useful contents, translate to zebra code
                    yield c
                elif c.isspace():
                    #ignore whitespace
                    pass
                elif c=='{':
                    #start comment at {
                    comment = True
                else:
                    #invalid symbol treated as end of tape
                    return

class State:
    """The virtual state of a Zebra
    registers: a RegisterBank
    R26=SetVariable((-1,0)), etc
    During run:
        R31 sets R26..R30 to BoundedVariable
        memory read gives "M...", and adjusts assumption for M...
        stop if C register is a BoundedVariable
    """
    def __init__(self, registers=None, tape=''):
        """Give only value for C under default assumptions,
        or full set of registers and assumptions.
        """
        if registers is None:
            registers = RegisterBank().zebraSetup()
            self.origin = "Restart"
        elif isinstance(registers, int):
            #value of register C is given
            regC = registers
            registers = RegisterBank().zebraSetup()
            registers['C'] = regC
            self.origin = "Start"
        # setup drum and dialer
        self.drumAccess(None)
        self.setDialer(None)
        self.registers = registers
        self.tapeReader = TapeReader(self, tape)
        self.instructionCounter = 0
        self.verbose = True

    def __repr__(self):
        C = self.registers['C']
        try:
            return display('State', *split(int(C)))
        except Depends:
            return 'State' + repr(C)

    def step(self):
        """Compute next state.
        Use a nicely split readable routine.
        raises Depends if instruction cannot be predicted unconditionally
        """
        if self.verbose:
            self.showInstruction()
        try:
            #C is always interpreted positive
            C = int(self.registers['C'])&MASK33
        except Depends as e:
            raise VariableInstruction('C', e.expression)
        self.instructionCounter +=1
        self.func, self.reg, self.mem = C>>18, C>>13&31, C&ADDRMASK
        #bit must be set after testCondition, because func is changed there
        self.testCondition()
        bit = self.bit = FunctionBits(self.func)
        registerValue = self.getRegister()
        drumValue = self.getMemory()
        addAB = self.preadders(registerValue, drumValue)
        addAB = self.invert(*addAB)
        addA, addB, newCarry = self.doCLR(*addAB)
        #adder of C: add D or date to optional register
        if bit.A: addC = self.registers['D']
        else: addC = drumValue
        if bit.K: addC += registerValue
        self.store(addA, addB, addC, newCarry)
        #print memory read
        if bit.D or bit.W:
            #memory isn't read
            pass
        elif bit.K and bit.X and self.verbose:
            #memory location is read to get instruction
            self.showMemory(self.mem)
        #compute origin for next instruction
        self.origin = self.nextOrigin()

    def nextOrigin(self):
        """Produce a string explaining where the value of C comes from.
        """
        # TODO 6: combine this function with corresponding code in explain
        # explain next instruction to user in self.origin
        reg, mem = self.reg, self.mem
        bit = self.bit
        if bit.A: origin = "D"
        elif bit.W or bit.D: origin = ""
        else:
            if bit.E and reg==24: origin = "({})&R5".format(mem)
            else: origin = mem
        if bit.K and not bit.E:
            #add register to origin
            regS = "01AB"[reg] if reg<4 else 'R'+str(reg)
            if isinstance(origin, int): origin = "({:03d})".format(mem)
            if origin=="": origin = regS
            else: origin += "+"+regS
        return origin

    def showInstruction(self):
        """Print the instruction that is executed
        show exact instruction if it can be determined.
        Otherwise, print constant part as normal instruction,
        and show variable part separately
        """
        try:
            regC = int(self.registers['C'])&MASK33
            result = display(self.origin, *split(regC),
                explainCondition=False)
            try: explanation = Explainer(self.origin, *split(regC))()
            except UnknownInstruction as e: explanation = "?: "+e.args[0]
            if len(result)<40:
                print(result.ljust(40)+explanation)
            else: print(result)
            return
        except Depends:
            offset,variablePart = self.registers['C'].splitConstant(True)
            result = display(self.origin, *split(offset),
                explainCondition=False)
            print(result+variablePart)

    def testCondition(self):
        """Check if condition is satisfied, and set instruction to A otherwise
        """
        # check condition and skip if needed
        # funcVn=1..7 for U1..U7, 9..15 for V1..V7
        func = self.func
        funcVn = func>>1 & 0xf
        R = self.registers
        if funcVn==0: doInstr = True
        elif funcVn<8:
            try: doInstr = int(R['U'+str(funcVn)])
            except Depends as e:
                raise DependsOnCondition('U'+str(funcVn), e.expression)
        elif funcVn==8: doInstr = True
        elif funcVn==9:
            #first do simple test for highest bit
            #note that bit 32 of register A is tested, not bit 33!
            #TODO 4: BUG: doesn't know what to do at 002497fa3 (bug is already at 06c03bfbb where A turns into uA0, and then back?)
            try: doInstr = R['A']&HALF
            except Depends as e:
                raise DependsOnCondition('A<0', e.expression, 'A')
        elif funcVn==10:
            try: doInstr = R['B']&HALF
            except Depends as e:
                raise DependsOnCondition('B<0', e.expression, 'B')
        elif funcVn==11:
            try: doInstr = R['A']!=0
            except Depends as e:
                raise DependsOnCondition('A!=0', e.expression, 'A')
        elif funcVn==12:
            try: doInstr = R['B']&1
            except Depends as e:
                raise DependsOnCondition('B&1', e.expression, 'B')
        elif funcVn==15:
            raise ValueError("Dialer")
        else: raise ValueError("Unknown instruction")
        # if condition fails, replace instruction by A000W
        if not int(doInstr):
            self.func, self.reg, self.mem = fA000W, 0, 0
            if self.verbose: print("Instruction skipped")
        else:
            expl = useless(R["C"])
            if expl:
                raise UnusedInstruction(expl, Explainer(None, self.func, self.reg, self.mem)())

    def getRegister(self):
        """Value coming from the register bank for this instruction.
        """
        # read applicable register, including side effects
        bit = self.bit
        R = self.registers
        reg = self.reg
        if bit.E: return 0
        elif reg in R: return R[self.reg]
        elif reg==0: return 0
        elif reg==1: return 1
        elif reg==2:
            #note: register 2 is A without the extra bit!
            return int(R['A']) & MASK33
        elif reg==3: return R['B']
        elif reg==23: return HALF
        elif reg==24: return R['A'] & R['B']
        elif 26<=reg<31:
            return self.tapeReader.readReg(reg)
        elif reg==31:
            self.tapeReader.advance()
            return 0
        raise ValueError("Unknown register:", reg)

    def getMemory(self):
        """read memory, including AND mask effect of write to reg 24.
        Result in 0..WORDSIZE
        Reads negative numbers as negative.
        """
        bit = self.bit
        drumValue = 0
        if not bit.W and not bit.D:
            self.drumAccess(self.mem)
            drumValue = self.readMemoryOrEmpty(self.mem)
            if bit.A and self.verbose: self.showMemory(self.mem)
        #special action for E24
        if bit.E and self.reg==24:
            if verbose:
                self.showRegister(5)
            drumValue = self.registers[5]&drumValue
        return drumValue

    def readMemoryOrEmpty(self, addr):
        try:
            result = drum[addr]
            #sign extend
            #TODO 5: handle dExpression on drum
            if result>>32: result -= WORDSIZE
        except KeyError:
            newParam = 'M'+str(addr)
            result = Expression(BoundedVariable(newParam))
        return result

    def preadders(self, registerValue, drumValue):
        """Compute output of both preadders, including V and Q bits"""
        bit = self.bit
        R = self.registers
        context = R.context
        #left input of both preadders
        if not bit.A and bit.D:
            #XD special operations
            leftInput = R[15]
        elif bit.K:
            leftInput = 0
        else:
            leftInput = registerValue

        #bring to selected preadder
        if bit.B: addA, addB = 0, leftInput
        else: addA, addB = leftInput, 0
        #note that the preadder of A adds 33 bit signed values with a 34 bits result
        #TODO 1: this is where much of the trouble comes from, because a general number cannot be sign extended
        addA = signExtend(addA)
        #multiplication feature works for only for A, independent of B bit
        if bit.LR and int(R['B']&1): addA += signExtend(R[15])

        if self.func & 0x1e==0x10:
            #V instruction
            #add carry to A; first make fresh variable so we can rename it
            carryProp = R['V'].asVariable()
            #make unique expression for this carry value
            carryProp.name = 'V'+str(self.instructionCounter)
            carry = Expression(carryProp)
            addA += 1-carry if bit.I else carry
            if bit.L and not bit.R and int(carry)^bit.I:
                #TODO 1: handle unknown carry in case of Lr
                #delay carry on left shift, as specified by Willem
                if bit.A and not bit.B and addA.parity() and drumValue&1:
                    #since carry is implemented as an OR on bit 1,
                    #we will get a wrong result; emulate this faithfully
                    addA -=1
                else:
                    #nothing to worry about: do it right
                    addA +=1

        #increment B if needed
        if bit.Q: addB +=1

        #add right input of preadders
        if bit.A:
            if bit.B: addB += drumValue
            else: addA += signExtend(drumValue)
        return addA, addB

    def invert(self, addA, addB):
        """Handle action of I bit"""
        #invert output of preadders
        if self.bit.I:
            addA = -addA
            #the carry behaviour of B is fixed in computeCarry
            addB = -addB
        return addA, addB

    def doCLR(self, addA, addB):
        """Handle action of C,L,R bits
        For B addition, the addition is made explicit for computeCarry
        """
        #do the shift, clear and adder operation of A and B
        R = self.registers
        bit = self.bit
        bitnC = not self.bit.C
        shiftedA = shiftedB = 0
        if bit.R:
            #handle right shift for R and LR
            if bitnC:
                #double shift; transport bit from A to B
                #note A is not sign extended, but the extra bit is used
                shiftedA = signExtend(R['A'], 34)>>1
                shiftedB = R['B']>>1
                #carry
                carry = R['A']&1
                shiftedB += carry<<32
            elif bit.B:
                shiftedA = signExtend(R['A'], 34)>>1
            else:
                shiftedB = R['B']>>1
        elif bit.L:
            #handle left shift
            if bitnC:
                try:
                    carry = R['B']>>32&1
                except Depends:
                    carry = Expression(BoundedVariable('B32', 0, 1))
                try:
                    shiftedA = (R['A']<<1) + carry
                except Depends:
                    shiftedA = Expression(BoundedVariable('uA'))
                shiftedB = R['B']<<1
            elif bit.B:
                shiftedA = R['A']<<1
            else:
                shiftedB = R['B']<<1
        elif bitnC:
            #no clear; actual register values are used
            shiftedA = R['A']
            shiftedB = R['B']
        elif bit.B:
            shiftedA = R['A']
        else:
            shiftedB = R['B']
        #accumulator adding
        addA += shiftedA
        newCarry = self.computeCarry(shiftedB, addB)
        addB += shiftedB
        return addA, addB, newCarry

    def store(self, addA, addB, addC, newCarry):
        """Write computed values to registers and memory"""
        #store results
        bit = self.bit
        R = self.registers
        #only compute writeValue if needed
        if bit.E or (bit.D and not bit.W):
            writeValue = RegisterBank.reduce(
                self.registers['AB'[bit.B]],
                (33 if bit.B else 34))
        if bit.E:
            R[self.reg] = R['D'] if bit.K else writeValue
        if bit.D and not bit.W:
            self.drumAccess(self.mem)
            drum[self.mem] = int(writeValue & MASK33)
            # always print stores
            self.showMemory(self.mem, True)

        #update registers; do D before C since D needs the old value of C
        if bit.A: R['D'] = R[4]
        else: R['D'] = int(R['C'])+2 &MASK33
        R['A'] = addA
        #set carry trap if needed: according to Willem, at B or Q
        if bit.Q or bit.B:
            #store carry
            #TODO 3: is this efficient enough?
            R['V'] = Expression(newCarry)
        R['B'] = addB
        R['C'] = addC

    def computeCarry(self, x, y, wordsize=33):
        """Predict possible carry of x and y when added by a Zebra.
        Takes the I bit into account: computes the borrow as defined in the zebra.
        Carry of x+y (defined independent of sign of x and y):
        (x+y>>33) - (x>>33) - (y>>33)
        Borrow of x-y equals carry of x+~y+1 = x+WORDSIZE-y:
        1 + (x-y>>33) - (x>>33) + (y>>33)
        Returns a variable describing the carry: 0, 1 or ?
        Note: wordsize only used for test; normal code always has 33
        >>> s=State()
        """
        try:
            if isinstance(x, int): xLo = xHi = x
            else: xLo, xHi = x.limits()
            if isinstance(y, int): yLo = yHi = y
            else: yLo, yHi = y.limits()
            if 0==xLo==xHi: return KnownVariable('V', 0)
            if 0==yLo==yHi: return KnownVariable('V', self.bit.I)
            xLoS, xHiS = xLo>>wordsize, xHi>>wordsize
            yLoS, yHiS = yLo>>wordsize, yHi>>wordsize
            if self.bit.I:
                #subtract
                resLo, resHi = xLo-yHi>>wordsize, xHi-yLo>>wordsize
                if xLoS==xHiS and yLoS==yHiS and resLo==resHi:
                    return KnownVariable('V', 1+resLo-xLoS+yLoS)
                return BoolVariable('V', 1+resLo-xHiS+yLoS, 1+resHi-xLoS+yHiS)
            else:
                #add
                resLo, resHi = xLo+yLo>>wordsize, xHi+yHi>>wordsize
                if xLoS==xHiS and yLoS==yHiS and resLo==resHi:
                    return KnownVariable('V', resLo-xLoS-yLoS)
                return BoolVariable('V', resLo-xHiS-yHiS, resHi-xLoS-yLoS)
        except Depends:
            return BoolVariable('V')

    def drumAccess(self, addr):
        """Register drum access, and keep time.
        """
        if addr is None:
            # set up
            self.time = 0
        elif self.lastAddress is not None:
            distance = (addr-self.lastAddress-1)%32+1
            if distance>5 and self.verbose:
                print(INDENT, '-'*10, "Drum rotated", distance, "places")
            self.time += distance
        self.lastAddress = addr

    def setDialer(self, num):
        """Set the dialer string as if a given number is dialed
        right at this moment.
        A digit is a series of 60 ms impulses, separated by 40 ms.
        End of digit is a 100 ms pulse.
        End of string is a 1.5 s pulse.
        The dialer string denotes for every drum rotation of 10 ms:
        0: U7 succeeds
        1: U7 fails (as if button is pressed)
        """
        if num is None:
            self.dialTime = self.time
            self.dialString = ""
        else:
            self.dialString = ''.join(
                "1111110000" * (digit or 10) + "000000"
                for digit in map(int, str(num))
                ) + "0" * 140

    def readDialer(self):
        """Read the U7 switch when the dialer is active
        If the dailer string is empty, U7 is returned normally.
        Returns True if the dialer opens the switch
        """
        # determine how much time has passed since last call
        # each time a multiple of 32 is reached, one digit is removed
        rotated = (self.time >> 5) - (self.dialTime >> 5)
        self.dialString = self.dialString[rotated:]
        return self.dialString[:1] == "1"

class Interpreter(State):
    """Run the zebra program and interact with user.
    """
    #TODO 6: make breakpoints
    #the character map
    def __init__(self, address=None, tape=''):
        if address is None: address = 22
        super().__init__(address, tape)

    def isNumericReg(self, reg):
        """Check if reg is a register described as Rnn
        #>>> Interpreter().isNumericReg('R15')
        True
        """
        return reg[:1]=='R' and reg[1:].isdigit() and \
            int(reg[1:]) in self.registers

    def interact(self, e):
        """Ask the user for a command:
        """
        #enable specific commands for the situation
        #TODO 6: rethink information passed on in condition
        #e.action: the condition of the instruction
        #e.expression: the formula that is evaluated
        #e.names: the parameters the expression depends on
        if isinstance(e, DependsOnCondition):
            if e.expression is None:
                print("Condition", e.action, "cannot be predicted")
            else:
                print("Condition", e.action, "cannot be predicted:", e.expression)
        elif isinstance(e, VariableInstruction):
            print("This instruction cannot be predicted.")
        elif isinstance(e, Depends):
            print(e.action+':', e.expression)
        try: reg, sep, value = input("Command:").partition('=')
        except EOFError: sys.exit(0)
        if reg=='tape':
            self.setTape(value)
        elif reg in self.registers:
            self.assignRegister(reg, value)
        elif self.isNumericReg(reg):
            self.assignRegister(int(reg[1:]), value)
        elif reg in self.registers.context:
            self.assignParameter(reg, value)
        elif reg.isdigit():
            self.assignMemory(int(reg), value)
        elif reg in ("doit","skip"):
            self.forceJump(reg, e.action, e.names)
        elif reg=='carry':
            self.setCarry(value)
        else:
            self.printHelp(reg, sep, value)

    def printHelp(self, reg='?', sep='', value=''):
        """Print help text.
        """
        if reg!='?': print('Command "{}{}{}" not understood. '.format(
            reg, sep, value), end='')
        print("Allowed commands:\n"
            "?:             print this help text\n"
            "tape=<char>:   assign next tape charater(s) to read."
                          " First character is R26-R30\n"
            "<reg>=expr:    assign value to register. OldExpressions involving"
                          " parameters\n"
            "               are allowed; you can enter instructions by prepending"
                          " with :\n"
            "<param>=value: set parameter.\n"
            "               use lo..hi for ranges or elem,elem,elem for sets\n"
            "doit/skip:     do or skip conditional instruction\n"
            "carry=value    set/clear/show carry.\n"
            "               0:clear 1:set ?:undefined <empty>:show value\n"
            "<reg/param>:   show information on possible values of reg/param\n"
            "^Z:            quit"
            )

    def setTape(self, value):
        """Set R26..R30 to match given tape character
        Use current variables if these are still available
        """
        self.tapeReader.setTape(value)
        self.tapeReader.show()

    def assignRegister(self, reg, value):
        """Assign value to register. OldExpressions in params are allowed
        Note: result must be expressible as OldExpression"""
        R = self.registers
        if not value:
            self.showRegister(reg)
            return
        result = ParseAndCompute(R).process(value)
        R[reg] = R[reg].makeLikeMe(result)
        # invent new parameters
        for p in R[reg].lincomb:
            if p not in R.context:
                R.context[p] = BoundedVariable(reg)
        if reg=='C': self.origin = 'user'
        else: self.showRegister(reg)

    def showRegister(self, reg):
        value = self.registers[reg]
        if isinstance(reg, int): reg = 'R'+str(reg)
        self.showValue(value, '->   '+reg, 33+(reg=='A'))

    def showMemory(self, addr, writeFlag=False):
        data = self.readMemoryOrEmpty(addr)
        if data>WORDSIZE-NEGATIVE: data -= WORDSIZE
        self.showValue(data,
            '{}   ({:03d})'.format(('->','<=')[writeFlag], addr) )

    def showValue(self, value, startStr, wordsize=33):
        #TODO 4: show value if instruction that is low value
        mask = ~(~0<<wordsize)
        if isinstance(value, int):
            offset = RegisterBank.reduce(value, wordsize)
            variablePart = ''
            isConstant = True
        else:
            offset,variablePart = value.splitConstant(True)
            offset = RegisterBank.reduce(offset, wordsize)
            isConstant = value.constant()
        result = [startStr]

        if not variablePart:
            #variablePart is empty, so there is a constant offset=value
            #write as instruction if big
            if offset==0:
                result.append('0')
            elif -NEGATIVE<=offset<DRUMSIZE:
                result.append('{:09x}'.format(offset&mask))
                result.append(offset)
            else:
                #rewrite result to let display do its job
                result = [display(
                    result[0]+' =',
                    *split(offset&mask),
                    explainCondition=False)]
        elif isConstant and int(value)==offset:
            #offset = value and constant, so variable part actually is zero
            #write as instruction if big
            if offset==0:
                result.append(variablePart.lstrip('+'))
                result.append(0)
            elif -NEGATIVE<=offset<DRUMSIZE:
                result.append('{:09x}+...'.format(offset&mask))
                result.append(str(offset)+variablePart)
                result.append(offset)
            else:
                #rewrite result to let display do its job
                result = [display(
                        result[0]+' = (',
                        *split(offset&mask),
                        explainCondition=False)+')'
                    +variablePart]
                result.append('{:09x}+...'.format(offset&mask))
        elif not -NEGATIVE<=offset<DRUMSIZE:
            #big offset, different from value: write offset as instruction
            result = [display(
                    result[0]+' = (',
                    *split(offset&mask),
                    explainCondition=False)+')'
                +variablePart]
            if isConstant:
                v = int(value)
                result.append('{:09x}'.format(v&mask))
                if -NEGATIVE<=v<DRUMSIZE: result.append(v)
            else:
                result.append(value.asVariable())
        elif not isConstant:
            #variable, offset is small: no instruction
            if offset:
                result.append('{:09x}+...'.format(offset&mask))
                result.append(str(offset)+variablePart)
            else:
                result.append(variablePart.lstrip('+'))
            result.append(value.asVariable())
        elif -NEGATIVE<=int(value)<DRUMSIZE:
            #both value and offset are small, value is constant: no instruction
            if offset:
                result.append('{:09x}+...'.format(offset&mask))
                result.append(str(offset)+variablePart)
            else:
                result.append(variablePart.lstrip('+'))
            v = int(value)
            result.append('{:09x}'.format(v&mask))
            result.append(v)
        else:
            #offset is small, value is big, we show instruction for value.value
            v = int(value)
            result = [display(
                result[0]+' =',
                *split(v&mask),
                explainCondition=False)]
            if offset:
                result.append('{:09x}+...'.format(offset&mask))
                result.append(str(offset)+variablePart)
            else:
                result.append(variablePart.lstrip('+'))

        if repr(result[-1]) == '(..)': del result[-1]
        result[0] = INDENT+' '+result[0]
        print(*result, sep=' = ')

    def assignParameter(self, param, value):
        """Assign value to parameter. Set and ranges are allowed"""
        if not value:
            self.showParameter(param)
            return
        context = self.registers.context
        if value.isdigit():
            context[param] = KnownVariable(int(value))
        elif value[:2] in ('0x', '0X', '0o', '0O', '0b', '0B'):
            context[param] = KnownVariable(int(value))
        elif ',' in value:
            context[param] = SetVariable(*
                map(int, value.split(',')))
        elif '..' in value:
            context[param] = BoundedVariable(*
                map(int, value.split('..')))
        self.showParameter(param)

    def showParameter(self, param):
        print(param, '=', self.registers.context[param])

    def assignMemory(self, addr, value):
        if not value:
            self.showMemory(addr)
            return
        drum[addr] = int(value)

    def forceJump(self, command, action, names):
        """If command is doit or skip, force jump condition"""
        #TODO 6: extend this trick for carry
        valueOptions = list(map(self.registers.context.get, names))
        combinations = defaultdict(list)
        for values in itertools.product(*valueOptions):
            mapping = dict(zip(names, map(KnownVariable, values)))
            if action=='A<0':
                condition = int(self.registers.modify(mapping)['A'])<0
            elif action=='B<0':
                condition = int(self.registers.modify(mapping)['B'])<0
            elif action=='A!=0':
                condition = int(self.registers.modify(mapping)['A'])!=0
            elif action=='B&1':
                condition = int(self.registers.modify(mapping)['A'])&1
            else:
                combinations = None
                break
            combinations[condition].append(values)
        if combinations is not None:
            v = combinations[command=='doit']
            #get possible values of the parameters for this condition
            combinedMapping = dict(zip(names, map(SetVariable, *v)))
            newRegs = self.registers.modify(combinedMapping)
            oldParams = self.registers.context
            newParams = newRegs.context
            for p in oldParams:
                if str(oldParams[p])!=str(newParams[p]):
                    print('    ', p, '= ', newParams[p], sep='')
            #change parameters
            self.registers.context = newParams
        #replace instruction by one that enforces the effect
        if command=='skip':
            #replace command by A instruction
            self.registers['C'] = fA000W<<18
        else:
            #clear all the condition bits (X000V7 instruction)
            self.registers['C'] &= ~(0x1e<<18)
        self.origin = command

    def setCarry(self, value):
        """set or clear carry"""
        if value=='0': self.registers.context['V'] = KnownVariable(False)
        elif value=='1': self.registers.context['V'] = KnownVariable(True)
        elif value=='?': self.registers.context['V'] = BoolVariable()
        elif not value: print("Carry:", self.registers['V'])
        else: self.printHelp()

    #for recognize
    def makeRecognizeDB(self):
        """Set up the database for recognize.
        It is a mapping from instruction to line number in the DB file
        """
        self.recognizeDB = {}
        for lineno,line in enumerate(open(ZTRACEDB, encoding='ascii'), start=1):
            line = line.strip()
            if not line or line.startswith('#'): continue
            instr = line[:line.index(':')]
            try: self.recognizeDB[assemble(instr)] = lineno
            except SyntaxError:
                raise SyntaxError(
                    "assembly error in line {} of regnames.db".format(lineno))

    def recognize(self):
        """Recognize value of C, and fill in symbolic name for known registers
        at that point.
        #>>> i = Interpreter(27)
        #>>> i.registers.context['tapeChar'] = BoundedVariable(0,31)
        #>>> i.makeRecognizeDB()
        #>>> i.step()
        Start: 00000001b X027                   goto 027
        #>>> i.recognize()
              Rewritten: B
        #>>> i.showRegister('B')
              ->   B = -tapeChar = (-31..0)
        """
        try: C = int(self.registers['C'])
        except Depends: return
        #TODO 3: rethink order of operations: try not to change variables when not needed
        if C not in self.recognizeDB: return

        #fetch and parse line from the file
        line = linecache.getline(ZTRACEDB, self.recognizeDB[C]).rstrip()
        #split off parameter bounds if available
        regAsns, semicolon, propAsns = \
            line[line.index(':')+1:].partition(';')
        newBoundedProps = {}
        if semicolon:
            #get the new variables with bounds
            for p in self.processParamBounds(propAsns):
                newBoundedProps[p.name] = p
        #do the rewritings of the variable names
        substitutions = {}
        self.processRewriteRegs(regAsns, substitutions, newBoundedProps)

    def processRewriteRegs(self, regAsns, substitutions, newBoundedProps):
        """Process the register substitutions in a line of regnames.db.
        Produce a mapping of the parameters to new names (when needed),
        and performs the register substitutions.
        Sets the variables that can be computed to the proper value,
        or sets the new bounds.
        Prints a line indicating what has been substituted.
        """
        R = self.registers
        #for output
        rewritten = set()
        #parser for expressions
        parse = ParseAndCompute(R).process
        for regAsn in regAsns.split(','):
            regS, equals, expr = regAsn.partition('=')
            regS = regS.strip()
            #get register index
            if not equals or not regS: raise SyntaxError
            if regS[0]=='R': reg = int(regS[1:])
            else: reg = regS
            #parse and substitute
            parsed = self.substituteParameters(
                parse(expr), reg, substitutions, newBoundedProps)
            if isinstance(R[reg], int) or R[reg].constant():
                #if we know the register value, keep it if we can
                #by fixing the (only) parameter
                parsed.solve(int(R[reg]))
            R[reg] = parsed
            rewritten.add(regS)
        if rewritten and self.verbose:
            if len(rewritten) == 1:
                print(INDENT, 'Rewritten: {} = {}'.format(
                    rewritten.pop(), expr))
            else:
                print(INDENT, 'Rewritten:', ', '.join(sorted(rewritten)))

    def substituteParameters(self, expr, reg, substitutions, newBoundedProps):
        """Replace all parameters used in expr by fresh names,
        and register the substitutions.
        Returns new expr.
        Makes sure all parameters in expr are not in use by other registers.
        """
        R = self.registers
        #get the names of parameters that are in use (i.e. needs substitution)
        #by other registers
        propsInUse = set().union(*(
            map(attrgetter('name'), v.lincomb)
            for r,v in R.reg.items()
            if r!=reg and isinstance(v, Expression)
            )) - {'1'}
        #substitute variables that are in use by other registers
        for prop in expr.lincomb:
            if prop in propsInUse:
                #prop is in use by other registers
                #find a new name for the variable that isn't used yet
                name = prop.name
                for i in itertools.count():
                    newName = name+str(i)
                    if newName not in propsInUse:
                        break
                if name in newBoundedProps:
                    newProp = newBoundedProps[name]
                    newProp.name = newName
                else:
                    newProp = BoundedVariable(newName)
                substitutions[name] = newProp
        #do the actual substitution for the expression (overwriting it)
        expr.values = {
            substitutions.get(p.name, p):m
            for p,m in expr.lincomb.items()}
        return expr

    def processParamBounds(self, propAsns):
        """Generate the variables with bounds from a line of regnames.db.
        """
        if not propAsns: return
        for propAsn in propAsns.split(','):
            name, equals, expr = propAsn.partition('=')
            #get the substituted name
            name = name.strip()
            #get the right variable
            lo, dotdot, hi = expr.partition('..')
            if dotdot:
                prop = BoundedVariable(
                    name,
                    int(lo or 0),
                    int(hi or WORDSIZE-1))
            else:
                values = list(map(int, expr.split(',')))
                if len(values)==1:
                    prop = KnownVariable(name, int(expr))
                else:
                    prop = SetVariable(name, *map(int, values))
            yield prop

    # main
    verboseOn = [assemble("X7967C4")]
    verboseOff = []
    def main(self, timeout=10000):
        """Main program:
        Run until something unpredictable happens, then ask user.
        Break everything if too many instructions are executed.
        """
        oldRvalues = dict.fromkeys("CD")
        self.makeRecognizeDB()
        for instructionCounter in range(timeout):
            #run until state cannot be predicted
            #step raises an exception if the outcome depends on an assumption
            R = self.registers
            try:
                if self.verbose:
                    oldRvalues = {r:str(R[r]) for r in R.keys()}
                    oldRvalues['V'] = list(R['V'].lincomb.keys())[0].reprValues()
                    if int(R['C']) in self.verboseOff:
                        self.verbose = R.verbose = False
                self.recognize()
                self.step()
                if all(str(R[r])==oldRvalues[r]
                        for r in 'CD'):
                    raise Depends('halt')
                if int(R['C']) in self.verboseOn and not self.verbose:
                    print ("(Some output suppressed)", '-'*40)
                    self.verbose = R.verbose = True
            except Depends as e:
                self.verbose = True
                self.interact(e)
            else:
                if self.verbose:
                    #instruction is executed; show modified regs
                    for r,oldValue in sorted(oldRvalues.items(),
                            key=lambda p:str(p[0])):
                        if r in ('C','D'): pass
                        #TODO 3: fix this when steps are done
                        elif r=='V':
                            carryStr = list(R['V'].lincomb.keys())[0].reprValues()
                            if carryStr==oldValue: continue
                            print(INDENT, '->   V = '+carryStr)
                        elif str(R[r])!=oldValue: self.showRegister(r)
        raise ValueError("Timeout exceeded")

drum = {}
