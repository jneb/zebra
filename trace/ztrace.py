#!python3
"""Program to analyze Zebra programs, to compensate for lack of documentation.
In order to automatically generate sensible annotations and conclusions
about values of registers, we need a moderate amount of intelligence
in the computation of register values.
To represent register values, we use an array like object that I call RegisterBank.
A context element is a linear combination of Parameters,
that are maintained by the context.
(Think of parameters as the initial values of the registers.)
Requirements:
- you can set a variable to a known value or parameter
- you can simply get the value of a Variable if it is known
- it must be easy to manipulate Variables with +, <<, etc
- it must be easy to test Variables for <0, &1 if the result is known
- otherwise, you get an error saying which parameters determine the value
- it must be easy to split a context for different values of a parameter, for conditional jumps
- support for handling of sign bits of variables would be great
- support for "shifted out bits" and "shifted in bits" should be intuitive
Examples:
>>> c = RegisterBank().zebraSetup()
>>> c['A']
iA:(..)
>>> (c['A']<<3)+c['B']
iA*8+iB:(..)
>>> c['A']+3<0    #doctest: +SKIP
Traceback ...
Depends: A
>>> c['R26']<0    #doctest: +SKIP
False

Implementation:
A context behaves like an array.
A context also contains a table of parameters and things that are known about
these.
The elements are OldExpressions, dict like objects describing the multiplicities.
The OldExpressions know how to calculate, and point back to their context to access
the parameters when needed. (For <0 tests, for example.)
The parameters are stored in a dict in the context; the information about the
parameter is stored as a variable.
"""

# plans:
# make a real dialer that sends pulses depending on drum
# string variable that contains 1or 0 per drum rotation
# figure out makelikeme
# make unit tester and move tests
# make separate project with main.py interface that can call unittest, and run the tracer on a given tape
# but leave zebra/ztrace where it is

import sys, os
os.chdir(os.path.dirname(__file__) or '.')
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from zebra import readFRD, ZLOT6, display, split, FunctionBits, Tape
from zebra import MASK33, MASK34, ADDRMASK, fA000W, SIGNBIT, assemble
from zebra import UnknownInstruction
from operator import or_, eq
from functools import reduce
from state import State, Interpreter, TapeReader, drum
import itertools, collections, sys, doctest

#for comparison of output with example output
import difflib, io, bz2
from expression import Depends, DependsOnCondition, Variable, KnownVariable, SetVariable, BoolVariable, BoundedVariable
from expression import WORDSIZE, LOW34, HIGH34, NEGATIVE, DRUMSIZE, HALF
from expression import Expression, signExtend, ParseAndCompute
#database of locations in LOT with known information
EXAMPLEFILE = 'outputExample.zp'


def setTestOutput():
    """Set the example test output file used by compareTestOutput"""
    #compress output on the file to example file
    #newline format is for easy editing of the decompressed file in Vim
    sys.stdout = bz2.open(EXAMPLEFILE, 'wt', newline='\n')
    try:
        i = Interpreter(22)
        i.setTape(''.join(i.tapeReader.readTapeFile("tapes/demo2.znc")))
        #assuming demo2, stop example output when compilation is ready and running starts
        try: i.main(7228)
        except ValueError as e:
            if e.args[0]!='Timeout exceeded': raise
        print("Program starting")
        try: i.main(100)
        except ValueError as e:
            if e.args[0]!='Timeout exceeded': raise
            print("Stopped after {} instructions".format(i.instructionCounter))
    finally:
        sys.stdout.close()
        #set stdout back
        sys.stdout = sys.__stdout__

def compareTestOutput():
    """Compare produced output with test file."""
    sys.stdout = io.StringIO()
    try:
        i = Interpreter(22)
        i.setTape(''.join(i.tapeReader.readTapeFile("tapes/demo2.znc")))
        i.main(10**4)
    finally:
        output = sys.stdout.getvalue().splitlines(1)
        sys.stdout = sys.__stdout__
        linesPrinted = 0
        for line in difflib.unified_diff(
                list(bz2.open(EXAMPLEFILE, mode='rt')),
                output,
                fromfile='example file',
                tofile='current run',
                n=10):
            sys.stdout.write(line)
            linesPrinted +=1
            if linesPrinted>100:
                print("Output truncated")
                break
        if not linesPrinted:
            print("Output is identical, ends with:")
            print(*output[-100:], sep='')

def showHelp():
    print("Usage: ztrace [-s|-t] [filename]")
    print("-s: set reference file")
    print("-t: check output against reference file")
    print("filename: run this file")
    print("No arguments: self test")
    print("Default file for reference test is demo2;")
    print("default argument if self test succeeds is start of demo2")
    sys.exit(1)

def main(argv=None):
    if argv is None:
        argv = sys.argv
    #read ZLOT
    for adr,f,r,d in readFRD(ZLOT6):
        drum[adr] = f<<18|r<<13|d
    if len(argv)==1:
        Interpreter(22,
                #standard header for 16 entry points
                '0000'
                '0AD100YT4'
                'AD34ZADPYNDK11QADP16YT9D00000'
                'AC9ZADPYNKE14NNKKCX3827RC58U7NKKBC'
                'X2331RBD16U1X45P6NK14-1T9D'
                '00000'
                'AC9ZADPYNKE14NNKKCX3827RC58U7NKKBC'
                'X2331RBD16U1X45P6NK14-1T9D'
                '00000'
                'AC9ZADPYNKE14NX45P4CNC10NKKBC'
                'X000LRICEU4X45P2NKKBCX000LRCEU5'
                'NC11X45P2NK14-1T9D'
                '00000AC9ZADPY'
                'NX34P1PP'
                'NC1NE10NC10NBC10'
                'NX40P1NRNBE11'
                'NX34P2PP'
                'NKKBC10-10'
                'NV2N001.0X000'
                'NKKB+11'
                'XP4BE10T0D'
                'AC9ZAD34PZX34Z'[:7]
                ).main(10**5)
    elif len(argv)==2:
        if argv[1].startswith('-'):
            #argument starts with -: do special stuff
            SILENT = True
            if argv[1]=='-t':
                compareTestOutput()
            elif argv[1]=='-s':
                setTestOutput()
            elif argv[1]=='-m':
                #measure compression
                compressed = open(EXAMPLEFILE, 'rb').read()
                decompressed = bz2.decompress(compressed)
                print("Compression factor: {:.3f}".format(len(decompressed)/len(compressed)))
                sys.exit(0)
            else:
                showHelp()
        else:
            #run from argument
            i = Interpreter(22)
            i.setTape(''.join(i.tapeReader.readTapeFile(argv[1])))
            i.main(10**5)
    else:
        showHelp()

if __name__=='__main__':
    main(sys.argv)