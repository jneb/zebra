#!python3
"""Write the effect of a zebra instruction
in a neat way.
"""

from itertools import chain
from zebra import split, FunctionBits

class Explainer:
    """Generate explain string
    """
    def __init__(self, adr, func, reg, mem):
        self.adr = adr
        self.func, self.reg, self.mem = func, reg, mem
        self.f = FunctionBits(self.func)
        self.LRBC = (self.func >> 8) & 12 | (self.func >> 7) & 3
        self.setParts()
        self.setRn()
        self.comment = ""

    def setParts(self):
        """Set:
        self.sign: + or -
        self.minus: "" or -
        self.memS: [mem] if Adw
        self.q: "" or 1
        self.qEffect: "" or B++ or B--
        self.v: "" or "V" or "1-V"
        self.vEffect: "" or "A+=V" or "A+=V-1"
        """
        f = self.f
        self.sign = "+-"[f.I]
        self.minus = "-" if f.I else ""
        self.memS = ""
        if f.A and not f.D and not f.W:
            if f.E and self.reg == 24:
                self.memS = "([{:03d}]&R5)".format(self.mem)
            else:
                self.memS = "[{:03d}]".format(self.mem)
        self.q = "1" if f.Q else ""
        self.qEffect = "B" + self.sign * 2 if f.Q else ""
        if self.f.Vn == 8:
            self.v = "1-V" if f.I else "V"
            self.vEffect = "A+=V-1" if f.I else "A+=V"
        else:
            self.v = self.vEffect = ""
        self.condition = self.makeCondition()

    # for each LRBC combination, in the order of the template functions,
    # indicate if you need a temp register if the reg number is used
    # 2,3 means temp needed for A resp. B, * means Tb is there already
    tempFlags = ["", "", "", "",
                 "23", "", "23", "3",
                 "23", "", "23", "",
                 "2*3", "", "2*3", "2"]

    def setRn(self):
        """Set:
        temp: the assignment to a temp variable if needed
        Rn: the "nice" name of the register or "" if K set
        regS: the "nice" name of the register, even if K set
        """
        flags = self.tempFlags[self.LRBC]
        reg = self.reg
        # figure out what temp registers to use
        self.temp = ""
        # make temp register for A or B if the register value is used again
        if str(reg) in flags:
            # accumulator in use also read as register
            if self.f.XD:
                # XD overwrites any register input, so no temp needed
                pass
            elif not self.f.B and self.f.LR:
                # LR overwrites A input, so reg input not used
                pass
            elif reg == 2: self.temp = "Ta=A"
            elif reg == 3 and "*" not in flags: self.temp = "Tb=B"
        if self.f.E and reg == 4 and self.f.A:
            # R4 is written by D, and also read
            self.temp = "Tr=R4"
        if self.f.E and reg == 15 and (self.f.LR or self.f.XD):
            # E15 writes R15, while LR/XD reads it
            if not self.f.B: self.temp = "Ta=A"
            elif self.f.C and self.f.LR:
                # case LRBCE15 is explicitly handled in template15
                pass
            elif "*" not in flags: self.temp = "Tb=B"
        # set name of register that is read
        if self.f.E: reg = 0
        if reg == 0: name = ""
        elif reg == 1: name = "1"
        elif reg == 2: name = "Ta" if "Ta" in self.temp else "A"
        elif reg == 3: name = "Tb" if "Tb" in self.temp or "*" in flags else "B"
        elif reg == 23: name = "1/2"
        elif reg == 24: name = "(A&B)"
        elif 26 <= reg < 31: name = "tapebit" + str(30 - reg)
        elif 4 <= reg < 16: name = "R" + str(reg)
        else: name = ""
        # Rn is what you take into the calculations
        # regS is the name of the register that is read
        self.regS = name
        self.Rn = "R15" if self.f.XD else "" if self.f.K else name

    def makeAdd(self, acc, *values):
        """Build inplace addition of the form A-=B+V
        produces nothing if no values are present
        """
        lhs = acc + self.sign + "="
        rhs = "+".join(filter(None, values))
        return rhs and lhs + rhs
        
    def makeAssign(self, acc, *values):
        """Build assignment of the form B=-A-(123)-1
        produces nothing if no values are present
        """
        lhs = acc + "=" + self.minus
        rhs = self.sign.join(filter(None, values))
        # note that A=A actually does something (clearing A33)
        result = lhs + (rhs or "0")
        if result == "B=B": return ""
        return result.replace("-1-V", "-1+V")

    def makeShift(self, reg, *values):
        """Build shift and add expression of the form A=A/2+B
        """
        rhs = self.sign.join(filter(None, values))
        # fix -(1-V) = -1+V
        rhs = rhs.replace("-1-V", "-1+V")
        if rhs:
            result = (reg + "=" +
                      reg + ("/" if self.f.R else "*") + "2" +
                      self.sign + rhs)
            # fix -(1-V) = -1+V
            return result.replace("-1-V", "-1+V")
        else:
            return reg + ("/" if self.f.R else "*") + "=2"

    def makeCondition(self):
        if self.f.Vn & 7:
            condition = [
                None,
                "U1",
                "U2",
                "U3",
                "U4",
                "U5",
                "U6",
                "not start",
                None,
                "A<0",
                "B<0",
                "A!=0",
                "B&1",
                "?",
                "?",
                "?"][self.f.Vn]
            return "if {}:".format(condition)
        else: return ""

    def doNext(self):
        if self.reg == 31 and not self.f.E:
            yield "advance tape"
        if self.f.K and not self.f.E: offset = self.regS
        else: offset = ""
        if self.f.W:
            if self.mem & 1 or self.mem <= 8192 - 2 * 100:
                self.comment = " (fast)"
            else:
                self.comment = " (rept {})".format(8192 - self.mem >> 1)
        if self.f.A:
            yield "next=D" + (offset and "+" + offset)
            if not (self.f.E and self.reg == 4): yield "D=R4"
            else: yield "D=Tr"
        elif self.comment or self.f.D:
            yield "next=" + (offset or "X000")
        elif offset:
            yield "next=[{:03d}]+".format(self.mem) + offset
        elif self.mem - 1 != self.adr:
            # note that self.adr may not be an integer
            yield "goto {:03d}".format(self.mem)

    def doWrite(self):
        reg = self.reg
        acc = "AB"[self.f.B]
        if acc in self.temp or "*" in self.tempFlags[self.LRBC]:
            acc = "T" + acc.lower()
        if self.f.E and 4 <= reg < 16:
            if self.f.K:
                yield "R" + str(reg) + "=D"
            else:
                yield "R" + str(reg) + "=" + acc
        if self.f.D and not self.f.W:
            yield "[{:03d}]={}".format(self.mem, acc)
            
    def template0(self):
        "Case lrbc: A+-=(mem)+Rn+V;B+-"
        yield self.makeAdd("A", self.memS, self.Rn, self.v)
        yield self.qEffect

    def template1(self):
        "Case lrbC: A=+-(mem)+-Rn+-V;B+-"
        yield self.makeAssign("A", self.memS, self.Rn, self.v)
        yield self.qEffect

    def template2(self):
        "Case lrBc: B+-=(mem)+Rn+1;A+=V"
        makeAdd = self.makeAdd("B", self.memS, self.Rn, self.q)
        if not makeAdd and self.f.I:
            yield "B-=0 (set V)"
        yield makeAdd
        yield self.vEffect

    def template3(self):
        "Case lrBC: B=+-(mem)+-Rn+-1;A+=V"
        yield self.makeAssign("B", self.memS, self.Rn, self.q)
        yield self.vEffect

    def template4(self):
        "Case lRbc: AB>>;A+-=(mem)+Rn+V;B+-"
        yield "AB>>"
        yield from self.template0()

    def template5(self):
        "Case lRbC: A+-=(mem)+Rn+V;B=B/2+1"
        yield self.makeAssign("A", self.memS, self.Rn, self.v)
        yield self.makeShift("B", self.q)

    def template6(self):
        "Case lRBc: AB>>;B+-=(mem)+Rn+1;A+=V"
        yield "AB>>"
        yield from self.template2()

    def template7(self):
        "Case lRBC: B=+-(mem)+-Rn+-1;A=A/2+-V"
        yield self.makeAssign("B", self.memS, self.Rn, self.q)
        yield self.makeShift("A", self.v)

    def template8(self):
        "Case Lrbc: AB<<;A+-=(mem)+Rn+V;B+-"
        yield "AB<<"
        yield from self.template0()

    def template9(self):
        "Case LrbC: A+-=(mem)+Rn+V;B=B*2+1"
        yield self.makeAssign("A", self.memS, self.Rn, self.v)
        yield self.makeShift("B", self.q)

    def template10(self):
        "Case LrBc:AB<<;A+-=V;B+-=(mem)+Rn+1;A+=V"
        yield "AB<<"
        yield self.vEffect
        yield self.makeAdd("B", self.memS, self.Rn, self.q)

    def template11(self):
        "Case LrBC: B=+-(mem)+-Rn+-1;A=A*2+-V"
        yield self.makeAssign("B", self.memS, self.Rn, self.q)
        yield self.makeShift("A", self.v)

    def template12(self):
        "Case LRbc: Tb=B;AB>>;A+-=(mem)+R15*(Tb&1)+V;B+-=1"
        yield "Tb=B"
        yield "AB>>"
        yield self.makeAdd("A", self.memS, "R15*(Tb&1)", self.v)
        yield self.qEffect

    def template13(self):
        "Case LRbC: A=+-R15*(B&1)+-V;B=B/2+-1"
        yield self.makeAssign("A", self.memS, "R15*(B&1)", self.v)
        yield self.makeShift("B", self.q)

    def template14(self):
        "Case LRBc: Tb=B;AB>>;A+-=R15*(Tb&1)+V;B+-=(mem)+Rn+1"
        yield "Tb=B"
        yield "AB>>"
        yield self.makeAdd("A", "R15*(Tb&1)", self.v)
        yield self.makeAdd("B", self.memS, self.Rn, self.q)

    def template15(self):
        "Case LRBC: A=A/2+-R15*(B&1)+-V;B=+-(mem)+-Rn+-1"
        yield self.makeShift("A", "R15*(B&1)", self.v)
        # special exception for LRBCE15 to save a temp register
        if self.f.E and self.reg == 15:
            yield "R15=B"
            # suppress write that would come after this
            self.reg = 0
        yield self.makeAssign("B", self.memS, self.Rn, self.q)
        
    def __call__(self):
        """Build the explanation from the parts.
        """
        if self.f.K and (self.reg == 3 and (self.f.B or self.f.Q) or
                         self.reg == 2 and (not self.f.B or self.f.LR)):
            # write to A while next instruction depends on it: do next earlier
            statements = ";".join(filter(None, chain(
                self.doNext(),
                (self.temp,),
                self.doWrite(),
                getattr(self, "template" + str(self.LRBC))(),
                )))
        elif self.f.E and self.reg == 15 and (self.f.LR or self.f.XD):
            # write to R15 while it is in use: postpone write
            statements = ";".join(filter(None, chain(
                (self.temp,),
                getattr(self, "template" + str(self.LRBC))(),
                self.doWrite(),
                self.doNext(),
                )))
        else:
            # standard order
            statements = ";".join(filter(None, chain(
            (self.temp,),
            self.doWrite(),
            getattr(self, "template" + str(self.LRBC))(),
            self.doNext(),
            )))
        return self.condition + (statements or "No operation") + self.comment

if __name__ == "__main__":
    from zebra import assemble
    for s in (
            "X2",
            "X123B5",
            "ABC23",
            "XR2",
            ):
        e = Explainer(assemble(s, 100))
        print(s, e())
        print
