#!python3
"""Temp storage for functions in ztrace that appear to be useless.
"""

def myProduct(*args, repeat=1):
    """Like itertools.product, but produces the elements
    in an order that uses up the iterators as slowly as possible.
    >>> for x in itertools.islice(myProduct(range(2), repeat=10), 5): print(*x)
    0 0 0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0 0 1
    0 0 0 0 0 0 0 0 1 0
    0 0 0 0 0 0 0 0 1 1
    0 0 0 0 0 0 0 1 0 0
    """
    #get all the iterators
    iterators = list(map(iter, args*repeat))
    #get the first elements, and return the first result
    try: firstElements = tuple(map(next, iterators))
    except StopIteration:
        #result is empty
        return
    #invariant in main loop: all tuples corresponding to firstElements are done
    yield firstElements
    #now build up the cartesian product of the last iterators
    #cartesian product of iterators that are finished
    finished = [()]
    #main loop: scan last iterator that's left
    while iterators:
        current = iterators.pop()
        #prepare for filling the new cartesian product:
        #start with what we already output
        nextElementTuple = firstElements[-1],
        firstElements = firstElements[:-1]
        nextFinished = [nextElementTuple+x for x in finished]
        #output for the rest of this iterator
        for x in current:
            nextElementTuple = x,
            for p in finished:
                yield firstElements+nextElementTuple+p
                nextFinished.append(nextElementTuple+p)
        #new cartesian product is computed, and corresponing tuples output
        finished = nextFinished
        #ready for next iterator

def docTestLineNumberFix():
    """Modify doctest behaviour to figure out line numbers of strings in __test__
    """
    class MyDocTestFinder(doctest.DocTestFinder):
        def _find_lineno(self, obj, source_lines):
            if isinstance(obj, str):
                #find a unique line in the string, so we know where it is
                for offset,line in enumerate(obj.splitlines(True)):
                    if source_lines.count(line)==1:
                        return source_lines.index(line)-offset
            else: return super()._find_lineno(obj, source_lines)
    doctest.DocTestFinder = MyDocTestFinder

