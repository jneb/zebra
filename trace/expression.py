#python3
"""********************************************************
An expression is a way to describe the dependencies of a value on
unknown other values.
An Expression describes what is in a register in a descriptive way.
An Expression depends on one or more Variables.
Variables are names for things that are understandable for the user,
for example input values of routines or values of tape holes.
The trace can then follow these throughout a computation,
and rename where needed (like combining tape holes to a tape char).
The variables have bounds that are uncoditional (as a result of register sizes)
but also "soft" bounds that depends on what is known from the tracing context.
Of course, variables can be completely know (they're constants, then).
"""

#TODO: more explanations

import re
import operator

from zebra import MASK33, assemble
DRUMSIZE = 1<<13
#top of range of register values
WORDSIZE = 1<<33
WORDSIZE34 = 1<<34
#value of register 23, or instruction A000, or most negative number
HALF = WORDSIZE>>1
#values that can be shown as between -NEGATIVE and 0 are made negative
NEGATIVE = 1000
#range of values allowed for registers: LOW33<=value<HIGH33
LOW33, HIGH33 = -HALF, WORDSIZE-NEGATIVE
#and A is a bit longer
LOW34, HIGH34 = LOW33<<1, (WORDSIZE<<1)-NEGATIVE



def signExtend(x, bits=33):
    """Sign extend the number of a given number of bits.
    If the highest bit is 1, make the number negative.
    The number may be signed before this routine is called.
    >>> signExtend(20, 5)
    -12
    """
    #TODO 1: what to do with general numbers (i.e. iR4)? Solution: make a new object that shows what is done. For the moment, assume it is signed already
    word = 1<<bits
    half = word>>1
    if not -half <= x <word:
        raise ValueError("signExtend: number out of range")
    try:
        if x > half: x -= word
        return x
    except Depends:
        # if we don't know if high bit is 1, make an unknown variable
        return x - (Expression(BoolVariable('?', 0, 1)) << 32)


#for values used in the Zebra, if you don't want to specify limits
#TODO 4: first class for specific values from known variables, second for variable?
#TODO 5: simplify testing bits by making a MaskedOldExpression that does clever compares
"""Class for representing values with a limited number of possibilities.
To represent register values, we use an array like object that I call RegisterBank.
A context element is a linear combination of Parameters,
that are maintained by the context.
(Think of parameters as the initial values of the registers.)
Requirements:
- you can set a variable to a known value or parameter
- you can simply get the value of a Variable if it is known
- it must be easy to manipulate Variables with +, <<, etc
- it must be easy to test Variables for <0, &1 if the result is known
- otherwise, you get an error saying which parameters determine the value
- it must be easy to split a context for different values of a parameter, for conditional jumps
- support for handling of sign bits of variables would be great
- support for "shifted out bits" and "shifted in bits" should be intuitive
Examples:
>>> c = RegisterBank().zebraSetup()
>>> c['A']
iA:(..)
>>> (c['A']<<3)+c['B']
iA*8+iB:(..)
>>> c['A']+3<0    #doctest: +SKIP
Traceback ...
Depends: A
>>> c['R26']<0    #doctest: +SKIP
False

Implementation:
A context behaves like an array.
A context also contains a table of parameters and things that are known about
these.
The elements are OldExpressions, dict like objects describing the multiplicities.
The OldExpressions know how to calculate, and point back to their context to access
the parameters when needed. (For <0 tests, for example.)
The parameters are stored in a dict in the context; the information about the
parameter is stored as a variable.
"""

class Depends(Exception):
    """Raised if a value is requested that is not constant.
    Create with descriptive text and optional names dict.
    """
    def __init__(self, action, expression=None, names=()):
        super().__init__(action, expression)
        self.action = action
        self.expression = expression
        self.names = names

class DependsOnCondition(Depends):
    """Caused by conditional instruction"""

class VariableInstruction(Depends):
    """Caused by instruction that itself depends on an unknown value"""

class Variable:
    """Base class of variables that describe a parameter.
    What you can say about a number.
    Variables that match a pattern will be identical if their name is the same
    (this allows to add bits from the tape)
    """
    import weakref
    #dictionary of variables that are guaranteed to be identical
    reusedVariables = weakref.WeakValueDictionary()
    def __new__(cls, name, *values, reuse=False):
        """if reuse is True, use variable that is recognized by name.
        """
        reusedDict = Variable.reusedVariables
        if reuse and name in reusedDict:
            return reusedDict[name]
        result = super(Variable, cls).__new__(cls)
        #save variable for reuse
        if name: reusedDict[name] = result
        return result

    def __init__(self, name, *args):
        self.name = name

    def constant(self):
        """If this returns True, value is the actual value of the object"""
        raise NotImplementedError

    def __int__(self):
        """Give value if constant, or or raise Depends"""
        lo, hi = self.limits()
        if lo == hi: return lo
        raise Depends('Nonconstant parameter', repr(self))

    def __str__(self):
        """Give name"""
        return self.name

    def reprValues(self):
        """Overwrite for subvariables to get __repr__ working."""
        return ""

    def __repr__(self):
        resultList = [self.name, "(", self.reprValues(), ")"]
        if self.name: resultList[1] = ":("
        return ''.join(resultList)

    def limits(self, multiplier=1):
        """Lowest and highest possible values
        multiplier multiplies these by a given value,
        which is sometimes useful
        """
        raise NotImplementedError

    def __iter__(self):
        """Iterate over all possible values, if not too many.
        """
        raise NotImplementedError

    def __hash__(self):
        """Allow to use this as key for dicts"""
        return hash(self.name)

class KnownVariable(Variable):
    """Constant (a numeric value)
    >>> KnownVariable(5)
    (5)
    """
    def __init__(self, name, value, reuse=None):
        #reuse parameter is used by __new__
        super().__init__(name)
        self._value = value

    def constant(self): return True
    def __int__(self):
        return self._value

    def limits(self, multiplier=1):
        return (self._value * multiplier,) * 2
    def reprValues(self): return str(self._value)
    def __iter__(self): return iter([self._value])

ONE = KnownVariable('1', 1)

class SetVariable(Variable):
    """Variable with a given set of values.
    >>> c = SetVariable(-1,0)
    >>> c
    (-1,0)
    >>> c.limits(-1)
    (0, 1)
    """
    def __init__(self, name, *choices):
        super().__init__(name)
        self.choices = set(choices)

    def constant(self): return len(self.choices)==1
    def limits(self, value=1):
        a,b = min(self.choices), max(self.choices)
        a,b = a*value, b*value
        if a<=b: return a,b
        return b,a

    def reprValues(self):
        return ','.join(map(str, sorted(self.choices)))

    def __iter__(self): return iter(self.choices)

class BoolVariable(SetVariable):
    """Boolean variable
    >>> c = BoolVariable()
    >>> c
    (?)
    """
    def __init__(self, name, *possibilities, reuse=None):
        #reuse parameter is used by Variable.__new__
        if not possibilities: possibilities = {False,True}
        super().__init__(name, *possibilities)

    def reprValues(self):
        lo, hi = min(self.choices), max(self.choices)
        if lo==hi: return str(lo)
        return '?'

class BoundedVariable(Variable):
    """Value lies between lo and hi, INCLUSIVE!
    >>> b = BoundedVariable(1,10)
    >>> b
    (1..10)
    """
    #The default values allow for Zebra registers, both signed and unsigned
    #TODO 2: is this lo, lowerbound stuff still needed?
    defaultLowerbound = LOW33
    defaultUpperbound = WORDSIZE-1
    def __init__(self, name, lo=None, hi=None,
            lowerbound=None,upperbound=None):
        super().__init__(name)
        if lowerbound is None: lowerbound = self.defaultLowerbound
        if upperbound is None: upperbound = self.defaultUpperbound
        self.lo = lowerbound if lo is None else lo
        self.hi = upperbound if hi is None else hi
        self.lowerbound = lowerbound
        self.upperbound = upperbound
        assert self.lo<=self.hi

    def constant(self): return self.hi==self.lo
    def limits(self, value=1):
        a,b = self.lo*value, self.hi*value
        if value>=0: return a,b
        else: return b,a

    def reprValues(self):
        lo = "" if 0!=self.lo<=self.lowerbound else str(self.lo)
        hi = "" if self.hi>=self.upperbound else str(self.hi)
        return lo+'..'+hi

    def __iter__(self):
        return iter(range(self.lo, self.hi+1))


class Expression:
    """Expression class. Expressions can be in registers or memory.
    An expression is determined by zero or more variables;
    at least linear combinations are supported.
    The value is determined by the context, which is the values of the variables.
    Use int() if you want the numerical value,
    this will raise Depends if not fixed.
    Allowed operators:
    Behaves as int for +,-,neg,>>,<<,& and maybe ~,*(small int)
    Behaves variable for ==,!=,>,<,>=,<= if result can be determined,
    raises Depends otherwise.
    (> and family treat the value as signed!)
    Normally, values can be seen both as signed and unsigned,
    so possible values are between -1<<32 and 1<<33
    >> and & give a special kind of expression as result so
    that testing for bit 32 and 0 works.
    #>>> Expression(Variable('x'))
    x
    #>>> Expression(5)
    5
    """
    #TODO 3: make special expression subclass for >> and &
    #TODO 1: give Expression a word size so it knows what to do for << and sign extend
    def __init__(self, arg):
        if isinstance(arg, int):
            #constant
            self.lincomb = {ONE: arg}
        elif isinstance(arg, Variable):
            #parameter
            self.lincomb = {arg: 1}
        elif isinstance(arg, Expression):
            #copy constructor
            self.lincomb = arg.lincomb.copy()
        else: raise TypeError("Expression needs an int or Variable")

    def __repr__(self):
        """Make a nice representation of self,
        but don't apply shiftAndMask yet,
        to simplify overriding of __repr__.
        """
        result = shiftForm = ''
        values = self.lincomb
        for prop in sorted(values, key=str):
            count = values[prop]
            #shorthands for constants and trivial multipliers
            if prop==ONE:
                if count: result += '{:+d}'.format(count)
            elif count==1: result += '+'+str(prop)
            elif count==-1: result += '-'+str(prop)
            elif count==0: pass
            else:
                #use the shortest of count*name and (name*i<<j)
                productForm = '{}{!s}*{:d}'.format(
                    '+-'[count<0],
                    prop.name,
                    abs(count))
                j = (count&-count).bit_length()-1
                i = count>>j
                shiftForm = '{}({!s}{}<<{})'.format(
                    '+-'[i<0],
                    prop.name,
                    '' if abs(i)==1 else '*'+str(abs(i)),
                    j)
                #prefer product form if it is more than 1 place shorter
                if len(productForm)<len(shiftForm)-1: result += productForm
                else: result += shiftForm
        if result==shiftForm!='':
            #only one term, with parentheses: remove the parentheses
            result = result[0]+result[2:-1]
        #add zero if 
        if not result: result = '0'
        if result[0]=='+': result = result[1:]
        return result

    def asVariable(self):
        """Compute an assumption about the value of self.
        in order of preference:
        const (if all parameters are constant)
        choice (if all parameters are constant or choice)
        bounds
        >>> a = Expression('A', 33)
        >>> a.context['A'] = BoundedVariable(1, 29)
        >>> a.SRA(2)
        >>> a.asVariable()
        (0..7)
        """
        #for choice
        choices = 0,
        #for bounds
        lo = hi = 0
        for prop,count in self.lincomb.items():
            #maintain bounds
            loBound, hiBound = prop.limits(count)
            lo += loBound; hi += hiBound
            #maintain choices
            if choices is None: continue
            try:
                term = int(prop)*count
                choices = [c+term for c in choices]
            except Depends:
                if isinstance(prop, SetVariable):
                    choices = set(c+v*count
                        for c in choices
                        for v in prop.choices)
                else: choices = None

        if choices is None:
            if lo==hi: return KnownVariable('', lo)
            else: return BoundedVariable('', lo, hi)
        #remove doubles
        choices = set(choices)
        minC, maxC = min(choices), max(choices)
        if minC==maxC: return KnownVariable('', minC)
        elif maxC-minC==len(choices)-1: return BoundedVariable('', minC, maxC)
        else: return SetVariable('', *choices)

    def __lt__(self, other):
        selflo, selfhi = self.limits()
        if isinstance(other, int): otherlo = otherhi = other
        else: otherlo, otherhi = other.limits()
        if selfhi<otherlo: return True
        if selflo>=otherhi: return False
        #TODO 4: more explanation please
        raise Depends('Unpredictable comparison', '{}<{}'.format(self, other))

    def __gt__(self, other):
        selflo, selfhi = self.limits()
        if isinstance(other, int): otherlo = otherhi = other
        else: otherlo, otherhi = other.limits()
        if selflo>otherhi: return True
        if selfhi<=otherlo: return False
        #TODO 4: more explanation please
        raise Depends('Unpredictable comparison', '{}>{}'.format(self, other))

    def __le__(self, other):
        selflo, selfhi = self.limits()
        if isinstance(other, int): otherlo = otherhi = other
        else: otherlo, otherhi = other.limits()
        if selfhi<=otherlo: return True
        if selflo>otherhi: return False
        #TODO 4: more explanation please
        raise Depends('Unpredictable comparison', '{}<={}'.format(self, other))

    def __ge__(self, other):
        selflo, selfhi = self.limits()
        if isinstance(other, int): otherlo = otherhi = other
        else: otherlo, otherhi = other.limits()
        if selflo>=otherhi: return True
        if selfhi<otherlo: return False
        #TODO 4: more explanation please
        raise Depends('Unpredictable comparison', '{}>={}'.format(self, other))

    def limits(self):
        """Compute upper and lower bound for values of this expression.
        """
        #TODO 3: fix for multiple variable variables
        lo = hi = 0
        for prop,count in self.lincomb.items():
            Dlo, Dhi = prop.limits(count)
            lo += Dlo
            hi += Dhi
        return lo, hi

    def __int__(self):
        """Compute value"""
        return sum(int(p)*m for p,m in self.lincomb.items())

    def __add__(self, other):
        result = Expression(self)
        if isinstance(other, int):
            result.lincomb[ONE] = self.lincomb.get(ONE, 0)+other
        elif isinstance(other, Expression):
            for p,m in other.lincomb.items():
                #TODO 1: BUG: equals but not identical p can be in result.lincomb
                assert p in result.lincomb or str(p) not in map(str, self.lincomb)
                result.lincomb[p] = result.lincomb.get(p, 0)+m
        return result

    def __sub__(self, other):
        result = Expression(self)
        if isinstance(other, int):
            result.lincomb[ONE] = self.lincomb.get(ONE, 0)-other
        elif isinstance(other, Expression):
            for p,m in other.lincomb.items():
                result.lincomb[p] = result.lincomb.get(p, 0)-m
        return result

    def __radd__(self, other): return self+other

    def __rsub__(self, other): return -self+other

    def constant(self):
        #TODO 5: also return True if value of variable doesn't matter
        return all(p.constant() for p in self.lincomb)

    def __neg__(self):
        result = Expression(self)
        result.lincomb = {p:-m for p,m in self.lincomb.items()}
        return result

    def splitConstant(self, withSign=False):
        """Split expression in constant part ()
        and variable part (expressed as string)
        cleans up the lincomb to make sure output is neat
        """
        self.cleanup()
        if ONE not in self.lincomb:
            #there is no constant part
            offset, variablePart = 0, self
        else:
            offset = self.lincomb[ONE]
            variablePart = Expression(self)
            del variablePart.lincomb[ONE]

        if not variablePart.lincomb:
            variablePart = ''
        else:
            variablePart = repr(variablePart)
            if withSign and not variablePart.startswith('-'):
                variablePart = '+'+variablePart
        return offset, variablePart

    def cleanup(self):
        """Remove zeros in self.lincomb.values()
        """
        #copy the output of items so we can change the dict
        for p,m in list(self.lincomb.items()):
            if m==0: del self.lincomb[p]

    def __rshift__(self, n):
        """Shift over given number of bits."""
        mask = (1 << n) - 1
        # construct new lincomb, abort if not possible
        lincomb = {}
        for k, v in self.lincomb.items():
            lo, hi = k.limits()
            if (v >> n) << n == v:
                lincomb[k] = v >> n
            elif lo * v >> n == hi * v >> n == 0:
                continue
            else:
                return int(self) >> n
        result = Expression(self)
        result.lincomb = lincomb
        return result

    def __lshift__(self, n):
        """Shift over given number of bits."""
        assert isinstance(n, int)
        result = Expression(self)
        #TODO 1: this is where the intelligence should manifest itself: ignore 
        result.lincomb = {p:signExtend(m<<n&MASK33)
            for p,m in result.lincomb.items()}
        try:
            #TODO 4: fix shifting of 34 bits values (i.e. A)
            # try to fix outcome into an expression
            # this doesn't work because of the way the solve works:
            # introduction of new name is not wanted,
            # but if you change current name you get nonsensical output
            # for (1+digits)+((1+digits)<<1)
            # so we need to think about what to do with single property
            if result>=WORDSIZE:
                if True or not result.solve(int(result & MASK33)):
                    result &= MASK33
            elif result<-HALF:
                if True or not result.solve(int(result | ~MASK33)):
                    result |= ~MASK33
        except Depends: pass
        return result

    def __and__(self, other):
        #TODO: return variable value using special class
        if not isinstance(other, int):
            other = int(other)
        #keep formula if possible
        if int(self)==int(self)&other: return self
        return int(self)&other

    def __rand__(self, other):
        return self&other
    def __or__(self, other):
        #TODO: return variable value using special class
        assert isinstance(other, int)
        #keep formula if possible
        if int(self)==int(self)|other: return self
        return int(self)|other

    def solve(self, outcome):
        """For an expression with only one variable,
        modify the variable so that the expression gets the given value.
        If this variable occurs elsewhere, these will not be substituted;
        it is supposed to be called with a fresh variable.
        Returns flag indicating success.
        >>> lc = Expression(Variable('a'))-3
        >>> lc.solve(2)
        >>> sorted(lc.lincomb, key=attrgetter('name'))
        [1:(1), a:(5)]
        """
        offset = 0
        prop = None
        for p,m in self.lincomb.items():
            if p is ONE:
                offset = m
            elif prop is None:
                prop, factor = p,m
            else:
                #more than one parameter
                return False
        if prop is None:
            #zero parameters
            return False
        propValue = (outcome-offset)//factor
        #check if this approximation is good enough, and check mask
        actualOutcome = propValue*factor+offset
        if actualOutcome == outcome:
            #replace variable by a new one
            del self.lincomb[prop]
            newProp = KnownVariable(prop.name, propValue)
            self.lincomb[newProp] = factor
            return True
        else:
            return False

    def makeLikeMe(self, outcome):
        varParam = [v
                    for v in self.lincomb
                    if not v.constant()]
        if len(varParam) != 1:
            raise Depends("makeLikeMe")
        if not isinstance(outcome, Expression):
            outcome = Expression(outcome)
        adjust = self
        del adjust.lincomb[varParam[0]]
        return outcome - adjust

# Precedence parser for expressions based on the Pratt method
# Inspired by https://eli.thegreenplace.net/2010/01/02/top-down-operator-precedence-parsing

class Token(object):
    """Tokens used for parsing the expressions
    The routines nud and led
    """
    id = None # node/token type name
    value = None # used by literals
    bp = None # binding power

    def nud(token):
        """null denotation:
        prefix token handler
        returns value of expression after passing rest
        """
        raise SyntaxError("Syntax error (%r)." % self.id)

    def led(token, left):
        """left denotation:
        infix token handler
        returns value of expression given left, after passing rest
        """        
        raise SyntaxError("Unknown operator (%r)." % self.id)

    def __repr__(self):
        return self.id+': '+str(self.value)

class SymbolTable(dict):
    """Symbol table for the parser.
    Behaves as a dict from name to derived class of Token
    lbp stands for left binding power
    the parser is the provider of the values of the subexpressions
    """
    def __init__(self, parser):
        self.parser = parser

    def addSymbol(self, id, bp=0):
        """Add token to the symbol table with given binding power
        Returns a new Token subclass with that binding power.
        """
        if id not in self:
            class T(Token): pass
            T.name = 'symbol-'+id
            T.__qualname__ = id
            T.id = id
            T.lbp = bp
            self[id] = T
        else:
            T = self[id]
            T.lbp = max(bp, T.lbp)
        return T

    def addInfix(self, id, bp, oper):
        """Add infix operator than binds to the left
        open computes result of expression
        """
        def led(token, left):
            return oper(left, self.parser.expression(bp))
        self.addSymbol(id, bp).led = led

    def addRightInfix(self, id, bp, oper):
        """Add infix operator than binds to the right
        open computes result of expression
        """
        def led(token, left):
            return oper(left, self.parser.expression(bp-1))
        self.addSymbol(id, bp).led = led

    def addPrefix(self, id, bp, oper):
        """Add prefix operator
        """
        def nud(token):
            return oper(self.parser.expression(bp))
        self.addSymbol(id, bp).nud = nud

    def addLiteral(self, id):
        """Add literal with given value
        """
        def nud(token): return token.value
        self.addSymbol(id).nud = nud

    def valued(self, name, value):
        """Make token with given name and value
        """
        result = self[name]()
        result.value = value
        return result

class ParseAndCompute:
    """
    >>> R = Interpreter().registers
    >>> ParseAndCompute(R).process('-bit2+5<<1<<2')
    80-bit2*16
    """
    def __init__(self, registers):
        self.registers = registers
        #grammar definition
        symbolTable = self.symbolTable = SymbolTable(self)
        symbolTable.addLiteral('(value)')
        symbolTable.addLiteral('(param)')
        symbolTable.addLiteral('(reg)')
        symbolTable.addSymbol('(end)')
        symbolTable.addInfix('+', 20, operator.add)
        symbolTable.addInfix('-', 20, operator.sub)
        symbolTable.addRightInfix('<<', 10, operator.lshift)
        symbolTable.addRightInfix('>>', 10, operator.rshift)
        symbolTable.addPrefix('+', 100, operator.pos)
        symbolTable.addPrefix('-', 100, operator.neg)
        symbolTable.addPrefix('~', 100, operator.invert)
        symbolTable.addInfix('*', 30, operator.mul)

        #parentheses
        def nud(token):
            expr = self.expression()
            self.token = next(self.tokenStream)
            return expr
        symbolTable.addSymbol("(").nud = nud
        symbolTable.addSymbol(')')

    tokenPat = re.compile('([-+*~()]|<<|>>)')
    def tokenize(self, value):
        """Split expression for register in:
        (value): integer constants
        (reg): register values
        (param): context as Expression
        """
        result = self.tokenPat.split(value)
        for token in result:
            token = token.strip()
            if not token: continue
            elif token.isdigit():
                yield self.symbolTable.valued('(value)', int(token))
            elif token[:2] in ('0x', '0X', '0o', '0O', '0b', '0B'):
                yield self.symbolTable.valued('(value)', int(token))
            elif token[0]==':':
                yield self.symbolTable.valued('(value)', assemble(token[1:]))
            elif token in self.registers:
                yield self.symbolTable.valued('(reg)', token)
            elif token.startswith('R') and token[1:].isdigit():
                yield self.symbolTable.valued('(reg)', int(token[1:]))
            elif token in self.symbolTable:
                yield self.symbolTable[token]()
            else:
                token = BoundedVariable(token)
                yield self.symbolTable.valued('(param)', Expression(token))
        yield self.symbolTable['(end)']

    def expression(self, rbp=0):
        t, self.token = self.token, next(self.tokenStream)
        left = t.nud()
        while rbp < self.token.lbp:
            t = self.token
            self.token = next(self.tokenStream)
            left = t.led(left)
        return left

    def process(self, s):
        """Parse string"""
        self.tokenStream = self.tokenize(s)
        self.token = next(self.tokenStream)
        return self.expression()
