#!python3
"""The panels for the user interface of the zebra program.

Layout of the Zebra console differs a bit from one version to another,
but has these elements:
33 3-position switches for entering an instruction or break point
grouped 5,5,4,1;5;4,4,5
16 switches for locking pages, grouped 4,4,4,4 with a "lock" and "t0" switch
6 switches U1..6
4+2 switches/buttons cond.stop, stop, step, manual; clear, start
a rotary switch next to the screen with positions 4..15, D
My guess what it all does:
rotary switch: determine bottom line of screen (rest is A,B,C)
page lock: one switch per page, one for track 0, a global "unlock all" switch
cond.stop: switch that stops if the instruction has matching bits for the switches outside of the middle position
stop: stop the processor
step: do 1 instruction
manual: perform the instruction given by the switches (?)
clear: A=B=C=D=0
start: set U7=0 temporarily
"""

import os
import sys

import tkinter.tix as tk

from simpleImage import SimpleImage
from table import Table
import zebra

#for the clock indicator. Since it does about 10**5 instructions per minute,
#7 digits is plenty...
CLOCKFORMAT = '{:7d} instr in {:2d}:{:0>4.1f}'

#find our local files
CURDIR = os.path.dirname(sys.executable if hasattr(sys, 'frozen') else __file__)
FILEDIR = os.path.join(CURDIR, 'tapes')
IMAGEDIR = os.path.join(CURDIR, 'images/')
DEFAULTTAPE = 'DEMO1'

#basic images for KeyPanel
zero16 = SimpleImage([
    0b00000000,
    0b00000011,
    0b00001111,
    0b00011100,
    0b00110000,
    0b00110000,
    0b01100000,
    0b01100000, ], 8).quadruple()

blank16 = SimpleImage([0]*16, 16)

#one quarter of 1 symbol, then extended to a full one
one16 = SimpleImage([
    0b00000000,
    0b00000001,
    0b00000001,
    0b00000001,
    0b00000001,
    0b00000001,
    0b00000001,
    0b00000001, ], 8).quadruple()

#if you squint, you can read "Set switches" in the pattern of ones
rotatedSet = SimpleImage([
    0b0011110000000000000000000000000000000000000000000000010000000000000000000,
    0b0100001000000001000000000000000000000010010000000000010000000000000000000,
    0b1000000000000001000000000000000000000000010000000000010000000000000000000,
    0b1000000000000111110000000000000000000001111100000000010000000000000000000,
    0b0100000011110001000000001111001000001010010000001110010111000111100011110,
    0b0011000100001001000000010000101000001010010000010001011000101000010100001,
    0b0000100111111001000000010000001000001010010000100000010000101111110100000,
    0b0000010100000001000000001111001001001010010000100000010000101000000011110,
    0b0000010100000001000000000000101001001010010000100000010000101000000000001,
    0b1000010010001001000100010000101001001010010001010001010000110100010100001,
    0b0111100001110000111000001111000110110010001110001110010000100011100011110,
    ]).rotateLeft()

#provide a place to store the button image bitmaps (tk doesn't store them)
class NameSpace: pass
buttonBitmaps = NameSpace()

class ZebraKey(tk.Select):
    """A two-or three position Zebra panel key.
    Will only work as part of a KeyPanel,
    because is uses the x11... key images from there.
    """
    #colors of the keys
    LIGHT = '#ddd'
    DARK = '#aaa'
    def __init__(self, parent, values, *, label=None, value='0',
            color=LIGHT, **kwargs):
        super().__init__(parent,
            orientation=tk.VERTICAL,
            labelside=tk.NONE if label is None else tk.TOP,
            label=label,
            radio=True,
            **kwargs)
        #compute height of a position of the button
        height = buttonBitmaps.x11zero.height()
        if values==2: height += height//4
        #add two or three positions
        self.add('1',
            image=buttonBitmaps.x11one,
            height=height,
            background=color)
        if values==3:
            #middle button
            self.add('x',
                image=buttonBitmaps.x11space,
                background=color)
        self.add('0',
            image=buttonBitmaps.x11zero,
            height=height,
            background=color)
        #set initial value
        self["value"] = value

    def keyToggle(self, event):
        """For keyboard shortcut of two position keys."""
        self["value"] = '0' if self["value"]!='0' else '1'

    def keyToggle0(self, event):
        """For keyboard shortcut of three positions keys: upper key."""
        self["value"] = '0' if self["value"]!='0' else 'x'

    def keyToggle1(self, event):
        """For keyboard shortcut of three positions keys: lower key."""
        self["value"] = '1' if self["value"]!='1' else 'x'

    def keyRotate(self):
        """Go through three positions"""
        self["value"] = {'0':'x', 'x':'1', '1':'0'}[self["value"]]

class KeyPanel(tk.Frame):
    """A part of the front panel, consisting of one or more rows of two- or
    three state keys.
    getValue3 return the mask and settings as two integers:
    the most significant bit is added first.
    You have to do the building of the panel yourself, but the details of the
    keys and the value computation are done here.
    Your keys are in self.allKeys
    If you set the command of the Select to self.command,
    your callback is called with index of the button and the new value.
    Usage:
    #class ...:
    #    def __init__(self, parent, callback):
    #        super().__init__(parent)
    #        ... create keys with addKey
    #        #add callback at the end to prevent spurious calls
    #        self.callback = callback
    """
    #distance between groups
    SEPARATOR = 1
    def __init__(self, parent, callback=None, **kwargs):
        self.callback = callback
        super().__init__(parent, **kwargs)
        #the list of all keys in order of adding
        self.allKeys = []
        #previous state of the keys gets initialised if getValue3 is first used
        #if your initial state is not 0, fix this after initialisation
        self.lastStates = None

    def addKey(self, parent, values, color, label=None, value='0'):
        """Add a key to the panel.
        """
        s = ZebraKey(parent,
            values,
            color=color,
            label=label,
            value=value,
            command=self.command)
        self.allKeys.append(s)
        return s

    def getValue3(self):
        """Return a mask and a value for the breakpoint.
        Value is 1 for the upper positions;
        Mask is 0 for the middle positions.
        """
        theString = ''.join(s["value"][-1] for s in self.allKeys)
        xAs0 = int(theString.replace('x', '0'), 2)
        xAs1 = int(theString.replace('x', '1'), 2)
        return ~xAs0^xAs1, xAs0

    def command(self, button, value):
        """Call callback with number of changed button and new value"""
        #initialise lastStates if needed
        if self.lastStates is None: self.lastStates = 0,0
        #not needed if no callback
        if self.callback is None: return
        #we get a call for all two or three buttons; only react on press
        if value=='0': return
        #figure out changed button
        newStates = self.getValue3()
        changedBit = max(
            (new^last).bit_length()
            for (new,last) in zip(newStates, self.lastStates)
            )-1
        self.callback(changedBit, newStates)
        self.lastStates = newStates

class LockKeys(KeyPanel):
    def __init__(self, parent, callback):
        super().__init__(parent, borderwidth=1, relief=tk.SUNKEN)
        box = tk.Frame(self)
        tk.Label(box, text="Memory lock (*512 words)").pack(side=tk.TOP)
        for f in 'L dddd llll ddDD LLLL ':
            if f==' ':
                tk.Label(box).pack(side=tk.LEFT, ipadx=self.SEPARATOR)
            else:
                self.addKey(box,
                    2,
                    color=ZebraKey.DARK if f in "dD" else ZebraKey.LIGHT,
                    label=len(self.allKeys)-1 if self.allKeys else 'T0',
                    value='1' if f.isupper() else '0',
                    ).pack(side=tk.LEFT)
        self.lastStates = self.getValue3()
        self.makeUnlockButton(box)
        box.pack()
        self.callback = callback

    def makeUnlockButton(self, box):
        self.enableVar = tk.BooleanVar()
        self.enable = tk.Checkbutton(box, indicatoron=False, text="Unl.\nall",
            command=self.unlockButton, variable=self.enableVar)
        self.enable.pack(side=tk.RIGHT, fill=tk.Y)

    def unlockButton(self, *args):
        """Enable or disable buttons dependent on the unlock all button.
        """
        #button says "unl. all" so we enable the keys if not pressed
        state = "disabled" if self.enableVar.get() else "normal"
        for k in self.allKeys: k["state"] = state

class UserKeys(KeyPanel):
    def __init__(self, parent, callback, **kwargs):
        super().__init__(parent, borderwidth=1, relief=tk.SUNKEN, **kwargs)
        #build from right to left to make bit order work
        for f in range(6, 0, -1):
            self.addKey(self,
                2,
                color=ZebraKey.DARK if f=='d' else ZebraKey.LIGHT,
                label=f,
                ).pack(side=tk.RIGHT)
        self.callback = callback

class TriStateWord(KeyPanel):
    """A widget with 33 tri-state switches that allows to enter Zebra words
    or select breakpoint criteria.
    Also contains selected other widgets.
    +------------------------+------------+
    |                        | eff. meter |
    |   function keys (15)   +------------+
    |                        |    clock   |
    +-----+------+-----------+------------+
    |     | func |                        |
    | set | reg  | register/address keys  |
    |     | adr  |       (5+13)           |
    +-----+------+------------------------+
    """
    def __init__(self, parent, app):
        self.getNumber = app.getNumber
        self.makeImages()
        super().__init__(parent, borderwidth=1, relief=tk.SUNKEN)
        self.addFuncKeys().grid(rowspan=2, columnspan=2)
        #TODO 3: see http://tcltk.free.fr/source/volt.tcl for a nicer meter
        self.meter = tk.Meter(self, fillcolor='#0f3', text="Efficiency")
        self.meter.grid(row=0, column=2, columnspan=2, padx=5, sticky=tk.S)
        self.clock = tk.Label(self,
            text=CLOCKFORMAT.format(0, 0, 0),
            justify=tk.RIGHT,
            relief=tk.SUNKEN)
        self.clock.grid(row=1, column=2, columnspan=2)
        self.addSetButtons().grid(row=2, column=0, sticky=tk.W)
        self.addRegMemKeys().grid(row=2, column=1, columnspan=3)

    def makeImages(self):
        """Images for use by the ZebraKey
        """
        buttonBitmaps.x11zero = tk.BitmapImage(data=zero16.makeX11data())
        buttonBitmaps.x11space = tk.BitmapImage(data=blank16.makeX11data())
        buttonBitmaps.x11one = tk.BitmapImage(data=one16.makeX11data())

    def addFuncKeys(self):
        self.func = tk.Frame(self)
        for f in 'AKQLRIBCDE V421 W':
            if f==' ':
                tk.Label(self.func).pack(side=tk.LEFT, ipadx=self.SEPARATOR)
            else:
                self.addKey(self.func,
                    3,
                    color=ZebraKey.DARK if f in "IBCDE" else ZebraKey.LIGHT,
                    label=f,
                    value='x',
                    ).pack(side=tk.LEFT)
        return self.func

    def addRegMemKeys(self):
        self.regmem = tk.Frame(self)
        tk.Label(self.regmem,
            text="Registers",
            ).grid(row=0, column=0, columnspan=5)
        for c in range(5):
            self.addKey(self.regmem,
                3,
                color=ZebraKey.DARK,
                value='x',
                ).grid(row=1, column=c)
        tk.Label(self.regmem).grid(row=0, column=5, ipadx=self.SEPARATOR)
        tk.Label(self.regmem, text="Track").grid(row=0, column=6, columnspan=8)
        tk.Label(self.regmem, text="Address").grid(row=0, column=14, columnspan=5)
        for c in range(6, 19):
            self.addKey(self.regmem,
                3,
                color=ZebraKey.DARK if 10<=c<14 else ZebraKey.LIGHT,
                value='x',
                ).grid(row=1, column=c)
        return self.regmem

    def addSetButtons(self):
        self.setButtons = tk.Frame(self)
        self.rotatedSet = tk.BitmapImage(data=rotatedSet.makeX11data())
        getNumber = self.getNumber
        tk.Label(self.setButtons, image=self.rotatedSet).grid(
            row=0, rowspan=3, column=0, sticky=tk.W, padx=5)
        self.funcButton = tk.Button(self.setButtons, text="function",
            command=lambda:getNumber('func'))
        self.funcButton.grid(row=0, column=1, sticky=tk.E+tk.W)
        self.regButton = tk.Button(self.setButtons, text="register",
            command=lambda:getNumber('reg'))
        self.regButton.grid(row=1, column=1, sticky=tk.E+tk.W)
        self.adrButton = tk.Button(self.setButtons, text="address",
            command=lambda:getNumber('addr'))
        self.adrButton.grid(row=2, column=1, sticky=tk.E+tk.W)
        return self.setButtons

class LockingKey(tk.Checkbutton):
    """Behaves as a button with left mouse button,
    but can be kept depressed with the right button.
    The button is drawn darker to indicate this behaviour,
    unless it is kept depressed (this latter behaviour appears to be a tk
    bug, but I happen to like it this way).
    Has a builtin variable, that you access with get() and put()
    Command routine is called as usual.
    """
    def __init__(self, parent, **kwargs):
        self.variable = tk.BooleanVar()
        super().__init__(parent,
            indicatoron=False,
            background=ZebraKey.DARK,
            activebackground=ZebraKey.DARK,
            variable=self.variable,
            **kwargs)
        #toggle the button at press, to it behaves like a normal button
        self.bind('<ButtonPress-1>', lambda e:self.invoke())
        #right button makes it toggle
        self.bind('<ButtonRelease-3>', lambda e:self.invoke())

    def get(self): return self.variable.get()
    def put(self, value): return self.variable.put(value)

class ControlPanel(tk.Frame):
    """Control switches and buttons of the Zebra.
    Calls the tellZebra function with information on the button pushed.
    """
    GONE, DIAL, GRAZE, STAND, ACTIVE, STEP, WALK, RUN = range(0, 80, 10)
    DIALHISTORY = 10
    def __init__(self, parent, animationSpeed, app):
        super().__init__(parent,
            borderwidth=1,
            relief=tk.SUNKEN)
        self.animationSpeed = animationSpeed
        self.word = TriStateWord(self, app)
        self.word.pack(fill=tk.X)
        self.lock = LockKeys(self, self.lockKeysChanged)
        self.lock.pack(fill=tk.X)
        bottomRow = tk.Frame(self)
        bottomRow.pack(fill=tk.BOTH)
        self.speedButton = self.createSpeedIndicator(bottomRow)
        self.speedButton.pack(side=tk.LEFT, padx=3, pady=3)
        self.userKeys = UserKeys(bottomRow, self.userKeysChanged)
        self.userKeys.pack(side=tk.BOTTOM)
        self.createControlKeys(bottomRow).pack(
            side=tk.RIGHT, before=self.userKeys)
        self.tellZebra = app.tellZebra
        self.speed = 2
        #the last DIALHISTORY added numbers
        self.dialedNumbers = ["100"]
        #TODO 3: move undoVar to Application
        self.undoVar = tk.BooleanVar()

    def createSpeedIndicator(self, parent):
        """Create an the indicator showing running speed.
        """
        self.pictureTable = {
            speed:tk.PhotoImage(file=IMAGEDIR+name+'.gif')
            for speed, name in (
                (self.GONE, "gone"),
                (self.DIAL, "dial"),
                (self.GRAZE, "graze"),
                (self.STAND, "stand"),
                (self.ACTIVE, "active"),
                (self.STEP, "step"),
                (self.WALK, "walk1"), (self.WALK+1, "walk2"),
                    (self.WALK+2, "walk3"), (self.WALK+3, "walk4"),
                    (self.WALK+4, "walk5"), (self.WALK+5, "walk6"),
                (self.RUN, "run1"), (self.RUN+1, "run2"),
                    (self.RUN+2, "run3"), (self.RUN+3, "run4"),
                )
            }
        self.fastVar = tk.BooleanVar()
        speedButton = tk.Checkbutton(parent,
            indicatoron=False,
            image=self.pictureTable[self.GONE],
            borderwidth=1,
            height=48, width=72,
            relief=tk.SUNKEN,
            command=self.setSpeed,
            variable=self.fastVar,
            )
        #remember the after routine so we can cancel it when appropriate
        self.afterAnimate = None
        return speedButton

    def setImage(self, index=None):
        """Set the zebra to the given image,
        or to the next image in the sequence if index is None.
        A sequence is a series of images;
        the first image is a multiple of 10.
        If there is no further image in the sequence, it restarts.
        All images are in pictureTable; current index is in imageIndex
        Meaning of images:
        step button   zebra process   name      image        reason
        X             set, 0          GRAZE     graze        user action
        X             stop, 0         STAND     stand        program end
        X             dial, 0         DIAL      dial         program
        click                                   step         user action
        X             step, 1         STEP      step         user action
        X             set, 1          ignored
        X             set, 2          WALK      walk*        user action
        X             set, 3          RUN       run*         user action
        """
        #TODO 2: set image to active if something "interesting" happens, like conditional stop
        #stop old animation
        if self.afterAnimate is not None:
            self.after_cancel(self.afterAnimate)
        #calculate index
        if index is None:
            self.imageIndex +=1
            if self.imageIndex not in self.pictureTable:
                self.imageIndex -= self.imageIndex%10
        else:
            self.imageIndex = index
        theImage = self.pictureTable[self.imageIndex]
        if theImage.height()>48 or theImage.width()>72:
            #do a rough resize if the image is too big:
            #enlarge image and subsample
            self.theImage = theImage = theImage.zoom(48//24,72//24).subsample(
                -(theImage.height()//-24), -(theImage.width()//-24))
        #set image
        self.speedButton["image"] = theImage
        #do next animation
        self.afterAnimate = self.after(self.animationSpeed, self.setImage)

    def createControlKeys(self, panel):
        """Create the six main keys to operate the machine.
        """
        PAD = 3
        #we use x padding on alternate buttons to get distance effect
        box = tk.Frame(panel, borderwidth=1, relief=tk.SUNKEN)
        self.condStopVar = tk.BooleanVar()
        self.condStopKey = tk.Checkbutton(box,
            text="Cond.\nstop",
            command=self.condStopCommand,
            variable=self.condStopVar,
            indicatoron=False,
            )
        self.condStopKey.pack(side=tk.LEFT, padx=PAD, pady=PAD)
        self.stopVar = tk.BooleanVar()
        self.stopButton = tk.Checkbutton(box,
            indicatoron=False,
            text="Stop",
            command=self.setSpeed,
            variable=self.stopVar,
            )
        self.stopButton.pack(side=tk.LEFT, pady=PAD)
        self.stepButton = tk.Button(box,
            text="Step",
            command=self.stepPushed,
            )
        self.stepButton.pack(side=tk.LEFT, padx=PAD, pady=PAD)
        self.manualButton = tk.Button(box,
            text="Manual",
            command=self.manualPushed,
            )
        self.manualButton.pack(side=tk.LEFT, pady=PAD)
        self.clearButton = LockingKey(box,
            command=self.clearCommand,
            text="Clear",
            )
        self.clearButton.pack(side=tk.LEFT, padx=PAD, pady=PAD)
        self.startButton = LockingKey(box,
            command=self.startCommand,
            text="Start\n= U7")
        self.startButton.pack(side=tk.LEFT, fill=tk.Y, pady=3)
        return box

    def lockKeysChanged(self, bit, value):
        dummy, value = value
        #the bit numbers are T0: 16, rest: 15-num
        index = -1 if bit==16 else 15-bit
        self.tellZebra('memlock', index, value>>bit&1)

    def userKeysChanged(self, bit, value):
        dummy, value = value
        self.tellZebra("switch", bit+1, value>>bit&1)

    def condStopCommand(self):
        if self.condStopVar.get():
            self.tellZebra("break", *self.word.getValue3())
        else:
            self.tellZebra("break", 0, 1)

    def setSpeed(self):
        """Stop or speed pushed: send command to adjust zebra speed."""
        if self.stopVar.get():
            #push in: stop
            self.tellZebra("speed", 0)
        elif self.fastVar.get():
            #fast mode
            self.updateTime = 500
            self.tellZebra("speed", 3)
        else:
            #real time mode
            self.updateTime = 200
            self.tellZebra("speed", 2)

    def stepPushed(self):
        self.tellZebra("speed", 1)

    def manualPushed(self):
        self.tellZebra("register", 0, self.word.getValue3()[1])

    def clearCommand(self):
        if self.clearButton.get():
            self.tellZebra("switch", 0, None)
        self.setSpeed()

    def startCommand(self):
        self.tellZebra("switch", 7, (0 if self.startButton.get() else 1))
        #give it a push if it was waiting
        self.setSpeed()

    def dial(self):
        """Ask the user for a number.
        Open dial window.
        """
        dialWindow = self.dialWindow = tk.Toplevel()
        dialWindow.title("Please dial a number")
        history = self.history = tk.ComboBox(dialWindow,
            editable=True,
            anchor=tk.E,
            dropdown=False,
            selection=self.dialedNumbers[-1],
            label="The emulator has detected that the Zebra is waiting "
                "for dial input.\n Please enter a number to be dialed:",
            command=self.dialCommand)
        self.history.subwidget('label')['wraplength']=100
        #if window gets destroyed unexpectedly, reset zebra
        dialWindow.bind('<Destroy>', self.dialDestroyed)
        self.dialActive = True
        #fill history panel from stored numbers
        for n in sorted(self.dialedNumbers, key=lambda s:eval(s, {})):
            history.insert(tk.END, n)
        history.pack(padx=25, pady=25)
        history.focus()

    def dialCommand(self, s):
        try:
            value = eval(s, {})
            if not isinstance(value, int): raise TypeError
            if not 0 <= value < 1<<33: raise ValueError(value)
        except Exception:
            self.history['label'] = "Please enter a number or " \
                "Python expression giving an integer of up to 33 bits."
            return
        #maintain history list
        if s not in self.dialedNumbers: self.dialedNumbers.append(s)
        #only remember the last DIALHISTORY numbers
        del self.dialedNumbers[:-self.DIALHISTORY]
        #destroy window, but do not call dialDestroyed
        self.dialActive = False
        self.dialWindow.destroy()
        #pass on result to zebra
        self.tellZebra("dial", value)

    def dialDestroyed(self, event):
        """Abort the dial command. Since the zebra is waiting for it,
        go to zero to stop the computer.
        """
        if self.dialActive: self.tellZebra("register", 0, 0)


class IOpanel(tk.PanedWindow):
    """Panel with the I/O devices: tape reader, punch and printer"""
    #width in chars
    WIDTH = 25
    def __init__(self, parent, app):
        super().__init__(parent,
            orientation=tk.VERTICAL,
            height=100,
            panerelief=tk.SUNKEN)
        self.tellZebra = app.tellZebra
        self.add("reader", expand=3, min=30, size=80)
        self.add("punch", expand=3, min=40, size=60)
        self.add("printer", expand=3, min=30, size=80)
        self.buildReader(self.subwidget("reader"))
        self.buildPunch(self.subwidget("punch"))
        self.buildPrinter(self.subwidget("printer"))

    def buildReader(self, panel):
        """Populate the tape reader panel"""
        self.fileEntry = tk.Frame(panel)
        tk.Label(self.fileEntry, text="Input tape:").pack(side=tk.LEFT)
        self.currentName = tk.Label(self.fileEntry,
            width=15,
            relief=tk.SUNKEN,
            borderwidth=1,
            background='#FFF')
        self.currentName.pack(side=tk.LEFT)
        self.currentName.bind('<Button-1>', self.loadTape)
        self.loadButton = tk.Button(self.fileEntry,
            text="Load",
            command=self.loadFile)
        self.loadButton.pack(side=tk.RIGHT, before=self.currentName)
        self.fileEntry.pack(
            side=tk.TOP, fill=tk.X)
        self.reader = tk.ScrolledText(panel, width=self.WIDTH, scrollbar=tk.BOTH)
        self.reader.text["wrap"] = tk.NONE
        self.readerText = self.reader.text
        self.reader.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.currentTapeFile = None

    def loadFile(self):
        dialog = tk.ExFileSelectDialog(self,
            title="Select a tape file:",
            command=self.loadTape,
            )
        fsbox = dialog.subwidget('fsbox')
        fsbox['filetypes'] = \
            "{{*}     {.* All files}} " \
            "{{*.ZNC *.SRC *.ZSC *.ZBI *.ZRB *.ZTP} {.SRC,... Any Zebra file}} " \
            "{{*.ZNC} {.ZNC Zebra Normal Code}} " \
            "{{*.SRC} {.SRC Zebra file, unspecified}} " \
            "{{*.ZSC} {.ZSC Zebra Simple Code}} " \
            "{{*.ZBI} {.ZBI Zebra binary}} " \
            "{{*.ZRB} {.ZRB Zebra relocatable binary}} " \
            "{{*.ZTP} {.ZTP Zebra tape programme}}"
        if self.currentTapeFile is None:
            self.currentTapeFile = zebra.Tape.expandPath(DEFAULTTAPE)
        fsbox['directory'], fsbox['value'] = os.path.split(self.currentTapeFile)
        fsbox['pattern'] = "*.ZNC *.SRC *.ZSC *.ZBI *.ZRB *.ZTP"
        #make on screen list of file types longer; it is much too short
        fsbox.subwidget('types').subwidget('listbox')['height'] = 8
        #make the window comfortably big
        #screen_height = root.winfo_screenheight()
        fileDirPane = fsbox._nametowidget('lf.pane')
        #use half the screen height for the dir/file lists
        fileDirPane['height'] = 500 #min(screen_height//2, 800)
        #make the dir/file lists reasonably wide
        fileDirPane['width'] = 450
        dialog.bind('<Escape>', lambda e:dialog.popdown())
        dialog.popup()

    def loadTape(self, path=None):
        """(Re)load the tape file.
        May be called with event (which means click on file name: reload)
        or path (which means load).
        """
        if isinstance(path, tk.Event): path = self.currentTapeFile
        #get fileName and fileText: first set default values
        fileName = '<no file>'
        fileText = "File not found"
        if path is not None:
            fileName = os.path.basename(path)
            try:
                fileText = open(path, 'r').read()
                self.tellZebra("tape", path)
                self.currentTapeFile = path
            except FileNotFoundError:
                self.currentTapeFile = None
        #show fileName and fileText
        self.currentName['text'] = fileName
        self.readerText["state"] = tk.NORMAL
        self.readerText.delete('1.0', tk.END)
        self.readerText.insert(tk.END, fileText)
        #set tag styles
        self.readerText.tag_config('reader', background='#3ff', foreground='#000')
        self.readerText.tag_config('error', background='#f33', foreground='#000')
        self.readerText["state"] = tk.DISABLED
        #flag to delay update if UI can't keep up
        self.disableUpdate = False
        #initialize positions
        self.lastLine = 0
        self.readPosTodo = []

    def showReadPosition(self, line, col, char):
        """Mark the position as being read.
        Not now, that's too slow; the processing is done after idle by
        doMarkPositions.
        """
        #start background mark routine if needed
        if not self.readPosTodo: self.after_idle(self.doMarkPositions)
        #put info on the todo list
        self.readPosTodo.append((line, col, char))

    def doMarkPositions(self):
        """Color the most recent received tape character,
        and some extra if not too much work.
        This routine can come behind the tape being read, but eventually
        everything gets colored.
        It draw the most recent read character first;
        this makes it appear to keep up with the read positions.
        Occasionally, it stays behind for a while,
        resulting in a few "old" characters that are left blank
        for a fraction of a second.
        """
        readerText = self.readerText
        readPosTodo = self.readPosTodo
        #get last position to tag
        line, startCol, char = readPosTodo.pop()
        endCol = startCol+1
        if char=='':
            #end of tape condition: mark and stop
            startPos = "{}.{}".format(line, startCol)
            readerText.tag_add('error', startPos, startPos+'+1c')
            return
        #check if by any chance the previous char is at the end of the list
        #this occurs most of the time, so it is a big time saver
        while readPosTodo:
            line1, col1, char1 = readPosTodo[-1]
            if line1==line and col1==startCol-1:
                #mark an extra char before the current one(s)
                startCol = col1
                #remove the char, try if there is more to combine
                readPosTodo.pop()
            else:
                #done, draw
                break
        startPos = "{}.{}".format(line, startCol)
        endPos = "{}.{}".format(line, endCol)
        readerText.tag_add('reader', startPos, endPos)
        #scroll to show new line that is being processed
        if line>self.lastLine: readerText.see('reader.last-1c')
        self.lastLine = line
        #draw more chars if there is still stuff on the todo list
        if readPosTodo: self.after_idle(self.doMarkPositions)

    def buildPunch(self, panel):
        """Populate the punch panel"""
        tk.Label(panel, text="Punch").pack(side=tk.TOP, fill=tk.X)
        self.punch = tk.ScrolledText(panel, width=self.WIDTH, scrollbar=tk.Y)
        self.punch.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.punchText = self.punch.text
        self.punchBuffer = bytearray()
        punchType = self.punchType = tk.Select(panel,
            labelside=tk.NONE,
            orientation=tk.HORIZONTAL,
            radio=True,
            command=self.punchCommand)
        punchType.add("normal", text="Normal")
        punchType.add("simple", text="Simple")
        punchType.add("print", text="Print")
        punchType.add("binary", text="Binary")
        #TODO 4: add button "guess", ...
        punchType.pack(side=tk.BOTTOM, before=self.punch)
        punchType["value"] = "normal"

    def punchCommand(self, button, value):
        if value=='1' and self.punchBuffer:
            #button press. Release is not interesting
            #nor it this when there is nothing in the punch buffer
            self.punchText["state"] = tk.NORMAL
            self.punchText.delete("1.0", tk.END)
            #first symbol: reset the converter
            self.punchText.insert(tk.END,
                self.convertPunchChar(self.punchBuffer[0], reset=True)
                )
            #the rest
            self.punchText.insert(tk.END,
                ''.join(map(self.convertPunchChar, self.punchBuffer[1:]))
                )
            self.punchText["state"] = tk.DISABLED
            self.punchText.see(tk.END)

    def punchChar(self, char):
        """Update the punch panel.
        The character received is the direct tape value.
        A copy of the text is kept for reinterpretation.
        """
        self.punchBuffer.append(char)
        self.punchText["state"] = tk.NORMAL
        self.punchText.insert(tk.END, self.convertPunchChar(char))
        self.punchText["state"] = tk.DISABLED
        self.punchText.see(tk.END)

    def convertPunchChar(self, symbol, reset=False):
        """Convert a punch binary symbol according to current mode.
        reset is to reset the state of the state machine, where applicable.
        """
        punchType = self.punchType["value"]
        if punchType=="normal":
            char = zebra.TAPESYMBOLS[symbol]
            if char in "AXN+-T": char = '\n'+char
            return char
        elif punchType=="simple":
            symbol = zebra.TAPESYMBOLS[symbol]
            #adjust for characters of Simple Code
            if symbol=='B': symbol = 'H'
            elif symbol=='C': symbol = 'S'
            #layout
            if symbol in "HUNK+-QTYZSDLVXAEP": symbol = '\n'+symbol
            return symbol
        elif punchType=="print":
            if reset or not hasattr(self, "punchPrinter"):
                self.punchPrinter = zebra.Printer(self)
            return self.punchPrinter.decode(symbol)
        elif punchType=="binary":
            if reset or not hasattr(self, "binaryColumn"):
                #-1 means: not start seen yet
                self.binaryColumn = -1
            char = zebra.TAPESYMBOLS[symbol]
            if self.binaryColumn<0:
                #initial zeros are all on a line
                #initial one ends this line
                #any other char is start of next line
                if char=='0': pass
                elif char=='1': self.binaryColumn = 6
                else: self.binaryColumn = 0
            else:
                self.binaryColumn +=1
            if self.binaryColumn%7==0: char = '\n'+char
            return char

        else: raise NotImplementedError

    def buildPrinter(self, panel):
        """Populate the printer panel
        """
        tk.Label(panel, text='Printer output').pack(fill=tk.X)
        self.printer = tk.ScrolledText(panel, width=self.WIDTH, scrollbar=tk.Y)
        self.printer.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.printerText = self.printer.text
        #put an up arrow in the window
        self.printerText.insert(tk.END, '\x18')
        self.printerText["state"] = tk.DISABLED

    def printChar(self, char):
        self.printerText["state"] = tk.NORMAL
        #insert text before the "end" arrow
        self.printerText.insert(tk.END+'-2c', char)
        self.printerText["state"] = tk.DISABLED
        self.printerText.see(tk.END)

class RegisterPanel(Table):
    """Show values from a set of registers. Modern version of the Zebra screen.
    shownValues contains list of values shown
    """
    #TODO 2: put column dependent code in one place; maybe let Table provice some support for this?
    #the registers shown at startup; see addRegister
    INITIALREGISTERS = [2,3,-2,0,1,4,5,13,14,15,'34','100']
    SMALLSET = [2,3,0,13]
    def __init__(self, parent, app):
        #the requestZebra is used for redrawing the register window
        super().__init__(parent,
            borderwidth=1)
        self.requestZebra, self.tellZebra = app.requestZebra, app.tellZebra
        self.setupUI()
        #set the list of values that are shown at the moment
        self.shownValues = []
        for i in self.INITIALREGISTERS: self.addRegister(i)
        self.clickedRow = None

    def setupUI(self):
        """Build the construction of listboxes that form the columns
        and make them work with the scroll bar.
        self.listboxes will contain all the listboxes in order.
        """
        charWidth = self.contentFont.measure('n')
        #TODO 3: let fraction use up the available digits instead of fixing width
        for n,w in (
                ("Name", 4),
                ("Hex", 9),
                ("Instruction", 16),     #can be up to 18 for strange instructions
                ("Integer", 10),         #big negative numbers are 11
                ("Fraction", 12),        #12 is enough to show all digits
                ):
            just = "ljust" if n=="integer" else "rjust"
            self.addColumn(n, w, just)

    def addRegister(self, regNum):
        """Add a given register to the list of displayed objects
        Note: if you want updates, request them from the Zebra yourself!
        Encoding: A,B,C,D = 2,3,0,1  memory='addr'
        Special -2=origin -1=carry trap
        If regNum is a string, you get a memory location.
        """
        self.shownValues.append(regNum)
        self.changeRow(None,
            (self.getRegisterName(regNum), '', 'Waiting for reply', '')
            )

    def setRegisters(self, shownValues):
        """Set a given list of registers.
        """
        self.deleteRow()
        for regNum in shownValues: self.addRegister(regNum)
        self.redrawRegisters()

    def deleteRow(self, index=None):
        """Extend deleteRow to update shownValues"""
        super().deleteRow(index)
        if index is None: self.shownValues = []
        else: del self.shownValues[index]

    def getRegisterName(self, regNum):
        """Compute the name of a register or memory location
        if regNum is a string, it is a memory location (in decimal)
        """
        if regNum is None: return ''
        if isinstance(regNum, str): return regNum.zfill(4)
        if -2<=regNum<4: return ('C','D','A','B',"org",'V')[regNum]
        return str(regNum)

    def processUpdate(self, updateIndex, value):
        """Process register value reply from the Zebra
        Return True if this update was useful;
        False if the register is not displayed any more
        """
        name = self.getRegisterName(updateIndex)
        #determine place in display listbox
        try: index = self.shownValues.index(updateIndex)
        except ValueError:
            #apparently, index failed, which means we aren't interested
            #so don't request for updates anymore
            return False
        #compute other values to display. Special cases first
        if updateIndex==-1:
            hexString = fraction = ''
            instruction, signedValue = 'carry trap:', value
        if updateIndex==-2:
            hexString = fraction = ''
            if isinstance(value, int): value="({:03d})".format(value)
            instruction, signedValue = 'C computed as:', value
        else:
            #sign extend value
            signedValue = value
            bitLength = 34 if name=='A' else 33
            if signedValue>>bitLength-1: signedValue -= 1<<bitLength
            #disassemble, and split hex string and instruction
            if isinstance(updateIndex, str): base = int(updateIndex)
            else: base = '*'
            dummy, hexString, instruction = zebra.display(
                    base,
                    *zebra.split(value),
                    explainCondition=False).split(None, 2)
            fraction = '{:+.9f}'.format(signedValue/(1<<32))
        self.changeRow(index, (
            name,
            hexString,
            instruction,
            str(signedValue),
            fraction,
            ), see=False)
        #do request for changes in the future, so we are always up to date
        return True

    def resizePanels(self):
        """The panels are resized; do a redraw."""
        super().resizePanels()
        self.redrawRegisters()

    def redrawRegisters(self, row=None):
        """Ask zebra for current values of everything that is displayed
        Zebra reply calls Application.Zregister, which calls processUpdate
        for the drawing and then requests for new update
        """
        if row is None: values = self.shownValues
        else: values = [self.shownValues[row]]
        for i in values:
            if isinstance(i, str): self.requestZebra("memory", int(i))
            else: self.requestZebra("register", i)

    def enterNumber(self, row, col, entry, lb):
        """Enter was pressed in row,col.
        Entry widget was entry, listbox is lb
        """
        #existing TODO 2: better handle invalid entries; show error popup?
        #erase row for empty value
        shownValues = self.shownValues
        value = entry.get()
        if not value:
            self.deleteRow(row)
        elif col==0:
            #change, delete or add registers
            #refuse syntax errors or double entries
            try: regNums = self.parseRegNum(value.strip())
            except ValueError: return
            #refuse double entries, but allow replacing by the same one
            forbidden = set(shownValues)-{shownValues[row]}
            if forbidden.intersection(regNums): return
            shownValues[row:row+1] = list(regNums)
            #update rows in the table
            for r in range(row, len(shownValues)):
                self.changeRow(r, (
                    self.getRegisterName(shownValues[r]),
                    '',
                    'Waiting for reply',
                    ''))
            self.redrawRegisters()
        else:
            try:
                if col==1: value = int(value, 16)
                elif col==2: value = zebra.assemble(value)
                elif col==3: value = int(value, 10)
            except (ValueError, SyntaxError): return
            if isinstance(shownValues[row], str):
                self.tellZebra("memory", int(shownValues[row], 10), value)
            else:
                self.tellZebra("register", shownValues[row], value)
            self.redrawRegisters(row)
        self.endEdit()

    def parseRegNum(self, s):
        """Convert s as typed in by user to values for shownValues.
        Registers are numbers, memory addresses are strings.
        Multiple values are allowed, separated by commas.
        Raises ValueError if there are invalid values.
        """
        registerNames = {'A':2, 'B':3, 'C':0, 'D':1, 'V':-1, "org":-2, '':None}
        result = []
        for regNum in s.split(','):
            if regNum in registerNames:
                result.append(registerNames[regNum])
                continue
            start, sep, end = regNum.partition('-')
            if start and sep and end:
                start, end = int(start, 10), int(end, 10)
                #range of registers or memory locations
                if regNum[0]=='0' or end>=32:
                    for n in range(start, end+1): result.append(str(n))
                else:
                    for n in range(start, end+1): result.append(n)
            else:
                n = int(regNum, 10)
                if regNum[0]=='0' or n>=32: result.append(str(n))
                else: result.append(n)
        return result

    def addEntry(self):
        """Add new line."""
        self.addRegister(None)
        self.makeEntryFrame(len(self.shownValues)-1, 0)

    def deleteRegister(self):
        """If there is a row selected, delete it.
        """
        if self.clickedRow is not None:
            row = self.clickedRow
            self.deleteRow(row)
        self.endEdit()

    def makeEntryFrame(self, row, col):
        """Save clicked row"""
        super().makeEntryFrame(row, col)
        #remember row number for delete
        self.clickedRow = row

    def endEdit(self, event=None):
        """Update clickedRow"""
        super().endEdit(event)
        self.clickedRow = None
