#!python3
"""Table widget, used by Zebra program.
"""
import tkinter.tix as tk
from tkinter.font import Font

class Table(tk.Frame):
    """Table widget consisting of listboxes in a PanedWindow that scroll together.
    panes: the PanedWindow
    listboxes: list of the Listbox items
    You have to provide a function enterNumber that decides what to do
    if the user enters a number and presses Enter
    updating is done per row with changeRow
    """
    FONTSIZE = 11
    def __init__(self, parent, contentFont=None, headerFont=None, **kwargs):
        super().__init__(parent, **kwargs)
        if contentFont is None:
            contentFont = Font(family="Courier", size=self.FONTSIZE)
        self.contentFont = contentFont
        if headerFont is None:
            headerFont = Font(family="Helvetica",
                slant="italic",
                size=self.FONTSIZE*3//4)
        self.italicFont = headerFont

        self.scrollbar = tk.Scrollbar(self, command=self.yview)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        #each column in the display is a listbox: this is a handy reference
        self.panes = tk.PanedWindow(self,
            panerelief=tk.SUNKEN,
            orientation=tk.HORIZONTAL)
        self.panes.pack(fill=tk.BOTH, expand=True)

        #for internal use to quickly access the boxes
        self.listboxes = []
        self.justifications = []

        #for changing the boundaries
        self.bind_all('<ButtonRelease-1>', self.handleClick, add='+')

        #the frame for editing entries
        self.entryFrame = None

    def addColumn(self, name, width, justification="ljust"):
        """Add a column
        name: lower case name of column
        width: number of characters for this column
        justification: "ljust", "rjust", "center", "zfill"
        """
        charWidth = self.contentFont.measure('n')
        self.panes.add(name.lower(), min=10, size=width*charWidth+5)
        self.justifications.append(justification)
        subwidget = self.panes.subwidget(name.lower())
        tk.Label(subwidget, text=name, font=self.italicFont).pack(
            side=tk.TOP, fill=tk.X)
        var = tk.Variable()
        columnNumber = len(self.listboxes)
        lb = tk.Listbox(subwidget,
            width=width,
            yscrollcommand=self.scrollSet,
            font=self.contentFont)
        lb.pack(fill=tk.BOTH, expand=True)
        self.listboxes.append(lb)

    def scrollSet(self, lo, hi):
        """Set the scrollbar, and synchronize the listboxes
        """
        self.scrollbar.set(lo, hi)
        #now update the listboxes without calling scrollbar.set
        for lb in self.listboxes: lb.yview_moveto(lo)

    def yview(self, *args):
        """Scroll all the listboxes with the scroll bar"""
        for lb in self.listboxes: lb.yview(*args)

    def changeRow(self, index, values, see=True):
        """Change or add entries of the listboxes with the respective values.
        You can add a new entry by taking index=None
        This routine assumes the width of the boxes is kept updated after
        a panel is resized, but tix doesn't do that for us;
        this is done for us by resizePanels
        Set see to False if you don't want the image to scroll to the updated row.
        """
        #remember scroll position, so we can honour see=False
        #because the listboxes scroll by themselves at .insert()
        scrollPos = int(self.scrollbar.get()[0]*self.countRows()+.5)
        #make new line if index is None
        if index is None: index = self.countRows()
        for i, lb, value in zip(range(5), self.listboxes, values):
            if index is not None: lb.delete(index)
            #get column width for clipping; maintained for us by resizePanels
            columnWidth = lb["width"]
            #justify
            value = getattr(value, self.justifications[i])(columnWidth)
            #show ellipsis if value doesn't fit 
            if len(value)>columnWidth:
                value = value[:columnWidth-1]+u'\N{horizontal ellipsis}'
            lb.insert(index, value)
        #scroll back to original place if see is False
        if not see: self.yview(scrollPos)

    def deleteRow(self, index=None):
        """Delete a row from the table.
        Redraw is not needed.
        Omit index to delete all (clear the table).
        """
        for lb in self.listboxes:
            if index is None:
                lb.delete(0, tk.END)
            else:
                lb.delete(index)

    def countRows(self):
        """Count the number of visible rows.
        """
        return self.listboxes[0].size()

    def handleClick(self, event):
        """Handle clicks for resizing or editing.
        When a cell is clicked, contentClick is called.
        Note: you are responsible for destroying the frame!
        You can call endEdit to do that.
        """
        #check if this is a click on a sash
        #these sashes are recognizable as event.widget is a string
        if isinstance(event.widget, str):
            self.resizePanels()
        #now we have a click in a listbox: find it
        #the other listboxes give empty selections
        for col, lb in enumerate(self.listboxes):
            if lb.curselection(): break
        else:
            #no selection found
            return
        #we got it: get row number, make frame over it for entry
        row, = lb.curselection()
        self.makeEntryFrame(row, col)

    def makeEntryFrame(self, row, col):
        """Place a frame exactly over a cell of the table."""
        lb = self.listboxes[col]
        #destroy previous frame
        if self.entryFrame is not None: self.entryFrame.destroy()
        self.entryFrame = tk.Frame(lb)
        x,y,w,h = lb.bbox(row)
        self.entryFrame.place(bordermode='outside',
            x=x, y=y, width=w, height=h+1)
        #handle the click proper
        self.contentClick(row, col, lb)

    def resizePanels(self):
        """One of the dividers has moved: recalculate the widths.
        You'll have to redraw in your class if you want to
        reformat the contents.
        """
        charWidth = self.contentFont.measure('n')
        self.update_idletasks()
        for lb in self.listboxes:
            #compute number of characters fitting in width
            #and set width to that
            oldwidth = lb["width"]
            newwidth = lb["width"] = int((lb.winfo_width()-3)/charWidth)

    def endEdit(self, event=None):
        self.entryFrame.destroy()
        self.entryFrame = None
        self.focus()

    def contentClick(self, row, col, lb):
        """There was a click in a cell. Start editing it.
        """
        value = lb.get(row, row)
        #row 0 gives the value proper; the rest gives a tuple; be flexible
        if isinstance(value, tuple): value, = value
        f = tk.Entry(self.entryFrame, font=self.contentFont)
        f.pack(fill=tk.BOTH)
        f.insert(tk.END, value)
        f.selection_range(0, tk.END)
        f.focus()
        f.bind('<KeyPress-Return>', lambda e:self.enterNumber(row, col, f, lb))
        f.bind('<KeyPress-Tab>', lambda e:self.tabPressed(row, col, f, lb))
        f.bind('<KeyPress-Escape>', self.endEdit)

    def tabPressed(self, row, col, f, lb):
        """User pressed tab.
        This will not update the other fields on the same line,
        as it is first sent to the Zebra.
        """
        self.enterNumber(row, col, f, lb)
        lb.selection_set(row)
        #give the a bit of time to send the value back before we update
        self.after(100, self.makeEntryFrame, row, col+1)
