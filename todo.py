#!python3
"""Handy utility to view what to do next."""

#tip: you can also use :vim /TODO \d:/ *.py|copen

import glob, sys
result = []
for fn in glob.glob('*.py'):
    for ln,line in enumerate(open(fn, 'r'), start=1):
        line = line.strip()
        if line.startswith('#TODO '): result.append((line, fn, str(ln)))

result.sort()

#compute column widths; sorry Edsger
maxline, maxfn, maxln = (max(map(len, c)) for c in zip(*result))

lines = 20
pat = ''
for a in sys.argv[1:]:
    if a.isdigit(): lines = int(a)
    else: pat = a
for line, fn, ln in result:
    if pat.startswith('-'):
        #negative pattern: we don't want pat in fn, so if it is, skip
        if pat[1:] in fn: continue
    else:
        #positive pattern
        if pat not in fn: continue
    print(fn.ljust(maxfn), ln.rjust(maxln), line[:85-maxfn-maxln])
    lines -=1
    if lines<=0: break
