#!python3
class SimpleImage:
    """A very simple image format: just a list of numbers, and a width
    Used for generating symmetric bitmaps for tk"""
    def __init__(self, numbers, width=None):
        if width is None: width = max(n.bit_length() for n in numbers)
        self.numbers = numbers
        self.width = width

    @property
    def height(self): return len(self.numbers)

    @staticmethod
    def mirrorBits(n, w):
        """Mirror an w-bits number n"""
        return int(('{:0{}b}'.format(n, w))[::-1], 2)

    def mirrorLR(self):
        return SimpleImage([self.mirrorBits(n) for n in self.numbers], self.width)

    def mirrorTB(self):
        return SimpleImage(self.numbers[::-1], self.width)

    def doubleLR(self, odd=False):
        """Extend image by adding its mirror to the right.
        Odd makes width odd by removing middle pixels.
        """
        width = self.width
        newWidth = 2*self.width-odd
        return SimpleImage(
            [n<<newWidth-width | self.mirrorBits(n, width) for n in self.numbers],
            newWidth)

    def doubleTB(self, odd=False):
        """Extend image by adding its mirror at the bottom.
        Odd makes width odd by removing middle pixels.
        """
        newHeight = 2*len(self.numbers)-odd
        result = list(self.numbers)
        result.extend(self.numbers[newHeight-self.height-1::-1])
        return SimpleImage(result, self.width)

    def quadruple(self, oddHeight=False, oddWidth=False):
        """Extend image four ways by adding mirrors to the right and bottom.
        """
        return self.doubleLR(oddWidth).doubleTB(oddHeight)

    def rotateRight(self):
        """Rotate image to the right.
        """
        return SimpleImage([
            sum((b>>col&1)<<row for row,b in enumerate(self.numbers))
            for col in range(self.width-1, -1, -1)], self.height)

    def rotateLeft(self):
        """Rotate image to the right.
        """
        return SimpleImage([
            sum((b>>col&1)<<row for row,b in enumerate(reversed(self.numbers)))
            for col in range(self.width)], self.height)

    def makeX11data(self):
        """Make an X11 bitmap file out of a width and list of ints.
        The bits of the ints determine the lines of the image.
        What a pathetic format...
        """
        template = '#define test_width {}\n' \
                   '#define test_height {}\n' \
                   'static char test_bits[] = {{ {} }};'
        data = []
        #make list of relevant bytes
        for row in self.numbers:
            row = self.mirrorBits(row, self.width)
            for col in range(0, self.width, 8):
                data.append(row>>col&0xff)
        return template.format(
                self.width,
                self.height,
                ', '.join(map('0x{:02x}'.format, data))
                )
