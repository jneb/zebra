This directory contains documentation on the Zebra, the "Operating system" and
various intermediate files used for obtaining knowledge.
The NIP is the original name of what we would call the "assembler" and that is
located at the highest addresses.

- nipnotes: both documentation of Normal Code, and a map of the memory layout
  of the assembler (that I used to reverse engineer the NIP)
- notes.txt: a very convoluted document that was the first attempt to reverse
  engineer the NIP. May contain errors
- zlotDot.txt: the second attempt after notes to organise everything.
  Eventually notes and zlotdoc will have to merge into a big commented listing
  of the NIP.
- instruction combi: document used to design explain.py
