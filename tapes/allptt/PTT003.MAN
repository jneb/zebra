Tue  08-08-95
Standard output. The calls have the vorm XaPn where
a = 37: print or print/punch machine code or punch telepr. code
    45: print
    46: print & puch machine code
    47: punch telepr. code
The form of the output is dependent on a switch in loc. 67 as
follows:
0:   print
-1:  print and punch machine code
-2:  punch telepr. code
The resident pattern for printing is contained in 65.
The call X47P3 brings (B) to 65.
The actions for a = 45, 46 and 47 depend on n:
n = 0  : print/punch (A) according to pattern in 65
    2  :     "       (A) according to pattern in B
   28  :     "       (A) superpositive with pattern in 65
   30  :     "       (A) superpositive with pattern in B
   31  :     "       (A) as standard integer +xxxxx_xxxxx__
   32  :     "       (A) as standard fraction +x.xxxxxxxxx__
Registers 4,5,6,7 and 15 are used.
For a = 45, 47:
n = 4C : print/punch CR, LF, FigShift
    6  :      "      (AB) in pentads (maximum 13)
    6C :      "      (A)  in pentads (maximum  6)
Uses registers 4,5,6,15

The calls for a = 37 are: ( (67) = 0, -1 or -2)
X37P      : print/punch (A) as standard integer +xxxxx_xxxxx__
X37P.3P.0 :      "      (A) as fraction  +x.xxxxxxxxx__
X37P2     :      "      (A) according to pattern in B
Uses registers 4,5,6,7, and 15

Only when (67) = 0 or -2:
X37P4C       : print/punch CR, LF, FS
X37P6        :      "      (AB) in pentads
X37P6C       :      "      (A)  in pentads
Uses registers 4,5,6,15
X37P117.4    :      "      (A) as instruction. (B) is address.
When instruction uses address (B)+1 instruction is printed in N form
X37P117.4 uses all registers except 12,13 and 14.

The pattern is a word of 33 bits, having the following significance
b0 = 0 : suppress sign
b0 = 1 : print sign
b1 = 0 : integer
b1 = 1 : fraction
b2b3b4b5 = i :gives the number of digits to be suppressed. Number
              of digits for output is 10 - i
b6b7, b8b9 .. etc. can form k = 0,1,2 or 3. Each symbol to be
output is controlled by the corresponding bit pair k :
k = 0: Unconditional output of digit
    1: for integers: non-significant zeros are replaced by spaces
    2: space. For fractions: b6b7 = 2: suppress first digit.
    3: point.
When all 10 - i digits are output then:
k = 0 : finish
    1 : CR, LF, FS, finish
  2,3 : as above

{Copyright Netherlands Postal & Telecommunications Services.
 PTT Research 1957-1967}


